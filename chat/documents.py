import datetime
import random
import re
import sha
import json, urllib
from django.conf import settings
from mongoengine import StringField, ListField, Document
from mongoengine import EmbeddedDocumentField, EmbeddedDocument, GeoPointField, DateTimeField
from mongoengine.django.auth import User
from mongoengine.queryset import *
from errorReport.views import save_error

class Chat(Document):

    first_user = StringField()
    second_user = StringField()
    message = ListField(EmbeddedDocumentField("Message"))
    title = StringField()
    type_chat = StringField()
    env_id = StringField()
    date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def create_chat(cls,**kwargs):
        try:
            msg = cls()
            msg.first_user = str(kwargs['first_user'])
            msg.second_user = str(kwargs['second_user'])
            msg.title = str(kwargs['title'])
            msg.type_chat = str(kwargs['type_chat'])
            guardar = msg.save()
        except Exception as e:
            error = "Error to create new chat: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'Chat',
                    method = 'create_chat - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')
        return msg

    @classmethod
    def add_message(cls,**kwargs):
        
        user_first = str(kwargs['first_user'])
        user_second = str(kwargs['second_user'])
        message=Message()
        message.text=str(kwargs['message'])
        message.user_send=user_first

        try:
            msg = cls.objects((Q(first_user=user_first) & Q(second_user=user_second)) | (Q(first_user=user_second) & Q(second_user=user_first))).first()
            msg.message.append(message)
            msg.save()
        except Exception as e:
            print "antes de create: ", kwargs['env_id']
            msg = cls.create_chat(first_user=user_first,second_user=user_second,title="",type_chat=str(kwargs['type_chat']),env_id=kwargs['env_id'])

            chat = cls.objects(id=msg.id).first()
            chat.message.append(message)
            chat.save()

            error = "Error adding message: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'Chat',
                    method = 'add_message - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')
        return msg

    @classmethod
    def add_message_by_chat_id(cls,chat_id=None,user_send=None,mssg=None,response=None):
        if chat_id is not None and mssg is not None:
            message = Message()
            message.text = str(mssg)
            message.user_send = user_send
            try:
                response = cls.objects(id=chat_id).first()
                response.message.append(message)
                response.save()
            except Exception, e:
                error = "Error adding message by chat_id: "+e.__str__().decode('utf-8')
                print error
                save_error(module = 'Chat',
                    method = 'add_message_by_chat_id - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')
        return response

    @classmethod
    def new_chat(cls,msg=None,**kwargs):
        
        user_first = str(kwargs['first_user'])
        user_second = str(kwargs['second_user'])
        message=Message()
        message.text=str(kwargs['message'])
        message.user_send=user_first

        try:
            msg = cls.create_chat(first_user=user_first,second_user=user_second,title="",type_chat=str(kwargs['type_chat']))
            chat = cls.objects(id=msg.id).first()
            chat.message.append(message)
            chat.save()
        except Exception as e:
            error = "Error creatting new chat: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'Chat',
                    method = 'new_chat - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')
        return msg

    @classmethod
    def delete_chat(cls, chat_id=None):
        try:
            chat = cls.objects(id=chat_id)
            chat.delete()
            return True
        except Exception as e:
            print "Error deleting chat: "+e.__str__().decode('utf-8')
            return e

    @classmethod
    def add_env_id(cls,chat_id,env_id,response=None):
        try:
            chat = Chat.objects(id=chat_id).first()
            print "chat second user: ", chat.second_user
            chat.env_id = env_id
            print "se agrego env_id from offer: ", chat.env_id
            chat.save()
        except Exception as e:
            error = "Error adding env_id: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'Chat',
                    method = 'add_env_id - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')

        return response

    @classmethod
    def get_chat_from_env_id(cls, env_id, response = None):
        if env_id is not None:
            try:
                response = Chat.objects(env_id__exact=env_id).first()
            except Exception as e:
                error = "Error getting chat from env_id: "+e.__str__().decode('utf-8')
                print error
                save_error(module = 'Chat',
                    method = 'get_chat_from_env_id - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')

        return response

    @classmethod
    def get_chat_by_id(cls,chat_id,response=None):
        """Get a chat for a given chat_id"""
        if chat_id is not None:
            try:
                response = Chat.objects(id=chat_id).first()
            except Exception as e:
                error = "Error getting chat from chat_id: "+e.__str__().decode('utf-8')
                print error
                save_error(module = 'Chat',
                    method = 'get_chat_by_id - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')
        return response

class Message(EmbeddedDocument):

    text = StringField()
    user_send= StringField()
    date = DateTimeField(default=datetime.datetime.now)




      