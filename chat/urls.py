from django.conf.urls.defaults import patterns, url
from views import home,message,create_chat,retrieve_chat,retrieve_message,delete_chat


urlpatterns = patterns('',
    #url(r'^login/$', auth_views.login,{'template_name': 'registration/login.html'},name='auth_login'),    #URL TO LOGIN
    #url(r'^logout/$',auth_views.logout,{'template_name': 'registration/logout.html'},name='auth_logout'), #URL TO LOGOUT VIA AUTH
    url(r'^$',
        home,
        name='home'),
    url(r'^m/$',
        message,
        name='message'),                    #URL TO STEP 1 OF USER REGISTER    
     url(r'^create_chat/$',
        create_chat,
        name='create_chat'),
    url(r'^retrieve_chat/$',
        retrieve_chat,
        name='retrieve_chat'),
    url(r'^retrieve_message/$',
        retrieve_message,
        name='retrieve_message'),
    url(r'del_chat/$',
        delete_chat,
        name="delete_chat"),                #URL TO DELETE A GIVEN CHAT               
)

