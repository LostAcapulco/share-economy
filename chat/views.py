#!/usr/bin/python
# -*- encoding: utf-8 -*-

from django.shortcuts import render_to_response
from django.conf import settings
from django.http import HttpResponse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils.safestring import mark_safe
from django.contrib.auth.decorators import login_required

from documents import *
from user.documents import RegistrationProfile
from comments.activity import Activity
from notifications.views import general_notification
from errorReport.views import save_error

from mongoengine.queryset import *
from mongoengine import StringField, ListField, Document
from mongoengine import EmbeddedDocumentField, EmbeddedDocument, GeoPointField

import pusher
import datetime, time

p = pusher.Pusher(
              app_id=settings.PUSHER_APP_ID,
              key=settings.PUSHER_APP_KEY,
              secret=settings.PUSHER_APP_SECRET
            )

@login_required(login_url='/account/login/')
def home(request):
    if not request.session.get('user'):
        request.session['user'] = 'user'
    return render_to_response('chat/home.html', {
                              'PUSHER_KEY':settings.PUSHER_APP_KEY,
                            }, RequestContext(request))

@login_required(login_url='/account/login/')
def message(request):

    if request.POST:
        from user.documents import RegistrationProfile as obj_user
        user_session = request.session['user']
        try:
            user_session_id = user_session.id.__str__().decode('utf-8')
            user_session_username=user_session.username
            user_session_picture = user_session.picture
            
            chat_id = request.POST.get('chat_id')

            mssg=str(request.POST.get('message'))
            mssg = mark_safe(mssg.__str__().decode('utf-8'))

            chat = Chat.get_chat_by_id(chat_id)
            
            if user_session_id != chat.first_user:
                sec_user = chat.first_user
            else:
                sec_user = chat.second_user

            print "sesion", user_session_id, "secod", sec_user

            sec_user = obj_user.get_user(sec_user)

            sec_user_pic = sec_user.picture
            
            user_second_username = sec_user.username
            user_second_id = str(sec_user.id)

            print 'session ', user_session.id, 'user_second ', user_second_id
            message = str(mssg.__str__().decode('utf-8'))+'#'+chat_id.__str__().decode('utf-8')
            message = message+'#'+user_second_id.__str__().decode('utf-8')+'#'+user_session_id

            activity_type = "activity"
            activity = Activity(activity_type, message, user_session_id).get_message()

            p[user_second_id].trigger(activity_type,activity)
            #p[user_session_id].trigger(activity_type,activity)

            msg=Chat.add_message_by_chat_id(chat_id,user_session_id,mssg.__str__().decode('utf-8'))

            general_notification(request,channel_id=user_second_id,
                                 user_id=user_session_id,
                                 user_name=user_session_username,
                                 user_pic=user_session_picture,
                                 action_trigered="te ha escrito un mensaje",
                                 target_id=user_second_id,
                                 target_name=user_second_username,
                                 message=user_session_username+" te ha escrito un mensaje",
                                 enviorment='chat',
                                 is_read='0')

        except Exception as e:
            error = "Error sending chat message: "+e.__str__().decode('utf-8')
            print error

            save_error(module = 'Chat',
                    method = 'message - view',
                    error = error,
                    user_name = user_session.username,
                    user_id = user_session.id.__str__().decode('utf-8'))

    return HttpResponse('')

@csrf_exempt
def create_chat(request,msg=None):
    user_session = request.session['user']
    user_id = user_session.id
    user_send = str(user_id)
    try:

        if request.POST:

            user_second = request.POST.get('second_user')

            user = user_session.username
            message = request.POST.get('message')
            message = message.encode('utf-8')

            p[user_second].trigger('message', {'message':message,'user':user,})

            msg = Chat.new_chat(first_user=user_send,second_user=user_second,message=str(message),type_chat='')
            msg = msg.id.__str__().decode('utf-8')

            user_sec = User.objects(id=user_second).first()
            user_sec_name = user_sec.username 

            general_notification(request,channel_id=user_second,
                                     user_id=user_send,
                                     user_name=user,
                                     user_pic=user_session.picture,
                                     action_trigered="ha escrito un mensaje",
                                     target_id=str(user_second),
                                     target_name=user_sec_name,
                                     message=user+" te ha escito un mensaje",
                                     enviorment='chat',
                                     is_read='0')
    except Exception as e:
        error = "Error creating chat: "+e.__str__().decode('utf-8')
        print error
        save_error(module = 'Chat',
                    method = 'create_chat - view',
                    error = error,
                    user_name = user_session.username,
                    user_id = user_session.id.__str__().decode('utf-8'))

    return HttpResponse(msg)

@csrf_exempt
@login_required(login_url='/account/login/')
def retrieve_chat(request):
    chat=[]
    if request.POST:
        user_owner = request.POST['user']
        msg = Chat.objects(Q(first_user=user_owner) |  Q(second_user=user_owner)).order_by('-date').all()
        for item in msg:
            try:
                if item.second_user != 'None':
                    info={}
                    txt_chat= item.message[len(item.message) - 1].text
                    user = User.objects(id=user_owner).first()
                    user.picture
                    id_chat= str(item.id)
                    data=[]
                    # Search second user
                    user_session = request.session['user']
                    user_id = user_session.id.__str__().decode('utf-8')
                    if item.second_user != user_id:
                        second_user_id = item.second_user
                    else:
                        second_user_id = item.first_user

                    second_user = User.objects(id=second_user_id).first()
                    second_username = second_user.username
                    second_user_picture = second_user.picture
                    info.update({
                                'title':item.title,
                                'first_user':item.first_user,
                                'second_user':second_user_id,
                                'second_username':second_username,
                                'second_user_picture': second_user_picture,
                                'username':user.username,
                                'picture':user.picture,
                                'id':id_chat,
                                'txt':txt_chat,
                                'date':str(item.date),
                                'env_id':item.env_id
                              })

                chat.append([info])
            except Exception as e:
                print e
    return HttpResponse(json.dumps({'chats':chat}))

@login_required(login_url='/account/login/')
def retrieve_chats(request,env_id):
    '''
    Retrive chats for product chat enviorment
    '''
    cht=[]
    user_session =  request.session['user']
   
    user_session_id = user_session.id.__str__().decode('utf-8')

    chats = Chat.objects(Q(env_id=env_id)&(Q(first_user=user_session_id) |  Q(second_user=user_session_id)))
    
    for chat in chats:
       
        try:
            
            if chat.second_user != 'None':
                
                info={}
                txt_chat= chat.message[len(chat.message) - 1].text
                user = User.objects(id=user_session_id).first()
                id_chat= str(chat.id)
                data=[]
                # Search second user
                
                if chat.second_user != user_session_id:
                    second_user_id = chat.second_user
                else:
                    second_user_id = chat.first_user
               
                second_user = User.objects(id=second_user_id).first()
                second_username = second_user.username
                second_user_picture = second_user.picture

                info.update({
                            'title':chat.title,
                            'first_user':chat.first_user,
                            'second_user':second_user_id,
                            'second_username':second_username,
                            'second_user_picture': second_user_picture,
                            'username':user.username,
                            'picture':user.picture,
                            'id':id_chat,
                            'txt':txt_chat
                          })

            cht.append([info])
        except Exception as e:
            print e
    return json.dumps({'chats':cht})

@login_required(login_url='/account/login/')
@csrf_exempt
def retrieve_message(request):
    messages=[]
    if request.POST:
        from user.documents import RegistrationProfile as obj_user
        user_session = request.session['user']
        user_session_id = user_session.id.__str__().decode('utf-8')
        chat_id=request.POST['search_id']

        chat = Chat.get_chat_by_id(chat_id)

        for message in chat.message:
            info={}
            if message.date is not None:
                date = message.date
            else:
                chat.date

            if user_session_id != chat.first_user:
                receptor = chat.first_user
            else:
                receptor = chat.second_user 
            receptor = obj_user.get_user(receptor)
            receptor = receptor.username
            user = obj_user.get_user(message.user_send)

            info.update({
                            'message':message.text,
                            'username':user.username,
                            'picture':user.picture,
                            'user_receptor':receptor,
                            'date':date.__str__().decode('utf-8')
                        })

            messages.append([info])

    return HttpResponse(json.dumps({'chats':messages}))

@login_required(login_url='/account/login/')
@csrf_exempt
def delete_chat(request,response=False):
    if request.POST:
        chat_id = request.POST.get('chat_id')
        response = Chat.delete_chat(chat_id)
    print response
    return HttpResponse(response)