#!/usr/bin/env python
# encoding: utf-8
"""
Views which allow users to create a group profile.

"""

from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from mongoengine.django.auth import User
from notifications.views import general_notification
from comments.documents import Comment
from user.storage_s3 import *
from models import Group
from mongoengine.queryset import *
from documents import *
from django.utils import encoding
from array import *
from product.documents import Product
from notifications.documents import Notification

from forms import GroupRegistrationForm

import json

@login_required(login_url='/account/login/')
def create_group(request, group_id=None, tmpl_nm='reg_group_info.html',form_class=GroupRegistrationForm):
    """
    Allow a -user- to create a group
    """

    form = form_class(data=request.POST, files=request.FILES)
    if request.method == 'POST' and form.is_valid():
        user_session = request.session['user']
        user_id = user_session.id.__str__().decode('utf-8')
        user_group = form.save(user_id,group_id)
        if user_group is not None:
            request.session['group'] = user_group
            group_id = user_group.id.__str__().decode('utf-8')
            request.session['group_id'] = group_id
            return redirect('/group/profile/'+group_id+'/')
        else:
            """
            Si exite un error:
            Consultar en mongo si el Gupo ha sido guardado.
            y desplegar el perfil en caso de que asi sea.
            """
            return HttpResponse('Error al guardar el grupo')
    elif group_id is not None:
        group = Group.objects(id=group_id).first()
        if group is not None: 
            extra_context = {"group_id":group.id.__str__().decode('utf-8'),
                             "group_name":group.group_name,
                             "about_group":group.about_group}
            to_response_extra = response_none_valid(request,tmpl_nm,extra_context)
            return render_to_response(to_response_extra['template_name'],
                                      {'form':form,'group_id':group_id},
                                      context_instance=to_response_extra['context_instance'])
        else:
            redirect("/group/create/")
    else:
        extra_context = {}
        to_response_extra = response_none_valid(request,tmpl_nm,extra_context)
        return render_to_response(to_response_extra['template_name'],
                                  {'form':form,},
                                  context_instance=to_response_extra['context_instance'])

@login_required(login_url='/account/login/')
def group_profile(request, group_id=None, template = "gprofile.html"):
    """
    Display a -Group profile- for a loged -user-
    """
    user_session = request.session['user']
    user_id = user_session.id.__str__().decode('utf-8')
    print group_id

    request.session['group_id'] = group_id

    try:
        group = Group.objects(id=group_id.__str__().decode("utf-8")).first()
    except:
        template="not_found.html"
        return render_to_response(template)

    if group.user_id == user_id:
        owner = 'owner'
    else:
        owner = 'no-owner'

    name = group.group_name
    typeg = group.type_group
    about = group.about_group

    group_products = group.group_products
    chunck_products = []

    if len(group_products) > 0:
        for p in group_products:

            product = Product.objects(id=p.product_id).first()

            if product is not None:
                tags = {}
                category = {}
                
                user_product = {}
                user_product.update({'product_name':product.product_name})
                user_product.update({'product_id':product.id})
                user_product.update({'product_img':product.pictures[0].url })
                for c in product.categories:
                    category.update({'category_1': c.first_cat})
                    category.update({'category_2': c.second_cat})
                user_product.update({'categories':category})
                for t in product.tags:    
                    tags.update({'tags_1':t.first_tag})
                    tags.update({'tags_2':t.second_tag})
                    tags.update({'tags_3':t.third_tag})
                user_product.update({'product_tags':tags})
                if product.shares is not None:
                    user_product.update({'product_shares':product.shares})
                    
                chunck_products.append([user_product])

    context = RequestContext(request)
    comments = group_comments(group_id)
    return render_to_response(template,{'owner':owner,
                                        'group_name':name,
                                        'type_group':typeg,
                                        'about_group':about,
                                        'group_id':group_id,
                                        'comments':comments,
                                        'PUSHER_KEY': settings.PUSHER_APP_KEY,
                                        'products':chunck_products
                                        }
                                        ,context)

@csrf_exempt
def products_group(request):
    group_id = request.POST.get('group')

    group = Group.objects(id=group_id).first()
    group_products = group.group_products
    chunck_products = []

    if len(group_products) > 0:
        for p in group_products:

            product = Product.objects(id=p.product_id).first()

            if product is not None:
                tags = {}
                category = {}
                
                user_product = {}
                user_product.update({'product_name':product.product_name})
                user_product.update({'product_id':product.id.__str__().decode('utf-8')})
                user_product.update({'product_img':product.pictures[0].url })
                for c in product.categories:
                    category.update({'category_1': c.first_cat})
                    category.update({'category_2': c.second_cat})
                user_product.update({'categories':category})
                for t in product.tags:    
                    tags.update({'tags_1':t.first_tag})
                    tags.update({'tags_2':t.second_tag})
                    tags.update({'tags_3':t.third_tag})
                user_product.update({'product_tags':tags})
                if product.shares is not None:
                    user_product.update({'product_shares':product.shares})
                    
                chunck_products.append([user_product])

    return HttpResponse(json.dumps({'products':chunck_products}))

@csrf_exempt
def users_group(request):
    group_id = request.POST.get('group')

    group = Group.objects(id=group_id).first()
    group_members = group.members
    
    chunck_members = []

    user = User.objects(id=group.user_id).first()

    try:
        address = user.address[0].country
    except:
        address = "Sin especificar"

    list_member = {}
    list_member.update({'friend_id':user.id.__str__().decode('utf-8')})
    list_member.update({'friend_name':user.username})
    list_member.update({'friend_pic':user.picture})
    list_member.update({'friend_rating':user.rating})
    list_member.update({'friend_country':address})
    list_member.update({'friend_friends':len(user.friends)})

    chunck_members.append([list_member])

    if len(group_members) > 0:
        for u in group_members:
            try:
                id_user = u.uid
            except:
                id_user = None
            if id_user is not None and int(u.invite_type) > 0:

                user = User.objects(id=u.uid).first()

                try:
                    address = user.address[0].country
                except:
                    address = "Sin especificar"

                list_member = {}
                list_member.update({'friend_id':user.id.__str__().decode('utf-8')})
                list_member.update({'friend_name':user.username})
                list_member.update({'friend_pic':user.picture})
                list_member.update({'friend_rating':user.rating})
                list_member.update({'friend_country':address})
                list_member.update({'friend_friends':len(user.friends)})

                chunck_members.append([list_member])


    return HttpResponse(json.dumps({'friends':chunck_members}))

def group_comments(group_id=None):
    comments = Comment.objects(Q(enviorment_type="group_profile") & Q(enviornment_id=group_id)).all()
    chunck_comments = []

    if comments:
        for comment in comments:
            info_comment = {}
            info_comment.update({'user_id':comment.user_id})
            info_comment.update({'user_name':comment.user_name})
            info_comment.update({'user_pic':comment.user_pic})
            info_comment.update({'date':comment.date})
            info_comment.update({'text':comment.text})
            chunck_comments.append([info_comment])

    return chunck_comments

@csrf_exempt
def groups_user(request,type_group=None):

    user_session = request.session['user']
    user_session_id = user_session.id.__str__().decode('utf-8')
    if request.POST:
        type_group = request.POST.get('type_group')

    if type_group is None or type_group == 'all':
        groups = Group.objects(Q(user_id=user_session_id) | Q(members__uid=user_session_id)).all()
    elif type_group == 'close' or type_group == 'open':
        groups = Group.objects(Q(type_group=type_group) & Q(user_id=user_session_id)).all()
        print groups
    elif type_group == 'search':
        search_group = request.POST.get('search')
        print search_group
        groups = Group.objects(group_name__icontains=search_group).all()

    if groups is not None:
        arrGroups = {}
        chunck_groups = []
        for group in groups:
            user_group = {}
            members = {}
            user_group.update({'group_name':group.group_name})
            user_group.update({'group_id':group.id.__str__().decode('utf-8')})
            user_group.update({'group_type':group.type_group})
            user_group.update({'about_group':group.about_group})
            user_group.update({'group_members':len(group.members)})
            user_group.update({'group_products':group.group_products.__len__()})
            for m in group.members:    
                members.update({'uid':m.uid})
                members.update({'friend_pic':m.friend_pic})
            chunck_groups.append([user_group])

        arrGroups.update({'groups':chunck_groups})
    if request.POST:
        return HttpResponse(json.dumps({'groups':chunck_groups}))
    else:
        return ({'all_groups':arrGroups})

@csrf_exempt
def list_invite_friends(request):
    arrFriends = {}
    if request.POST:
        profile_id = request.POST.get('profile_id')
        user = User.objects(id=profile_id).first()
        friends = user.friends
        try:
            group_id = request.POST.get('group_id')
            group = Group.objects(id=group_id).first()
            group_members = group.members
        except:
            group_members = []
        invite = []
        flag = False
        for f in friends:
            for g in group_members:
                if f.uid != g.uid:
                    flag = True
                else:
                    flag = False
                    break
            if flag or len(group_members) == 0:
                invite.append(f)

        if invite is not None:
            chunck_invite = []
            for i in invite:
                list_user_friend = {}
                list_user_friend.update({'friend_id':i.uid})
                list_user_friend.update({'friend_name':i.friend_name})
                list_user_friend.update({'friend_pic':i.friend_pic})

                chunck_invite.append([list_user_friend])

            arrFriends.update({'friends':chunck_invite})
    return HttpResponse(json.dumps(arrFriends))

@csrf_exempt
def send_invite_join(request):
    if request.POST:
        try:
            user_id = request.POST.get('user')
            group_id = request.POST.get('group')
            invite_type = request.POST.get('invite_type')

            user = User.objects(id=user_id).first()
            group = Group.objects(id=group_id).first()

            if invite_type == '1':
                message = unicode("Tienes una invitaci\xc3\xb3n al grupo ",'utf-8')
            else:
                message = unicode("Tienes una invitaci\xc3\xb3n para ser moderador del grupo",'utf-8')

            message = message+group.group_name

            general_notification(request,
                            channel_id=user_id,
                            user_id=user_id,
                            message=message,
                            user_name=user.username,
                            user_pic=user.picture,
                            action_trigered=message, 
                            target_id=group_id,
                            target_name=group.group_name,
                            enviorment='group',
                            is_read='0')

            response = "Invitación enviada"
        except Exception as e:
            print e
            response = "Hubo un error al enviar la invitación"

    return HttpResponse(response)

@login_required(login_url='/account/login/')
def form_response_invite(request,group_id=None,template='form_response_invite.html'):
    group = Group.objects(id=group_id).first()
    group_name = group.group_name
    context = RequestContext(request,{'group_id':group_id,'group_name':group_name})
    return render_to_response(template,context)

@csrf_exempt
def response_invite_group(request):
    if request.POST:
        group_id = request.POST.get('group_id')
        response = request.POST.get('response')
        user_id = request.POST.get('user_id')
        user_name = request.POST.get('user_name')
        user_pic = request.POST.get('user_pic')
        invite_type = request.POST.get('invite_type')
        group = Group.objects(id=group_id).first()
        if response == 'acept':
            try:
                gpo = Group.add_member(group_id,
                                       uid=user_id,
                                       friend_name=user_name,
                                       friend_pic=user_pic,
                                       invite_type=invite_type)
                if invite_type == '1':
                    message = user_name+" se ha unido ha "+group.group_name
                else:
                    message = user_name+" ahora es moderador de "+group.group_name
                general_notification(request,
                                     channel_id=group.user_id,
                                     user_id=user_id,
                                     message=message, 
                                     user_name=user_name,
                                     user_pic=user_pic,
                                     action_trigered='se unio al grupo', 
                                     target_id=group_id,
                                     target_name=group.group_name,
                                     enviorment='group',
                                     is_read='0')

                action_trigered = "Tienes una invitación al grupo "+group.group_name
                notification = Notification.objects(Q(action_trigered=action_trigered)&Q(user_name=user_name)).first()
                notification.delete()
                result = 'acept'
            except Exception as e:
                print e
                result = "Tu solicitud no pudo ser procesada"
        else:
            try:
                action_trigered = "Tienes una invitación al grupo "+group.group_name
                notification = Notification.objects(Q(action_trigered=action_trigered)&Q(user_name=user_name)).first()
                notification.delete()
                result = 'cancel'
            except:
                result = "Tu solicitud no pudo ser procesada"

    return HttpResponse(result)

def group_member(request):
    group_id = ""
    group = Group.objects(id=group_id).first()
    members = {}
    for m in group:
        members.update({'uid':m.uid})
        members.update({'friend_pic':m.friend_pic})
    
    return members

def response_none_valid(request, tmpl_nm, extra_context=None):
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    to_response_extra = {'template_name':tmpl_nm,
                         'context_instance':context}

    return to_response_extra

def all_groups(request, template='list_groups.html'):
    
    context = RequestContext(request,{'PUSHER_KEY':settings.PUSHER_APP_KEY,},[groups_user])
    
    return render_to_response(template,context)

def search_mini(request):
    user_session = request.session['user']
    user_id = user_session.id.__str__().decode('utf-8')
    info=[]
    owner=False
    member=False
    group={}
    name_search = request.POST.get('name_search')
    groups = Group.objects(group_name__icontains="Iphons").all()
    for item in groups:
        name = item.group_name
        if item.user_id == user_id:
            owner=True
        members = len(item.members)
        for item in item.members:
            if item.uid == user_id:
                member=True
        total_result = groups.count()
        group.update({'title':name,'picture':members,'user':total_result,'owner':owner,'member':member})
    info.append([group])
    return HttpResponse(info)

@csrf_exempt
def delete_group(request):
    if request.POST:
        group_id = request.POST.get('group_id')
        group = Group.objects(id=group_id).first()

        if group is not None:
            group.delete()

            group = Group.objects(id=group_id).first()

            resp = group is None
        else:
            resp = False
    else:
        resp = False
            
    return HttpResponse(resp)

@csrf_exempt
def changue_owner(request):
    if request.POST:
        group_id = request.POST.get('group_id')
        owner_id = request.POST.get('owner_id')
        # Search group
        group = Group.objects(id=group_id).first()
        if group is not None:
            # Search info user of owner group
            user = User.objects(id=group.user_id).first()
            # Add old owner in members
            gpo = Group.add_member(group_id,
                                       uid=user.id.__str__().decode('utf-8'),
                                       friend_name=user.username,
                                       friend_pic=user.picture,
                                       invite_type="1")
            # Change owner gruop
            group.user_id = owner_id
            
            for item in group.members:
                if item.uid == owner_id:
                    item.invite_type="0"
                    group.save()

            group = Group.objects(id=group_id).first()

            resp = owner_id == group.user_id
        else:
            resp = False

        return HttpResponse(resp)

@csrf_exempt
def out_group(request):
    resp=False
    if request.POST:
        group_id=request.POST.get('group_id')
        user_id=request.POST.get('user_id')
        group = Group.objects(id=group_id).first()
        for item in group.members:
            if item.uid == user_id:
                item.invite_type="0"
                group.save()
                resp=True
    return HttpResponse(resp)

@csrf_exempt
def add_product_in_group(request):
    if request.POST:
        try:
            product_id = request.POST.get('product_id')
            product_owner_id = request.POST.get('product_owner_id')
            group_id = request.POST.get('group_id')

            listProduct = Product.objects(id=product_id).first()
            
            product_name = listProduct.product_name
            product_pic = listProduct.pictures[0].url

            Group.add_product_group(group_id,
                                    product_id=product_id,
                                    product_name=product_name,
                                    product_pic=product_pic,
                                    product_owner_id=product_owner_id)
            response = True
        except Exception as e:
            response = "No pudimos procesar tu peticion"

    return HttpResponse(response)

@csrf_exempt
def find_groups(request, group_name=None):
    if group_name is not None:
        groups = Group.objects(group_name__icontains=group_name).all()
    else:
        groups = Group.objects().all()

    chunck_groups = []
    if groups is not None:
        for group in groups:
            list_group = {}
            members = {}
            chunck_members = []
            list_group.update({'group_name':group.group_name})
            list_group.update({'group_id':group.id.__str__().decode('utf-8')})
            list_group.update({'group_type':group.type_group})
            list_group.update({'about_group':group.about_group})
            list_group.update({'group_members_num':group.members.__len__()})
            list_group.update({'group_products':group.group_products.__len__()})
            for m in group.members:    
                members.update({'uid':m.uid})
                members.update({'friend_pic':m.friend_pic})
                chunck_members.append([members])
            list_group.update({'group_members':chunck_members})
            chunck_groups.append([list_group])

    return chunck_groups