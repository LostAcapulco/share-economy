from django.conf.urls.defaults import patterns, url
from views import *
urlpatterns = patterns('',
    url(r'^create/$',
        create_group,
        name='create_group'),                  #URL FOR A GROUP REGISTER
    url(r'^edit/(?P<group_id>\w+)/$',
        create_group,
        name='edit_group'),                    #URL TO EDIT AN EXISTING GROUP
    url(r'^profile/(?P<group_id>\w+)/$',
        group_profile,
        name='group_profile'),                 #URL TO GROUP PROFILE
    url(r'^search/$',
        groups_user,
        name='search_group'),                    #URL TO GET GROUPS BY SEARCH
    url(r'^search/(?P<type_group>\w+)/$',
        groups_user,
        name='groups_search'),                    #URL TO GET GROUPS BY SEARCH
    url(r'^all/$',
        all_groups,
        name='all_grups'),
    url(r'^search_mini/$',
        search_mini,
        name='search_mini'),
    url(r'^invite/$',
        list_invite_friends,
        name='invite_friends'),                 #URL TO INVITE FRIENDS JOIN TO A GROUP
    url(r'^res-invite/',
        response_invite_group,
        name='response_invite_group'),          #URL TO RESPONSE IVITE TO JOIN A GROUP
    url(r'^send_invite/$',
        send_invite_join,
        name='send_invite_join'),               #URL TO SEND INVITE TO JOIN A GROUP
    url(r'^changue/$',
        changue_owner,
        name='changue_owner'),
    url(r'^out/$',
        out_group,
        name='out_group'),
    url(r'^add_product/$',
        add_product_in_group,
        name='add_product_in_group'),
    url(r'^products_group/$',
        products_group,
        name='products_group'),
    url(r'^users_group/$',
        users_group,
        name='users_group'),
    url(r'^invite-resp/(?P<group_id>\w+)/$',
        form_response_invite,
        name='form_response_invite'),
    url(r'^delete/$',
        delete_group,
        name='delete_group'),
    )
