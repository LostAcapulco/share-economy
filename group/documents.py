from mongoengine import StringField, Document, EmbeddedDocumentField, EmbeddedDocument, ListField, DateTimeField
from mongoengine.django.auth import User
from mongoengine.queryset import *
import datetime, time

class Group(Document):
    """
    A simple group profile which stores group info
    """
    user_id = StringField()
    group_name = StringField(max_length=40)
    type_group = StringField(max_length=40)
    about_group = StringField()
    members = ListField(EmbeddedDocumentField("Member_Group"))
    invites = ListField(EmbeddedDocumentField("Invite"))
    group_products = ListField(EmbeddedDocumentField("Group_product"))
    date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def manage_edit_create(cls, user_id, group_name, about_group,type_group, group_id=None):
        if group_id is None:
            user_group = cls.create_group(user_id=user_id,
                                          group_name=group_name,
                                          about_group=about_group,
                                          type_group=type_group)
        else:
            user_group = cls.update_group(user_id=user_id,
                                          group_name=group_name,
                                          about_group=about_group,
                                          type_group=type_group,
                                          group_id=group_id)

        return user_group

    @classmethod
    def create_group(cls, **kwargs):
        """
        Create a new, -Group- related to a User.

        """

        group = cls()
        group.save_group(group,**kwargs)
        group.save()

        return group

    @classmethod
    def update_group(cls, **kwargs):

        user_group = cls.objects(id=kwargs['group_id']).first()
        user_group.user_id = kwargs['user_id']
        user_group.group_name = kwargs['group_name']
        user_group.type_group = kwargs['type_group']
        user_group.about_group = kwargs['about_group']
        user_group.save()

        return user_group

    @classmethod
    def save_group(cls,group, **kwargs):

        group.user_id = kwargs['user_id']
        group.group_name = kwargs['group_name']
        group.about_group = kwargs['about_group']
        group.type_group = kwargs['type_group']
        
        member = Member_Group()
        member.member_id = kwargs['user_id']
        user = User.objects(id=kwargs['user_id']).first()
        username = user.username
        member.member_name = username
        group.members.append(member)
        
    @classmethod
    def add_member(cls,*args, **kwargs):
        group = cls.objects(id=args[0]).first()
        member = Member_Group(**kwargs)
        group.members.append(member)
        group.save()
        return member

    @classmethod
    def add_product_group(cls,*args, **kwargs):
        try:
            group = cls.objects(id=args[0]).first()
            exist = cls.objects(group_products__product_id=kwargs['product_id']).first()
            if exist is None:
                group_product = Group_product(**kwargs)
                group.group_products.append(group_product)
                group.save()
            response = group
            
        except Exception as e:
            print e
            response = e

        return response

    @classmethod
    def delete_product(cls,group_id,product_id):
        group = cls.objects(Q(members__product_id=product_id) & Q(id=group_id)).first()
        group.drop_collection()

    @classmethod
    def delete_member(cls,group_id,member_id):
        group = cls.objects(Q(members__member_id=member_id) & Q(id=group_id)).first()
        group.drop_collection()
    
    @classmethod
    def delete_invite(cls,group_id,user_id):
        group = cls.objects(Q(id=group_id) & Q(invites__user_id=user_id)).first()
        group.drop_collection()

class Member_Group(EmbeddedDocument):    
    uid=StringField(max_length=300)
    friend_name = StringField(max_length=90)
    friend_pic = StringField(max_length=400)
    invite_type = StringField(max_length=1)
    date = DateTimeField(default=datetime.datetime.now)
    
class Invite(EmbeddedDocument):
    user_id = StringField()

class Group_product(EmbeddedDocument):
    product_id = StringField(max_length=300)
    product_name = StringField(max_length=90)
    product_pic = StringField(max_length=400)
    product_owner_id = StringField(max_length=300)