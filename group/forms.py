#!/usr/bin/python
# vim: set fileencoding= utf-8 :

from django import forms
from django.utils.translation import ugettext_lazy as _

from models import *

attrs_dict = {'class': 'required'}

CHOICES=[('open','Abierto'),
         ('close','Cerrado')]

class GroupRegistrationForm(forms.Form):
    """
    Form for upload a prodcut to your inventory

    """
    group_name = forms.CharField( max_length=40,
                                widget=forms.TextInput(attrs={'placeholder':'Nombre del Grupo...', 'class':'left required bg-gray', 'data-type':'text'}),
                                label=_(u'Nombra tu grupo.\n (Ejemplos: Interkamvio de tarjetas de baseball)'),required=True)

    about_group = forms.CharField(  max_length=500,
                                    widget=forms.Textarea(attrs={'palceholder':'Descripción...', 'class':'left required bg-gray', 'data-type':'text'}),
                                    label=_(u'Describe tu Grupo.(Cuenta de que va tu Grupo y si hay reglas o especificaciones)'),
                                    required=True)
    type_group = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect(), label=_(u'Elije el tipo de grupo'))
    stp1_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'form_id','value':'{[form_id="form_id_31"]}'}),required=False)
    nxt_stp_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'kamvia_next_id', 'value':'{[kamvia_next_id]}'}),required=False)

    def save(self, user_id, group_id):

        new_group = Group.manage_edit_create(user_id,
                                       self.cleaned_data['group_name'],
                                       self.cleaned_data['about_group'],
                                       self.cleaned_data['type_group'],
                                       group_id)

        NodeGroup.create_node_group(self.cleaned_data['group_name'],
                                    user_id,
                                    self.cleaned_data['about_group'],
                                    self.cleaned_data['type_group'])

        return new_group
