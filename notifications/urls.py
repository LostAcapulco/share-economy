from django.conf.urls.defaults import patterns, url
from views import test_not, post_notification, auth_notification, get_notification, general_notification, count_notification


urlpatterns = patterns('',
    url(r'^test/$',
        test_not,
        name='test_not'),                  #URL FOR A RENDER FORM THAT SEND A NOTIFICATION
                       
    url(r'^send/$',
        post_notification,
        name='notification'),              #URL TO SEND NOTIFICATION
    url(r'^auth/$',
        auth_notification,
        name='auth_notification'),          #URL TO AUTHENTICATE A GIVEN USER OVER NOTIFICATION
    url(r'^get/$',
        get_notification,
        name='get_notification'),
    url(r'^count_notification/$',
        count_notification,
        name='count_notification')
)