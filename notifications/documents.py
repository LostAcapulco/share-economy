from mongoengine import StringField, DateTimeField, Document
from mongoengine.queryset import *

import datetime, time

class Notification(Document):
    """
    A feed document for the hole system
    """
    channel_id = StringField()
    user_id = StringField()
    user_name = StringField()
    user_pic = StringField()
    action_trigered = StringField()
    target_id = StringField()
    target_name = StringField()
    enviorment = StringField()
    is_read = StringField()
    message = StringField()
    env_id = StringField()
    date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def get_all_notification(cls,user_id,response=None):
        try:
            response = cls.objects(user_id=user_id).all()
        except Exception as e:
            print "Error getting all notifications: ", e
        
        return response

    @classmethod
    def get_friend_request_not(cls, user_id, target_id, response=None):
        try:
            response = cls.objects(Q(user_id=user_id)&Q(target_id=target_id)&Q(action_trigered='envio una solicitud amistad')).first()
        except Exception as e:
            print "Error getting friend request notification document: ", e

        return response

    @classmethod
    def add_notification(cls,**kwargs):
        try:
            notify = cls(**kwargs)
            notify.save()
        except Exception as e:
            print "Error adding notification on document: ", e

        return notify

    @classmethod
    def delete_notification(cls, not_id, response=None):

        try:
            notification = cls.objects(id=not_id).first()
            notification.delete()
            
            response =  True
        except Exception as e:
            print "Error deleting notification: ", e
            response = e 
        return response

    @classmethod
    def delete_notification(cls, date, response=None):
        """Borra notificaciones por fecha"""
        try:
            notification = cls.objects(date__gte=date).all()
            print notification
            for notif in notification:
                print notif.date
                notif.delete()
            
            response =  True
        except Exception as e:
            print "Error deleting notification: ", e
            response = e 
        return response