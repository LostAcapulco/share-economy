#!/usr/bin/python
# -*- encoding: utf-8 -*-

from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from errorReport.views import save_error
from documents import Notification
from user.documents import RegistrationProfile
from django.core.mail import send_mail
from email import utils

import pusher , json, datetime, time


pusher.app_id = settings.PUSHER_APP_ID
pusher.key = settings.PUSHER_APP_KEY
pusher.secret = settings.PUSHER_APP_SECRET

p = pusher.Pusher()

#@login_required(login_url='/account/login/')
def test_not(request, template='test_not.html'):
    
    return render_to_response(template, 
                              {'PUSHER_KEY': settings.PUSHER_APP_KEY,}, 
                              RequestContext(request))
 
  
def add_notification(**kwargs):
    

    try:
        Notification.add_notification(**kwargs)
           
        response = True

    except Exception as e:

        error =  "Error adding notification: " + e.__str__().decode('utf-8')
        print error
        save_error(module = 'notification',
                    method = 'add_notification - view',
                    error = error,
                    user_name = 's/n',
                    user_id = 's/id')
            
        response = e
            
    return response

@csrf_exempt
def get_notification(request):
    array_notifications = {}
    if request.POST:
        channel_id = request.POST.get('channel_id')
        
        notifications = Notification.objects(channel_id__exact=channel_id).order_by('-date').all()

        if notifications is not None:

            chunck_notifications = []
            for notify in notifications:
                user_notify = {}
                user_notify.update({'notification_id':notify.id.__str__().decode('utf-8')})
                user_notify.update({'user_id':notify.user_id})
                user_notify.update({'user_pic':notify.user_pic })
                user_notify.update({'user_name':notify.user_name})
                user_notify.update({'action_trigered':notify.action_trigered})
                user_notify.update({'target_id':notify.target_id})
                user_notify.update({'target_name':notify.target_name})
                user_notify.update({'enviorment':notify.enviorment})
                user_notify.update({'env_id':notify.env_id})
                dt = notify.date.timetuple()
                dt = time.mktime(dt)
                user_notify.update({'date':utils.formatdate(dt)})
                chunck_notifications.append([user_notify])
            array_notifications.update({'notifications':chunck_notifications})

            notifications_no_readed = Notification.objects(channel_id__exact=channel_id, is_read="0").all()

            for n in notifications_no_readed:
                n.is_read = "1"
                n.save()



    return HttpResponse(json.dumps(array_notifications))

@csrf_exempt
def count_notification(request):
    user_session = request.session['user']
    chanel_id = user_session.id.__str__().decode('utf-8')

    notifications = Notification.objects(channel_id__exact=chanel_id, is_read="0").order_by('-date').all()
    return HttpResponse(json.dumps(len(notifications)))

@csrf_exempt
#@login_required(login_url='/account/login/')
def post_notification(request):
    if request.POST.get('message'):
        message = request.POST.get('message')
    else:
        message = 'there is no message via form!!'
    p['noti-kamvia'].trigger('notification',{'message':message})
        
    return HttpResponse('')

@csrf_exempt
@login_required(login_url='/account/login/')
def auth_notification(request):
    channel_name = request.POST.get('channel_name')
    socket_id = request.POST.get('socket_id')
    
    auth = p[channel_name].authenticate(socket_id)
    json_data = json.dumps(auth)

    return HttpResponse(json_data)

def general_notification(request,**kwargs):
    """
    Allow to the system send notification about general user actions
    """

    if kwargs['user_id'] is not None:
        user_session = request.session['user']
        user_session_name = user_session.username
        try:
            response = add_notification(**kwargs)

            if response == True:
                channel = kwargs['channel_id']
                message = kwargs['message']
                enviorment = kwargs['enviorment']
                tit_not = kwargs['action_trigered']
                
                user = RegistrationProfile.get_user(channel)

                send_mail_notification(user, user_session_name, message, response)

                p[str(channel)].trigger('notification',{'message':message,'enviorment':enviorment})
        except Exception as e:
            error = "Error pushing notification: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'notification',
                        method = 'general_notification - view',
                        error = error,
                        user_name = user.username,
                        user_id = user.id.__str__().decode('utf-8'))
    return HttpResponse('')

def general_notification_copy(sender,**kwargs):
    """
    Allow to the system send notification about general user actions
    """

    if kwargs['user_id'] is not None:
        user = sender
        user_session = request.session['user']
        user_session_name = user_session.username
        try:
            response = add_notification(**kwargs)

            if response == True:
                channel = kwargs['channel_id']
                message = kwargs['message']
                enviorment = kwargs['enviorment']
                
                user = RegistrationProfile.get_user(channel)

                send_mail_notification(user, user_session_name, message, response)

                p[str(channel)].trigger('notification',{'message':message,'enviorment':enviorment})
        except Exception as e:
            error = "Error pushing notification: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'notification',
                        method = 'general_notification - view',
                        error = error,
                        user_name = user.username,
                        user_id = user.id.__str__().decode('utf-8'))
    return HttpResponse('')


def send_mail_notification(user, user_action, notification, send_email=False):
    if send_email:
        try:
            from django.core.mail import EmailMultiAlternatives
            from django.template.loader import render_to_string

            subject, from_email, to = 'Actividad en tu cuenta de kamvia', settings.DEFAULT_FROM_EMAIL, user.email

            html_content = render_to_string('actividad_kamvia.html',{'user_action': user_action,'user':user.username, 'notification':notification})
            text_content = 'Actividad en tu cuenta de kamvia'
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        except Exception as e:
            error = "Error sending email notification: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'notification',
                        method = 'send_mail_notification - view',
                        error = error,
                        user_name = user.username,
                        user_id = user.id)

    