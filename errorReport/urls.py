from django.conf.urls.defaults import patterns, url
from views import check_access_report, get_reports, update_friend_request, delete_notification, search_fb_user_with_product

urlpatterns = patterns('',
    url(r'^report_auth/$',check_access_report,name='get_report'),
    url(r'^reports/$',get_reports,name='get_report'),
    url(r'^update_friend_request/$',update_friend_request,name='update_friend_request'),
    url(r'^delet_not/$',delete_notification,name='delete_notification'),
    url(r'^sear_fb_usrs/$',search_fb_user_with_product,name='search_fb_user_with_product'),
)