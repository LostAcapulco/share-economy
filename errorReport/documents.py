import datetime

from django.utils.encoding import smart_str
from mongoengine import *
from mongoengine import DateTimeField

class ErrorReport(Document):

    module = StringField()
    method = StringField()
    error = StringField()
    user_name = StringField()
    user_id = StringField()
    date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def set_error(cls, response=None, **kwargs):
    	try:
    		response = cls(**kwargs).save()
    	except Exception as e:
    		print "Error saving report error: ", e
    	return response

    @classmethod
    def get_all_errors(cls, response=None):
    	try:
    		response = cls.objects().order_by('-date').all()
    	except Exception, e:
    		print "Error gettin all errors: ", e

    	return response

    @classmethod
    def get_error_by_module(cls, module, response=None):
    	try:
    		response = cls.objects(module=module).first()
    	except Exception as e:
    		print "Error gettin error by module", e

    	return response

    @classmethod
    def get_error_by_id(cls, id, response=None):
    	try:
    		response = cls.objects(id=id).first()
    	except Exception as e:
    		print "Error getting error by id: ", e

    	return response

    @classmethod
    def get_error_by_user_id(cls, user_id, response=None):
    	try:
    		response = cls.objects(user_id=user_id).first()
    	except Exception, e:
    		print "Error gettin error by user id: ", e
    	return response

    @classmethod
    def get_error_by_user_name(cls, user_name, response=None):
    	try:
    		response = cls.objects(user_name=user_name).first()
    	except Exception, e:
    		print "Erro gettin error by user name: ", e

    	return response

    @classmethod
    def get_error_by_date(cls, date, response=None):
    	try:
    		response = cls.objects(date=date).all()
    	except Exception, e:
    		print "Erro getting error by user name: ", e

    	return response