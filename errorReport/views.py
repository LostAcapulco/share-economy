#!/usr/bin/python
# -*- encoding: utf-8 -*-
"""
Views which allow users to view error report

"""

from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from mongoengine.queryset import *
from django.views.decorators.csrf import csrf_exempt
from documents import ErrorReport
from django.conf import settings

@login_required(login_url='/account/login/')
def check_access_report(request, id=settings.ID_ROOT, go_to="/home/"):
	try:
		session_user = request.session['user']
		session_user_id = session_user.id.__str__().decode('utf-8')

		if session_user_id == id:
			go_to = "/updates/all/"

	except Exception as e:
		print "Error en auth: ", e

	return redirect(go_to)

@login_required(login_url='/account/login/')
def get_reports(request, template='report.html'):
	try:
		report = ErrorReport.get_all_errors()
		print report
		extra_context = {'errors':report}
		context = RequestContext(request, extra_context)
	except Exception as e:
		print "Error rendering errors report: ", e
	return render_to_response(template, context)

def save_error(**kwargs):
	response = None
	try:
		response = ErrorReport.set_error(**kwargs)
	except Exception as e:
		print "Error view saving error report: ", e


def update_friend_request(request,response=None):
	"""
	Send friend request to people to homologate the system
	"""
	from user.documents import RegistrationProfile as obj_Reg
	try:
		users = obj_Reg.system_user_list()
		if users is not None:
			user_ids_list = []
			users_ids_dic = {}
			fiends_ids_list = []
			for user in users:
				user_ids_list.append(user.id.__str__().decode('utf-8'))
				user_friends = user.friends
				for friend in user_friends:
					fiends_ids_list.append(friend.uid)
				users_ids_dic.update({user.id.__str__().decode('utf-8'):fiends_ids_list})

		vista_users = users_ids_dic.keys()
		response = []
		print "----------------------INICIO---------------------------------"
		for user in user_ids_list:
			print "######################INICIO USER###################################"
			for user_2 in vista_users:
				users_list = users_ids_dic[user_2]
				if user in users_list:
					print "//////", user, " está en lista de ", user_2, "---->", users_list
					sec_user_list = users_ids_dic[user]
					print "\n"
					if user_2 in sec_user_list:
						print user_2, " está en la lista de ", user, "---->", sec_user_list
						print "$$$$$$$$$son amigos$$$$$$$$"
					else:
						user_sender = obj_Reg.get_user(user_2)
						user_reciber = obj_Reg.get_user(user)

						#response.append(sendInv(user_sender, user_reciber))
						print "······No son amigos·····"
				else:
					print "No se conocen"
				print "\n"
				print "\n"
			print "######################FIN USER###################################"
		print "----------------------FIN---------------------------------"

	except Exception, e:
		print e
	return HttpResponse(response)

def sendInv(user_sender, user_reciber):
	"""
		Funcion refactorizada para reenvio de solicitud de amistad 
		user_sender objeto user
		user_reciber objeto user
	"""
	from notifications.views import general_notification_copy
	try:
		if session_user_id == '523b638466ad7e77429f11a2': 
		    message = unicode("Tienes una solicitud de amistad ",'utf-8')

		    general_notification_copy(user_sender,
		                    channel_id=user_reciber.id.__str__().decode('utf-8'), #person who will recive de message
		                    user_id=user_sender.id.__str__().decode('utf-8'), #person who is sending de message
		                    user_name=user_sender.username, #name of the person who is sending the message
		                    user_pic=user_sender.picture, #picture of the person who is sending the message
		                    action_trigered="envio una solicitud amistad",
		                    target_id=user_reciber.id.__str__().decode('utf-8'),
		                    target_name=user_reciber.username,
		                    enviorment='user profile',
		                    is_read='0',
		                    message=message,)

		    response = "Solicitud enviada - "
	except Exception as e:
	    print "ERROR: ", e
	    response = "Hubo un error al enviar la invitación: "+e.__str__().decode('utf-8')
	    response = response+"-"

	return response


def delete_notification(request, response=None):
	"""Delete notifications for a given date"""
	from notifications.documents import Notification
	if session_user_id == '523b638466ad7e77429f11a2': 
		response = Notification.delete_notification("2014-03-19")
		
		if response == True:
			print "notifications deleted"
			response = "notifications deleted"
		else:
			print response

	return HttpResponse(response)

def search_fb_user_with_product(request, response=None):
	"""Check de activity of a FB users"""
	from user.documents import RegistrationProfile as obj_Reg
	from product.documents import Product as obj_prod
	from comments.documents import Comment
	from offer.documents import Offer
	from chat.documents import Chat
	try:
	
		user_session = request.session['user']
		session_user_id = user_session.id.__str__().decode('utf-8')

		if session_user_id == '523b638466ad7e77429f11a2': 
			fb_users = obj_Reg.objects(email__icontains='facebook.com').all()

			user_list = []
			
			for user in fb_users:
				users_log_fb = {}
				user_products = []
				user_id = user.id.__str__().decode('utf-8')
				product_user_fb = obj_prod.objects(user_id=user_id).all()
				for product in product_user_fb:
					product_data = {}
					product_data.update({'Nombre':product.product_name,
										'seguidores':len(product.follows)})
					user_products.append(product_data)
				num_comments = Comment.objects(user_id=user_id).all()
				num_comments = len(num_comments)
				num_chats = Chat.objects(Q(first_user=user_id)|Q(second_user=user_id)).all()
				num_chats = len(num_chats)
				num_offers = Offer.objects(Q(bidder_id=user_id)|Q(owner_id=user_id)).all()
				num_offers = len(num_offers)
				users_log_fb.update({user.username:user_products,
									"Comments":num_comments,
									"Chats":num_chats,
									"Offers":num_offers})
				user_list.append([users_log_fb])
			response = user_list
			print response
	except Exception as e:
			print e
		
	return HttpResponse(response)