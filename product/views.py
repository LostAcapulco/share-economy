#!/usr/bin/python
# -*- encoding: utf-8 -*-
"""
Views which allow users to create a product profile.

"""

#from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from mongoengine.django.auth import User
from mongoengine.queryset import *
from documents import Product as prod
from documents import *
from offer.models import Offer
from user.documents import Chat, Wishlist, RegistrationProfile
from user.views import *
from user.storage_s3 import *
from notifications.views import general_notification
from forms import ProductForm
from django.utils import encoding
from array import *
from comments.documents import Comment
from errorReport.views import save_error
from random import randint
import json


@login_required(login_url='/account/login/')
def upload_product(request, tmpl_nm='reg_prod_info.html',form_class=ProductForm,img=None):
    """
    Allow a -user- to upload a product to his account
    """
    form = form_class(data=request.POST, files=request.FILES)
    if request.method == 'POST' and form.is_valid():
        user_session = request.session['user']
        user_id = user_session.id.__str__().decode('utf-8')
        request.session['user_id'] = user_id
        product_id = request.session['product_id']
        print product_id
        user_prod = form.save(user_id,product_id,False)
        if user_prod is not None:
            if "product_id" in request.session:
                try:
                    del request.session['product_id']
                except Exception as e:
                    request.session['product_id'] = None
                    print e
                return redirect('/product/profile/'+str(product_id)+'/')
        else:
            if "product_id" in request.session:
                try:
                    del request.session['product_id']
                except Exception as e:
                    request.session['product_id'] = None
                    print e
            return HttpResponse('Error al guardar el producto')
    else:
        extra_context = {}
        to_response_extra = response_none_valid(request,tmpl_nm,extra_context)
        try:
            product_id = request.session['product_id']
            product = prod.objects(id=product_id).first()
            img = product.pictures
        except Exception as e:
            print e
        return render_to_response(to_response_extra['template_name'], {'form':form,'img':img},
                                                                    context_instance=to_response_extra['context_instance'])


@login_required(login_url='/account/login/')
def edit_product(request, product_id=None, tmpl_nm='edit_prod_info.html',form_class=ProductForm,img=None):
    """
    Allow a -user- to edit a product updloaded
    """
    form = form_class(data=request.POST, files=request.FILES)
    print "To process", form.is_valid()
    if request.method == 'POST' and form.is_valid():
        user_session = request.session['user']
        user_id = user_session.id.__str__().decode('utf-8')
        request.session['user_id'] = user_id
        print "Edit this product", product_id
        if product_id is None:
            return redirect('/product/upload/')
        else:
            edit_product = True

            user_prod = form.save(user_id,product_id,edit_product)
            if user_prod is not None:
                if "product_id" in request.session:
                    try:
                        del request.session['product_id']
                    except Exception as e:
                        print e
                    return redirect('/product/profile/'+str(product_id)+'/')
            else:
                if "product_id" in request.session:
                    del request.session['product_id']
                return HttpResponse('Error al guardar el producto')
    else:
        extra_context = {}
        to_response_extra = response_none_valid(request,tmpl_nm,extra_context)
        if product_id is not None:
            print "Some error happens ", form.errors
            request.session['product_id'] = product_id
            product = prod.objects(id=product_id).first()
            return render_to_response(to_response_extra['template_name'], { 'form':form,
                                                                            'product_id':product_id,
                                                                            'product_name':product.product_name,
                                                                            'product_tags':product.tags[0],
                                                                            'product_about':product.about_product,
                                                                            'product_categories': product.categories[0],
                                                                            'product_pictures': product.pictures
                                                                        },
                                  context_instance=to_response_extra['context_instance'])
        else:
            return redirect('/product/upload/')            

@csrf_exempt
def regist_product_pic(request):
    user_session = request.session['user']
    user_id = user_session.id
    pic_name = str(user_id)+str(randint(99999,999999))
    handle_uploaded_file(request.FILES['thefile'],pic_name)
    picture_url=settings.S3_URL+settings.BUCKET_PROFILE+"/"+pic_name

    try:
        product_id = request.session['product_id'] 
    except:
        product_id = '0'
    try:
        print product_id
        if product_id == '0':
            product_id = prod.add_picture(picture_url=picture_url)
            request.session['product_id'] = product_id
        elif product_id is not None:
            product_id = prod.add_picture(product_id=product_id,picture_url=picture_url)
            request.session['product_id'] = product_id

        response = picture_url
    except Exception as e:
        print e
        response = False
    return HttpResponse(response)

@csrf_exempt
def delete_picture(request):
    if request.POST:
        try:
            product_id = request.session['product_id']
            url = request.POST.get('url')
            response = prod.delete_picture(product_id,url)
        except Exception as e:
            print e
            response = False
    else:
        response = False

    return HttpResponse(response)

def response_none_valid(request, tmpl_nm, extra_context=None):
    if extra_context is None:
        extra_context = {}

    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    to_response_extra = {'template_name':tmpl_nm,
                         'context_instance':context,'PUSHER_KEY': settings.PUSHER_APP_KEY,}

    return to_response_extra

def info_product_edit_prof(request):
    user = request.session['user']
    user_id = user.id.__str__().decode('utf-8')
    
    product = prod.objects(user_id=user_id).first()
    
    info = {'name':product.product_name,'tags':product.tags[0],'categories':product.categories[0],'about':product.about_product}
    
    return info

@login_required(login_url='/account/login/')
def product_profile(request, 
                    product_id=None, 
                    extra_context=None, 
                    template="pprofile.html",
                    owner=False,
                    are_frieds=False,
                    follower=None,
                    followers=0,
                    num_common_friends=0,
                    comments=None,
                    info_offer=None,
                    wisheslist=None,
                    tot_offer=None,
                    is_follower=False,
                    is_offered = 'N'):
    from user.views import already_friends, commun_friends
    """
    Display a -Product profile- for a loged -user-
    """
    import re
    try:
        from documents import Product as p
        if "product_id" in request.session:
                del request.session['product_id']
        product = p.get_product(product_id)
        if product is not None:
            user_session = request.session['user']
            user_session_id = user_session.id.__str__().decode('utf-8')
            request.session['product_id'] = product_id
            info_product = get_product_info(request,product_id)
            product_name = info_product['product']

            tags_list = product.tags[0].first_tag + ', ' + product.tags[0].second_tag + ', ' + product.tags[0].third_tag
            tags_list = tags_list.__str__().decode('utf-8')


            if product.user_id == user_session_id:
                owner = True
                product_user_id = user_session_id
                tags = product.tags[0].first_tag+' '+product.tags[0].second_tag+' '+product.tags[0].third_tag
                tags = tags.__str__().decode('utf-8')
                tags = {tag.strip("#") for tag in tags.split() if tag.startswith("#")}
                
                searchable_tags =[]
                
                for tag in tags:
                    searchable_tags.append(tag)

                wisheslist = matches_wishlist(request,searchable_tags)
                info_offer = product_offer_pending(request)
                try:
                    offers = Offer.objects(Q(owner_id=product.user_id)&Q(product_owner_id=product_id)&Q(status="1")).all()
                    tot_offer = len(offers)
                except Exception as e:
                    print "ERROR: ", e
            else:
                product_user_id = product.user_id
                are_frieds = already_friends(request,product_user_id)
                num_common_friends = commun_friends(request,product_user_id)

            
                offer = Offer.objects((Q(bidder_id=user_session_id) & Q(product_owner_id=product_id)) & Q(status='1')).all()

                if len(offer) == 0:
                    is_offered = 'N'
                else:
                    is_offered = 'Y'

                follower = prod.is_follower(product.id,user_session_id)

                if follower is not None:
                    is_follower = True
                else:
                    is_follower = False
            followers = len(product.follows)

            comments = product_comments(product_id)

            context = RequestContext(request, {
                                                'owner_profile':owner,
                                                'mutual_friends': are_frieds,
                                                'info_product':info_product,
                                                'info_offer':info_offer,
                                                'is_follower':is_follower,
                                                'followers':followers,
                                                'is_offered':is_offered,
                                                'wisheslist':wisheslist,
                                                'tags': tags_list,
                                                'common_friends':num_common_friends,
                                                'tot_offer':tot_offer,
                                                'is_invited':are_frieds,
                                                'comments':comments,
                                                'user_id': product.user_id,
                                                'product_id': product_id
                                              },[owner_product_info])
        else:
            template = "not_found.html"
            context = RequestContext(request,{})
        del request.session['product_id']
    except Exception as e:
        print "ERROR: ", e
        template="not_found.html"
        context = RequestContext(request,{})
    
    return render_to_response(template,context)

def matches_wishlist(request,tags=None):
    if tags is not None:
        user_session = request.session['user']
        user_session_id = user_session.id
        total = 0
        chunck_users = []
        last_id = 0
        for tag in tags:
            wisheslist = Wishlist.objects(Q(wishes__wish_name__iexact='#'+tag)&Q(owner_id__ne=user_session_id)).all()
            total = total+len(wisheslist)
            for wish in wisheslist:
                user_info = {}
                if last_id!=wish.owner_id:
                    user = User.objects(id=wish.owner_id).first()
                    user_info.update({'nick_name':user.username})
                    user_info.update({'pic_profile':user.picture})
                    user_info.update({'user_id':user.id.__str__().decode('utf-8')})
                    chunck_users.append(user_info)
                    last_id = wish.owner_id

    return {'total_wisheslist':total,'chunck_users':chunck_users}

def product_comments(product_id=None):
    chunck_comments = []
    try:
        comments = Comment.get_all_comments(product_id)
        if comments:
            for comment in comments:
                info_comment = {}
                info_comment.update({'user_id':comment.user_id})
                info_comment.update({'user_name':comment.user_name})
                info_comment.update({'user_pic':comment.user_pic})
                info_comment.update({'date':comment.date})
                info_comment.update({'text':comment.text})
                chunck_comments.append([info_comment])
    except Exception as e:
        error = "Error getting comments from product: "+e.__str__().decode('utf8')
        print error
        save_error(module = 'product',
                    method = 'product_comments - view',
                    error = error,
                    user_name = '----',
                    user_id = '----')

    return chunck_comments

@login_required(login_url='/account/login/')
def get_product_info(request, product_id):
    product = prod.objects(id=product_id).first()
    gallery = product.pictures
    img = product.pictures[0]
    img = img.url
    user_prod = product.user_id

    tags_list = product.tags[0].first_tag + ', ' + product.tags[0].second_tag + ', ' + product.tags[0].third_tag
    tags_list = tags_list.__str__().decode('utf-8')

    request.session['user_prod'] = user_prod.__str__().decode('utf-8')
    
    return {'product_id':product_id,'product':product,'gallery':gallery,
            'id_user_prod':user_prod,
            'product_img':img,
            'tags': tags_list,
            'PUSHER_KEY': settings.PUSHER_APP_KEY,}

def owner_product_info(request):
    product_owner = request.session['user_prod']
    user = User.objects(id=product_owner).first()
    nick_name = user.username
    try:
        user_address = user.address[0]
        user_deleg = user_address.deleg_mun
        user_pic = user.picture
    except:
        user_deleg = 'no definida'
        user_pic = ''

    offers = Offer.objects(Q(owner_id=product_owner) & Q(status = '3') | Q(bidder_id=product_owner) & Q(status = '3')).all()
    total_swap = offers.count()
    return {'user':user,
            'nick_name':nick_name,
            'user_deleg':user_deleg,
            'total_swap':total_swap,
            'user_picture':user_pic,
            'user_rating':user.rating}

def product_offer_pending(request):
    product_id = request.session['product_id']

    offers = Offer.objects(Q(product_owner_id=product_id) & Q(status='1')).all()
    total_pending_offers = len(offers)

    user_info = {}
    chunck_users = []
    if total_pending_offers > 0:
        for offer in offers:
            bidder_id = offer.bidder_id
            user = User.objects(id=bidder_id).first()
            user_info.update({'nick_name':user.username})
            user_info.update({'pic_profile':user.picture})
            user_info.update({'user_id':user.id.__str__().decode('utf-8')})
            user_info.update({'user_offer':'/offer/current/'+offer.id.__str__().decode('utf-8')})
            chunck_users.append(user_info)

    return {'total_pending_offers':total_pending_offers,'chunck_users':chunck_users}

@csrf_exempt
def chunck_products(request):
    if request.POST:
        type_chunck = request.POST.get('type')
        user_id = request.POST.get('user_id')
        category = request.POST.get('category').__str__().decode('utf-8')
        if category != '':
            request.session['category'] = category
        request.session['user_id'] = user_id
        if type_chunck=='one':
            products = products_user(request)
        elif type_chunck=='two':
            ids = {}
            request.session['ids'] = ids
            products = products_friend()

    return HttpResponse(json.dumps(products))

def products_user(request):
    category = request.session['category']
    profile_id = request.session['profile_id']

    if category is None or category == '':
        products = prod.objects(Q(user_id=profile_id.__str__().decode('utf-8'))).all()
    else:
        products = prod.objects(Q(user_id=profile_id) & Q(categories__first_cat=category) | Q(user_id=profile_id) & Q(categories__second_cat=category)).all()

    arrProducts = {}
    if products is not None: 
        chunck_products = []
        tags = {}
        category = {}
        for product in products:
            user_prod = {}
            user_prod.update({'product_name':product.product_name})
            user_prod.update({'product_id':product.id})
            user_prod.update({'product_img':product.pictures[0].url })
            for c in product.categories:
                category.update({'category_1': c.first_cat})
                category.update({'category_2': c.second_cat})
            user_prod.update({'categories':category})
            for t in product.tags:    
                tags.update({'tags_1':t.first_tag})
                tags.update({'tags_2':t.second_tag})
                tags.update({'tags_3':t.third_tag})
            user_prod.update({'product_tags':tags})
            if product.shares is not None:
                user_prod.update({'product_shares':product.shares})
            else:
                user_prod.update({'product_shares':0})
            user_prod.update({'product_follows': product.follows.__len__() })
            #user_prod.update({'geo_location':product.geo_location})
            chunck_products.append([user_prod])

        arrProducts.update({'products':chunck_products})
        
    return arrProducts

@csrf_exempt
def products_user_json(request,offer_id=None,edit_opt='0'):
    from offer.documents import Offer as obj_offer
    profile_id = request.POST.get('user')
    offer_id = request.POST.get('offer_id')
    products = prod.objects(Q(user_id=profile_id.__str__().decode('utf-8'))).order_by('-date').all()

    if products is not None: 
        chunck_products = []
        for product in products:
            if offer_id is not None and offer_id != '1' and offer_id != '0':

                bag_bidder = obj_offer.bag_bidder(offer_id)

                if product.id.__str__().decode('utf-8') in bag_bidder:
                    print "product: ", product.id.__str__().decode('utf-8')
                    edit_opt = '3'
                else:
                    edit_opt = '0'

            tags = {}
            category = {}
            user_prod = {}
            user_prod.update({'edit_opt':edit_opt})
            user_prod.update({'product_name':product.product_name})
            user_prod.update({'product_id':product.id.__str__().decode('utf-8')})
            user_prod.update({'product_img':product.pictures[0].url })
            for c in product.categories:
                category.update({'category_1': c.first_cat})
                category.update({'category_2': c.second_cat})
            user_prod.update({'categories':category})
            for t in product.tags:    
                tags.update({'tags_1':t.first_tag})
                tags.update({'tags_2':t.second_tag})
                tags.update({'tags_3':t.third_tag})
            user_prod.update({'product_tags':tags})
            if product.shares is not None:
                user_prod.update({'product_shares':product.shares})
                user_prod.update({'product_follows': product.follows.__len__() })
            else:
                user_prod.update({'product_shares':0})
                user_prod.update({'product_follows': product.follows.__len__() })
            #user_prod.update({'geo_location':product.geo_location})
            chunck_products.append([user_prod])

    return HttpResponse(json.dumps({'products':chunck_products}))

@csrf_exempt
@login_required(login_url='/account/login/')
def search_product(request):
    if request.POST:
        item = request.POST.get('item')
        id = request.POST.get('user_id')

        products = prod.objects(Q(product_name__icontains=item) & Q(user_id=id)).all()

        total_result = products.count()
        product = {}
        chunck_products = []
        if total_result > 0:
            tags = {}
            category = {}
            for product in products:
                user_prod = {}
                user_prod.update({'product_name':product.product_name})
                user_prod.update({'product_id':product.id.__str__().decode('utf-8')})
                user_prod.update({'product_img':product.pictures[0].url })
                for c in product.categories:
                    category.update({'category_1': c.first_cat})
                    category.update({'category_2': c.second_cat})
                user_prod.update({'categories':category})
                for t in product.tags:    
                    tags.update({'tags_1':t.first_tag})
                    tags.update({'tags_2':t.second_tag})
                    tags.update({'tags_3':t.third_tag})
                user_prod.update({'product_tags':tags})
                user_prod.update({'product_shares':product.shares})
                user_prod.update({'product_follows': product.follows.__len__() })
                #user_prod.update({'geo_location':product.geo_location})
                chunck_products.append([user_prod])
        
    return HttpResponse(json.dumps({'products':chunck_products}))

@csrf_exempt
def delete_product(request,response=None):
    user = request.session['user']
    user_sessio_id = user.id.__str__().decode('utf-8')
    product_id = request.POST.get('product_id')
    user_id = request.POST.get('user_id')
    try:
        if user_id == user_sessio_id:
            response = prod.delete_product(product_id)
    except Exception as e:
        response = e
    return HttpResponse(response)
    
@csrf_exempt
def friends_products(request,offer_id=None,edit_opt='0'):
    from offer.documents import Offer as obj_offer
    chunck_products = []
    if request.POST:
        try:
            user_id = request.POST.get('user_id')
            offer_id = request.POST.get('offer_id')
            user = User.objects(id=user_id).first()
            friends = user.friends
            
            for friend in friends:
                products = prod.objects(user_id=friend.uid).all()
                if products is not None:
                    product = {}
                    tags = {}
                    category = {}
                    for product in products:
                        if product is not None:

                            if offer_id is not None and offer_id != '1':
                                bag_bidder = obj_offer.bag_bidder(offer_id)
                                
                                if product.id.__str__().decode('utf-8') in bag_bidder:
                                    edit_opt = '3'
                                else:
                                    edit_opt = '0'

                            friend_user = User.objects(id=friend.uid).first()
                            user_prod = {}
                            user_prod.update({'edit_opt':edit_opt})
                            user_prod.update({'product_user_id':friend.uid})
                            user_prod.update({'product_user_name':friend_user.username})
                            user_prod.update({'product_name':product.product_name})
                            user_prod.update({'product_id':product.id.__str__().decode('utf-8')})

                            user_prod.update({'product_img':product.pictures[0].url })
                            for c in product.categories:
                                category.update({'category_1': c.first_cat})
                                category.update({'category_2': c.second_cat})
                            user_prod.update({'categories':category})
                            for t in product.tags:
                                tags.update({'tags_1':t.first_tag})
                                tags.update({'tags_2':t.second_tag})
                                tags.update({'tags_3':t.third_tag})
                            user_prod.update({'product_tags':tags})
                            user_prod.update({'product_shares':product.shares})
                            user_prod.update({'product_follows': product.follows.__len__() })
                            #user_prod.update({'geo_location':product.geo_location})
                            chunck_products.append([user_prod])
        except Exception as e:
            print "Error getting friends products: ", e

    return HttpResponse(json.dumps({'products':chunck_products}))

@csrf_exempt
def search_friends_products(request):
    chunck_products = []
    if request.POST:
        from user.views import get_friends_ids
        from user.documents import RegistrationProfile

        item_search = request.POST.get('product')
        user_session = request.session['user']
        user_session_id = user_session.id
        friends_ids = get_friends_ids(user_session_id)

        for friend_id in friends_ids:
            list_products = RegistrationProfile.search_product_user(item_search,friend_id)
            if list_products is not None:
                product = {}
                print "list_products:", list_products
                for products in list_products:
                    if products is not None:
                        for item in products:
                            for i in item:
                                print i
                            print "item.product_name: ", item.product_name
                            user_prod = {}
                            user_prod.update({'product_user_id':friend_id})
                            user_prod.update({'product_name':item.product_name})
                            user_prod.update({'product_about':item.about_product})
                            user_prod.update({'product_img':item.pictures})
                            user_prod.update({'product_id':item.id})

                        chunck_products.append([user_prod])

    return HttpResponse(json.dumps({'products':chunck_products}))

@csrf_exempt
def search_and_latest_products(request, products=None):
    from user.views import find_users
    from user.documents import RegistrationProfile as user_obj
    from group.views import find_groups
    from documents import Product
    from offer.documents import Offer as offer_obj
    
    user_session = request.session['user']

    if request.POST:
        print "Call from post"
        item_search = request.POST.get('product')
        products = prod.objects(product_name__icontains=item_search).all()

        if len(products) == 0:
            products = prod.objects(Q(categories__first_cat__icontains=item_search) | Q(categories__second_cat__icontains=item_search)).all()

    else:
        try:
            print "Getting lastest products"
            products = prod.get_lastest_products()
        except Exception as e:
            print "Error getting lastest", e
    chunck_products = []
    if products is not None:
        print "Parsing products data: "
        for product in products:

            user_prod = {}
            tags = {}
            category = {}
            user_prod.update({'product_name':product.product_name})
            user_prod.update({'product_id':product.id.__str__().decode('utf-8')})
            has_offer = offer_obj.product_has_user_offer(user_session.id.__str__().decode('utf-8'),product.id.__str__().decode('utf-8'))
            is_follower = Product.user_is_following(user_session.id.__str__().decode('utf-8'),product.id.__str__().decode('utf-8'))

            if has_offer is not None:
                has_offer = True
            else:
                has_offer = False

            if user_session.id.__str__().decode('utf-8')==product.user_id:
                owner = True
            else:
                owner = False

            owner_data = user_obj.get_user(product.user_id)
            try:
                user_prod.update({'user_name_product':owner_data.username})
                user_prod.update({'user_pic_product':owner_data.picture})
                user_prod.update({'user_rating_product':owner_data.rating})
                user_prod.update({'user_id_product':product.user_id})
                user_prod.update({'user_has_offered':has_offer})
                user_prod.update({'user_is_following':is_follower})
            except Exception as e:
                print "error getting owner info", e
                print "seborro el articuli: ", product.id
                response = prod.delete_product(product.id)

            user_prod.update({'session_is_owner':owner})
            try:
                user_prod.update({'product_img':product.pictures[0].url})
                for c in product.categories:
                    category.update({'category_1': c.first_cat})
                    category.update({'category_2': c.second_cat})
                user_prod.update({'categories':category})
                if len(category)==0:
                    response = prod.delete_product(product.id)
                    print response

                for t in product.tags:
                    tags.update({'tags_1':t.first_tag})
                    tags.update({'tags_2':t.second_tag})
                    tags.update({'tags_3':t.third_tag})
                user_prod.update({'product_tags':tags})
                user_prod.update({'product_shares':product.shares})
                user_prod.update({'product_follows': product.follows.__len__()})

                if len(tags)==0:
                    response = prod.delete_product(product.id)
                    print response
            except Exception as e:
                response = prod.delete_product(product.id)
                print "error getting img product ", response, e

            chunck_products.append([user_prod])

    result = json.dumps({'products':chunck_products})

    return HttpResponse(result)



def search_wishlist(request, product_id=None):
    if product_id is not None:
        wishlist = Wishlist.objects(id)
        
def get_matches(request, product_name=None):
    if product_name is not None:
        user = request.session['user']
        user_id = user.id.__str__().decode('utf-8')

        wishlists = Wishlist.objects(wishname__icontains=product_name).all()

        if wishlists is not None:
            list_result = []
            for list in wishlists:
                owner_id = list.owner_id
                list_result.append(owner_id)

    return list_result

@csrf_exempt
def search_matches(request):
    if request.POST:
        from documents import Product
        user_id = request.POST.get('user_id')
        try:
            wishes = Wishlist.objects(owner_id=user_id).first()
            if wishes is not None:
                chunck_products = []
                check_list = []
                for wish in wishes.wishes:
                    name = wish.wish_name.__str__().decode('utf8')
                    name = name.split('#')
                    products = prod.objects((Q(tags__first_tag__icontains=name[1])&Q(user_id__ne=user_id))|(Q(tags__second_tag__icontains=name[1])&Q(user_id__ne=user_id))|(Q(tags__third_tag__icontains=name[1])&Q(user_id__ne=user_id))).all()

                    if products is not None:
                        tags = {}
                        category = {}

                        for product in products:
                            check_repeated = product.id in check_list

                            if product is not None and check_repeated == False :
                                user_prod = {}
                                
                                check_list.append(product.id)

                                user_prod.update({'product_name':product.product_name})
                                user_prod.update({'product_id':product.id.__str__().decode('utf-8')})
                                user_prod.update({'product_img':product.pictures[0].url })
                                for c in product.categories:
                                    category.update({'category_1': c.first_cat})
                                    category.update({'category_2': c.second_cat})
                                user_prod.update({'categories':category})
                                for t in product.tags:
                                    tags.update({'tags_1':t.first_tag})
                                    tags.update({'tags_2':t.second_tag})
                                    tags.update({'tags_3':t.third_tag})
                                user_prod.update({'product_tags':tags})
                                user_prod.update({'product_shares':product.shares})
                                #user_prod.update({'geo_location':product.geo_location})

                                chunck_products.append([user_prod])

        except Exception as e:
            print "Error: ", e

    return HttpResponse(json.dumps({'products':chunck_products}))

@csrf_exempt
@login_required(login_url='/account/login/')
def follow_product(request,response=None):
    if request.POST:
        try:

            product_id = request.POST.get('product_id')
            user_id = request.POST.get('user_id')
            user = User.objects(id=user_id).first()
            username = user.username
            user_pic = user.picture

            follows = Follow()
            follows.user_id=user_id
            follows.username=username
            follows.userpic=user_pic

            product = prod.objects(id=product_id).first()
            product_name = product.product_name

            product_owner = product.user_id
            product.follows.append(follows)
            product.save()

            message = str(username)
            usrnm = encoding.smart_unicode(username, "utf8")
            prdctnm = encoding.smart_unicode(product_name, "utf8")
            action = " esta siguiendo tu "
            action = encoding.smart_unicode(action, "utf8")
            message = usrnm+action+prdctnm
            
            
            
            general_notification(request,channel_id=product_owner,
                                 user_id=user_id,
                                 message=message, 
                                 user_name=username, 
                                 user_pic=user_pic,
                                 action_trigered='esta siguiendo', 
                                 target_id=product_id, 
                                 target_name=product_name,
                                 enviorment='product',
                                 is_read='0')

            response = True
        except BaseException as e:
            print e
            response = False
            
    return HttpResponse(response)

@csrf_exempt
def search_mini(request):
    """
    Se cambio esta funcion en la linea 644
    name_product = request.POST.get('product_id')
    """
    chunck_products=[]
    name_product = request.POST.get('criteria')
    if request.POST:
        products = prod.objects(product_name__icontains=name_product).all()
        if products is not None:
            info = {}
            category = {}
            user_prod = {}
            for product in products:
                user_prod.update({'product_name':product.product_name})
                user_prod.update({'product_id':product.id.__str__().decode('utf-8')})
                user_prod.update({'product_img':product.pictures[0].url })
                for c in product.categories:
                    category.update({'category_1': c.first_cat})
                    category.update({'category_2': c.second_cat})
                user_prod.update({'categories':category})
                for t in product.tags:    
                    info.update({'tags_1':t.first_tag})
                    info.update({'tags_2':t.second_tag})
                    info.update({'tags_3':t.third_tag})
                user_prod.update({'product_tags':info})
            chunck_products.append([user_prod])
    return HttpResponse(json.dumps({'products':chunck_products}))

@csrf_exempt
def user_product_whishlist(request, tags=None):
    """
    Get the list of user which his wishlists match for a given product
    """
    import re
    chunck_users = []
    if request.POST:
        tags = request.POST.get('tags')
        tags = re.findall(r'\w+',tags)        
        
        try:
            if tags is not None:
                user_session = request.session['user']
                user_session_id = user_session.id
                total = 0
                last_id = 0
                for tag in tags:
                    wisheslist = Wishlist.objects(Q(wishes__wish_name__iexact='#'+tag)&Q(owner_id__ne=user_session_id)).all()
                    total = total+len(wisheslist)
                    for wish in wisheslist:
                        user_info = {}
                        if last_id!=wish.owner_id:
                            user = User.objects(id=wish.owner_id).first()
                            user_info.update({'nick_name':user.username})
                            user_info.update({'pic_profile':user.picture})
                            user_info.update({'user_id':user.id.__str__().decode('utf-8')})
                            chunck_users.append(user_info)
                            last_id = wish.owner_id

        except Exception as e:
            print "ERROR: ", e

    return HttpResponse(json.dumps(chunck_users))


@csrf_exempt
def get_product_by_cat(request):
    """
    Get products by category
    """
    chunck_products=[]
    try:
        from documents import Product
        if request.POST:
            category = request.POST.get('category')
            products = Product.get_products_by_category(category)
            print products
            if products is not None:
                for product in products:
                    info = {}
                    category = {}
                    user_prod = {}
                    user_prod.update({'product_name':product.product_name})
                    user_prod.update({'product_id':product.id.__str__().decode('utf-8')})
                    user_prod.update({'product_img':product.pictures[0].url })
                    for c in product.categories:
                        category.update({'category_1': c.first_cat})
                        category.update({'category_2': c.second_cat})
                    user_prod.update({'categories':category})
                    for t in product.tags:    
                        info.update({'tags_1':t.first_tag})
                        info.update({'tags_2':t.second_tag})
                        info.update({'tags_3':t.third_tag})
                    user_prod.update({'product_tags':info})
                    chunck_products.append([user_prod])
    except Exception as e:
        print "Error executing getting products by category: ", e
    return HttpResponse(json.dumps({'products':chunck_products}))