#!/usr/bin/python
# vim: set fileencoding= utf-8 :

from django import forms
from django.utils.translation import ugettext_lazy as _

from models import *

attrs_dict = {'class': 'required'}
cat_producto = ['Libros, Revistas y Comics',
'Videojuegos y Películas',
'Computación, Celulares, Gadgets y Electrónica',
'Música e Instrumentos musicales',
'Deportes y Actividades al aire libre',
'Bicis, Motos y Transporte',
'Juguetes y Artículos para Niños y Bebes',
'Herramientas, Muebles, Electrodomésticos y Hogar',
'Salud y Belleza',
'Ropa, Zapatos y Accesorios',
'Fotografía, Arte y Diseño',
'Artículos de Colección y Antigüedades',
'Artículos para mascotas',
'Servicios',
'Otros'
]

cat_producto_array = ['Libros, Revistas y Comics',
'Videojuegos y Películas',
'Computación, Celulares, Gadgets y Electrónica',
'Música e Instrumentos musicales',
'Deportes y Actividades al aire libre',
'Bicis, Motos y Transporte',
'Juguetes y Artículos para Niños y Bebes',
'Herramientas, Muebles, Electrodomésticos y Hogar',
'Salud y Belleza',
'Ropa, Zapatos y Accesorios',
'Fotografía, Arte y Diseño',
'Artículos de Colección y Antigüedades',
'Artículos para mascotas',
'Servicios',
'Otros']
cat_num_prod = ['1','2','3','4','5','6','7','8','9']

class ProductForm(forms.Form):
    """
    Form for upload a prodcut to your inventory

    """
    productname = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Nombre del producto', 'class':'bg-gray'}),
                                label=_(u'Dale el nombre que gustes a tu producto(Ejemplos: iPad Clasico, Xbox 360)'),required=True)
    tags = forms.CharField( max_length=90,
                            widget=forms.TextInput(attrs=dict({'placeholder':'Ejemplo: #ipod #apple #tablet', 'class':'bg-gray'},
                            maxlength=75)),
                            label=_(u'Escoje tags para que la gente encuentre tu producto (Ejemplo: #iPad #apple #tablet)'),required=True)
    category = forms.ChoiceField(choices=[(x, x) for x in sorted(cat_producto)],
                                 label=_(u'¿En cuál de las categorías de Kamvia crees que encaja tu producto? Puedes elejir dos categorís distintas'))
    category_2 = forms.ChoiceField(choices=[(x, x) for x in sorted(cat_producto)],
                                   label=_(u' '))
    cant_produc= forms.ChoiceField(choices=[(x, x) for x in sorted(cat_num_prod)],
                                   label=_(u'¿Cuantos productos tienes en stock?'))
    about_product = forms.CharField(widget=forms.Textarea(attrs={'palceholder':'Decribe tu producto (max 200 caracteres)'}),
                                    label=_(u'Agrega una descripcion de tu producto, razones por la que quieres intercambiar, etc'),
                                    required=True)
    stp1_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'form_id','value':'{[form_id="form_id_21"]}'}),required=False)
    nxt_stp_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'kamvia_next_id', 'value':'{[kamvia_next_id]}'}),required=False)

    def clean(self):
        if self.cleaned_data.get('productname') == '' or self.cleaned_data.get('productname') == None:
            raise forms.ValidationError(_(u'Debes de introducir un nombre para tu producto'))

        if self.cleaned_data.get('about_product') == '' or self.cleaned_data.get('about_product') == None:
            raise forms.ValidationError(_(u'Debes de introducir una descripción para tu producto'))

        if self.cleaned_data.get('tags') == '' or self.cleaned_data.get('tags') == None:
            raise forms.ValidationError(
                    _(u'Por favor introduce 3 tags.'))
        else:
            tags_list = self.cleaned_data.get('tags').split()
            if len(tags_list)<3:
                raise forms.ValidationError(
                    _(u'Por favor introduce 3 tags.'))

        return self.cleaned_data

    def save(self, user_id=None, product_id=None, edit_product=True):

        temp_list = self.cleaned_data['tags'].split()
        tags_list = []
        for item in temp_list:
            item = unicodedata.normalize('NFKD', item).encode('ascii','ignore')
            tags_list.append(item.__str__().decode('utf-8'))

        cat_list = ['','']
        cat_tmp_1 = unicodedata.normalize('NFKD',self.cleaned_data['category']).encode('ascii','ignore')
        cat_tmp_2 = unicodedata.normalize('NFKD',self.cleaned_data['category_2']).encode('ascii','ignore')

        cat_list[0] = cat_tmp_1.__str__().decode('utf-8')
        cat_list[1] =  cat_tmp_2.__str__().decode('utf-8')

        if cat_list[0] == 'Categorias':
            cat_list[0] = ''

        product_name = unicodedata.normalize('NFKD',self.cleaned_data['productname']).encode('ascii','ignore')
        product_name = product_name.__str__().decode('utf-8')
        about_product = unicodedata.normalize('NFKD',self.cleaned_data['about_product']).encode('ascii','ignore')
        about_product = about_product.__str__().decode('utf-8')
        cant_prod = self.cleaned_data['cant_produc']
        print "Processing in froms"

        new_product = Product.update_product(user_id=user_id,
                                             product_name=product_name,
                                             tags_list=tags_list,
                                             cat_list=cat_list,
                                             about_product=about_product,
                                             product_id=product_id,
                                             cant_prod=cant_prod,
                                             edit_product=edit_product)

        return new_product

