# -*- encoding: utf-8 -*-
from mongoengine import StringField, IntField, ListField, GeoPointField, Document
from mongoengine import EmbeddedDocumentField, EmbeddedDocument, DateTimeField
from mongoengine.queryset import *

import unicodedata
import datetime
import sys

class Product(Document):
    """
    A simple product profile which stores product info
    """
    user_id = StringField()
    product_name = StringField(max_length=40)
    about_product = StringField(max_length=1500)
    shares = StringField(max_length=200)
    geo_location = GeoPointField()
    tags = ListField(EmbeddedDocumentField('PTags'))
    categories = ListField(EmbeddedDocumentField('Categories'))
    pictures = ListField(EmbeddedDocumentField("Picture"))
    follows = ListField(EmbeddedDocumentField("Follow"))
    cant_prod = StringField(max_length=2)
    date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def get_product(cls, product_id):
        try:
            product = cls.objects(id=product_id).first()
        except Exception, e:
            error = "Error getting product document: "+e.__str__().decode('utf-8')
            print error
        return product

    @classmethod
    def update_product(cls,user_id=None, product_name=None, tags_list=None, cat_list=None, 
                       about_product=None,product_id=None, cant_prod=None,edit_product=True):
        """
        Create a new, -Product- related to a profile.

        """
        from user.documents import RegistrationProfile

        product = cls.objects(id=product_id).first()
        product.user_id = user_id
        product.product_name = product_name
        product.about_product = about_product

        if edit_product:
            product.tags[0].first_tag = tags_list[0]
            product.tags[0].second_tag = tags_list[1]
            product.tags[0].third_tag = tags_list[2]
        
            product.categories[0].first_cat = cat_list[0]
            product.categories[0].second_cat = cat_list[1] 
            product.save()
        else:
            print "Saving product..."
            tags = PTags()
            print type(tags_list[0])

            tags.first_tag = tags_list[0]
            tags.second_tag = tags_list[1]
            tags.third_tag = tags_list[2]
            
            product.tags.append(tags)

            categories = Categories()
            categories.first_cat = cat_list[0]
            categories.second_cat = cat_list[1]
            product.categories.append(categories)

            product.cant_prod = cant_prod
            product.status = '0'

        product.save()
        try:
            RegistrationProfile.add_user_product(produc_id=product.id,
                                             product_name=product.product_name,
                                             product_about=product.about_product,
                                             product_picture=product.pictures[0])
        except Exception as e:
            print e

        return product

    @classmethod
    def add_picture(cls, product_id=None, picture_url=None):
        try:
            picture = Picture()
            picture.url = str(picture_url)

            if product_id is None or product_id == '0':
                product = cls()
                product.product_name = "Sin Nombre"
                product.save()
                product_id = product.id

            product = cls.objects(id=product_id).first()
            product.pictures.append(picture)
            product.save()

            product_id = product.id
            
            return product_id
        except:
            return sys.exc_info()[0]

    @classmethod
    def delete_picture(cls,product_id,url):
        try:
            cls.objects(id=product_id).update_one(pull__pictures__url=url)
            response = True
        except Exception as e:
            print e
            response = False
        return response

    @classmethod
    def delete_product(cls,product_id):
        try:
            product = cls.objects(id=product_id).first()
            product.delete()
            response = True
        except Exception as e:
            response = e

        return response

    @classmethod
    def get_lastest_products(cls):
        products = cls.objects().order_by('-date').all()
        return products

    @classmethod
    def get_products_by_category(cls,category,response=None):
        try:
            response =  cls.objects(Q(categories__firs_cat__exact=category)|Q(categories__second_cat__exact=category)).order_by('-date').all()
        except Exception as e:
            print "Error getting products from documents: ", e
        return response

    @classmethod
    def user_is_following(cls,user_id,product_id,response=None):
        response = cls.objects(Q(id=product_id)&Q(follows__user_id=user_id)).first()
        if response is not None:
            response = True
        else:
            response = False
        return response

    @classmethod
    def is_follower(cls, product_id, user_session_id,response=None):
        try:
            response = cls.objects(Q(id=product_id) & Q(follows__user_id=user_session_id)).first()
        except Exception as e:
            print e
        return response
    
class Picture(EmbeddedDocument):

    url=StringField()

class PTags(EmbeddedDocument):

    first_tag=StringField(max_length=150, required=False)
    second_tag=StringField(max_length=150, required=False)
    third_tag=StringField(max_length=150, required=False)

class Categories(EmbeddedDocument):

    first_cat=StringField(max_length=150, required=False)
    second_cat=StringField(max_length=150, required=False)
    
class Follow(EmbeddedDocument):
    
    user_id=StringField()
    username=StringField()
    userpic=StringField()
