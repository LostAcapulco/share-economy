from django.conf.urls.defaults import patterns, url

from views import upload_product, product_profile, regist_product_pic, chunck_products, search_product, delete_product, friends_products
from views import follow_product, friends_products,search_and_latest_products, search_mini, search_matches, products_user_json
from views import delete_picture, edit_product, search_friends_products, user_product_whishlist, get_product_by_cat

urlpatterns = patterns('',
    url(r'^upload/$',
        upload_product,
        name='product_upload'),                  #URL FOR A PRODUCT REGISTER
    url(r'^edit/(?P<product_id>\w+)/$',
        edit_product,
        name='edit_product'),                    #URL TO EDIT AN EXISTING PRODUCT
    url(r'^picture/', 
        regist_product_pic, 
        name='picture_upload'),                  #Upload picture to s3
    url(r'^profile/(?P<product_id>\w+)/$',
        product_profile,
        name='product_profile'),                 #URL TO PRODUCT PROFILE
    url(r'^products/$',
        chunck_products,
        name='chunck_products'),                 #URL TO GET A CHUNCK OF PRODUCTOS
    url(r'^search/$',
        search_product,
        name='search_product'),                 #URL TO MAKE A SEARCH PRODUCT
    url(r'^delete/$',
        delete_product,
        name='delete_product'),                 #URL TO DELETE A PRODUCT
    url(r'^testfp/$',
        friends_products,
        name='friends_products'),                 #URL TO DELETE A PRODUC
    url(r'^follow/$',
        follow_product,
        name='follow_product'),                 #URL TO FOLLOW A PRODUCT
    url(r'friendProduct/$',
        friends_products,
        name='friend_product'),                  #URL TO GET FRIENDS PRODUCTS
    url(r'^latest/$',
        search_and_latest_products,
        name='latest_product'),                  #URL TO GET THE LATEST PRODUCTS    

    url(r'^search_mini/$',
        search_mini,
        name='search_mini'),                    #URL TO MAKE A SEARCH
    url(r'^matchwishes/$',
        search_matches,
        name='search_matches'),                 #URL TO GET THE PRODUCTS THAT MATCH WITH A WISH LIST
    url(r'^products_all/$',
       products_user_json,
       name='products_user_json'),              #Get all productos by user
    url(r'^delete_picture/$',
        delete_picture,
        name='delete_picture'),
    url(r'^search_friends_products/$',
        search_friends_products,
        name='search_friends_products'),        #Make a search inside of friends products
    url(r'^user_match_wishlist/$',
        user_product_whishlist,
        name='user_product_whishlist'),        #Return the user that may be interest in a product
    url(r'^get_by_cat/$',
        get_product_by_cat,
        name='get_product_by_cat'),        #Return the user that may be interest in a product 
) 