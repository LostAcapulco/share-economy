import urllib

from django.shortcuts import redirect
from django.conf import settings
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate as  auth_authenticate
from backend import FacebookBackend
from django.core.urlresolvers import reverse
from errorReport.views import save_error

def login(request):
    """ First step of process, redirects user to facebook, which redirects to authentication_callback. """

    args = {
        'client_id': settings.FACEBOOK_APP_ID,
        'scope': settings.FACEBOOK_SCOPE,
        'redirect_uri': request.build_absolute_uri('/auth/authentication_callback'),
    }

    return redirect('https://www.facebook.com/dialog/oauth?' + urllib.urlencode(args))

def authentication_callback(request, url='/home/'):
    """ Second step of the login process.
    It reads in a code from Facebook, then redirects back to the home page. """
    try:
        
        code = request.GET.get('code')
        fbbckdn = FacebookBackend()
        user = fbbckdn.authenticate(token=code, request=request)
        if user.is_anonymous():
            #we have to set this user up
            url = reverse('facebook_setup')
            url += "?code=%s" % code
        else:
            password = str(user.facebook_id)
            password = user.username+password
            user = auth_authenticate(username=user.username,password=password)
            if user is not None:
                if user.is_authenticated:
                    request.session['user'] = user
                    request.name = user.username
                    auth_login(request, user)
                    url = getattr(settings, "LOGIN_REDIRECT_URL")
                else:
                    url = "/account/login/"
            else:
                url = "/account/login/"
            #figure out where to go after setup
            url = getattr(settings, "LOGIN_REDIRECT_URL")
    except Exception as e:
        error = "Error en authentication callback: " + e.__str__().decode('utf-8')
        print error
        save_error(module = 'facebook',
                    method = 'authentication_callback - view',
                    error = error,
                    user_name = 'login',
                    user_id = 'login')

    return redirect(url)
