from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
    url(r'^login$', 'facebook.views.login'),
    url(r'^authentication_callback$', 'facebook.views.authentication_callback'),
    url(r'^logout$', 'django.contrib.auth.views.logout'),
)