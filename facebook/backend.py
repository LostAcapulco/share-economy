import cgi, urllib, json

from django.conf import settings
from errorReport.views import save_error
from user.documents import RegistrationProfile

class FacebookBackend:
    def authenticate(self, token=None, request=None):
        """ Reads in a Facebook code and asks Facebook if it's valid and what user it points to. """
        args = {
            'client_id': settings.FACEBOOK_APP_ID,
            'client_secret': settings.FACEBOOK_APP_SECRET,
            'redirect_uri': request.build_absolute_uri('/auth/authentication_callback'),
            'code': token,
        }
        print "autenticando -authenticate backend-"
        try:
            # Get a legit access token
            
            target = urllib.urlopen('https://graph.facebook.com/oauth/access_token?' + urllib.urlencode(args)).read()
            response = cgi.parse_qs(target)
            access_token = response['access_token'][-1]

            # Read the user's profile information
            fb_profile = urllib.urlopen('https://graph.facebook.com/me?access_token=%s' % access_token)
            fb_profile = json.load(fb_profile)

        except Exception as e:
            error =  "Error getting acces token: " + e.__str__().decode('utf-8')
            print error
            save_error(module = 'facebook',
                        method = 'authenticate - backend',
                        error = error,
                        user_name = 'login',
                        user_id = 'login')

        try:

            # Try and find existing user create_user(username, password, email)
            user = RegistrationProfile.objects(facebook_id=fb_profile['id']).first()
            # Update access_token
            user.access_token = access_token
            user.save()

        except Exception:
            # No existing user
            username = fb_profile.get('username', fb_profile['first_name'])

            if getattr(settings, 'FACEBOOK_FORCE_SIGNUP', False):
                email = fb_profile['username']
                password = fb_profile['id']
                password = username+str(password)
                email = email+"@facebook.com"
                name = fb_profile['first_name']
                user = RegistrationProfile.create_inactive_user(username,password,email,False,name,None)
                user.first_name = fb_profile['first_name']
                user.last_name = fb_profile['last_name']
                user.facebook_id = fb_profile['id']
                user.access_token=access_token
                user.save()

        return user

    supports_object_permissions = True
    supports_anonymous_user = False

