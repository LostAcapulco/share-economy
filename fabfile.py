from fabric.api import *
from fabric.context_managers import hide

import logging

logging.basicConfig()
logging.getLogger('ssh.transport').setLevel(logging.INFO)

def production():
    """
    Config to deploy staging
    """
    env.use_ssh_config = True
    env.hosts = ["example"]
    env.user = "example"
    env.code_dir = "example"
    env.password = "example"
    env.branch = 'example'
    env.virtualenv = 'example'
    env.install_requirements = True
    env.resetdb = False
    env.syncdb = False
    env.collectstatic = True

def staging():
    """
    Config to deploy staging
    """
    env.use_ssh_config = True
    env.hosts = ["166.78.170.29"]
    env.user = "deploy"
    env.code_dir = "/home/deploy/sites/share-economy/"
    env.password = "d3v3l8804"
    env.branch = 'master'
    env.virtualenv = 'kamvia'
    env.install_requirements = True
    env.collectstatic = True

def with_virtualenv(command):
    """
    Execute the command inside the virtualenv
    """
    run('source /home/deploy/.virtualenvs/kamvia/bin/activate ' + '&& ' + command)

def with_virtualenv_sudo(command):
    """
    Execute the command inside the virtualenv
    """
    sudo('source /home/deploy/.virtualenvs/kamvia/bin/activate ' + '&& ' + command)


def resetdb():
    """
    Reset Database
    """
    command = 'python manage.py reset_db -R default --noinput'
    with_virtualenv(command)

def collectstatic():
    """
    Collect static files to serve it via webserver
    """
    command = 'python manage.py collectstatic --noinput -v3'
    with_virtualenv_sudo(command)


def syncdb():
    """
    Sync Database
    """
    command = 'python manage.py syncdb --noinput'
    with_virtualenv(command)

def install_requirements():
    """
    Install requirements via pip
    """
    command = 'pip install -r requirements.txt'
    with_virtualenv_sudo(command)

def restart():
    """
    Restart nginx and supervisor
    """
    sudo("service nginx restart")
    sudo("supervisorctl reload")

def deploy():
    """
    Deploy the app
    """

    with settings(port="22"):
        with cd(env.code_dir):

            sudo("git stash")
            sudo("git pull")
            sudo("git checkout %s" % env.branch)
            sudo("git pull")
            sudo("git stash pop")

            # Collect statics
            if env.collectstatic:
                collectstatic()

            restart()

def revert():
    """
    Revert git via reset --hard @{1}
    """
    with cd(env.code_dir):
        command = 'git reset --hard @{1}'
        run(command)
        restart()
