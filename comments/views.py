#!/usr/bin/python
# -*- encoding: utf-8 -*-
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from errorReport.views import save_error
from activity import Activity
from models import *
from user.documents import *
from notifications.views import send_mail_notification

import json
      
@csrf_exempt
@login_required(login_url='/account/login/')
def add_comment(request):

    if request.POST:
        try:
            enviorment_id = request.POST.get('enviorment_id')
            enviorment_type = request.POST.get('enviorment_type')
            text = request.POST.get('text')
            user = request.session['user']
            user_id = user.id.__str__().decode('utf-8')
            user_name = user.username                       
            user_obj = User.objects(id=user_id).first()
            user_pic = user_obj.picture
            
            Comment.add_comment(enviornment_id=enviorment_id,
                                user_id=user_id,
                                enviorment_type=enviorment_type,
                                user_name=user_name,
                                user_pic=user_pic,
                                text=text)
            
            send_comment(request,text,enviorment_id,user_name,user_pic,text)
            
            response = True

        except Exception as e:
            error = "Error adding comment: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'comments',
                        method = 'add_comment - view',
                        error = error,
                        user_name = user_name,
                        user_id = user_id)
            response = "Algo sucedio al enviar tu commentario"
    else:
        response = "no valid"

    return HttpResponse(response)

@csrf_exempt
@login_required(login_url='/account/login/')
def get_comments(request):
    if request.POST:
        enviorment_type = request.POST.get('enviorment_type')
        enviorment_id = request.POST.get('enviorment_id')

        comments = Comment( Q(enviorment_type=enviorment_type) & Q(enviorment_id=enviorment_id) ).all()

        if comments is not None:
            array_comments = {}
            chunck_comments = []
            response = {}
            for comment in comments:
                user_comment = {}
                user_comment.update({'user_id':comment.user_id})
                user_comment.update({'user_pic':comment.user_pic })
                user_comment.update({'user_name':comment.user_name})
                for response in comment.response:
                    response.update({'user_id':response.user_id})
                    response.update({'user_name':response.user_name})
                    response.update({'user_pic':response.user_pic})
                    response.update({'date':response.date})
                    response.update({'text':response.text})
                chunck_comments.append([user_comment])
            array_comments.update({'products':chunck_comments})

    return HttpResponse(json.dumps(array_comments))

def test_comment(request, template='test_comment.html'):
    user = request.session['user']
    user_id = user.id.__str__().decode('utf-8')
    request.session['user_id'] = user_id
    return render_to_response(template, 
                              {'PUSHER_KEY': settings.PUSHER_APP_KEY,}, 
                              RequestContext(request))

@csrf_exempt
@login_required(login_url='/account/login/')
def send_comment(request,action,enviroment_id,user_name,user_pic,text):

    user_session = request.session['user']
    user_session_id = user_session.id.__str__().decode('utf-8')
    activity_type = "activity"
    try:
        import pusher
        p = pusher.Pusher(
              app_id=settings.PUSHER_APP_ID,
              key=settings.PUSHER_APP_KEY,
              secret=settings.PUSHER_APP_SECRET
            )

        activity = Activity(activity_type, action, user_session_id).get_message()
        send_mail_notification(user_session, "ha escrito un cometario", action, True)
        print type(p)
        print "enviorment id:", enviroment_id
        print "activity: ", activity
        print "Activity type: ", activity_type

        p[enviroment_id].trigger(activity_type,activity)
    except Exception as e:
        error = "Error pushing activity: "+e.__str__().decode('utf-8')
        print error
        save_error(module = 'comments',
                    method = 'send_comment - view',
                    error = error,
                    user_name = user_name,
                    user_id = '----')

    return HttpResponse('')