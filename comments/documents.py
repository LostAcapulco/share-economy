from mongoengine import StringField, ListField, DateTimeField, Document
from mongoengine import EmbeddedDocumentField, EmbeddedDocument
from mongoengine.queryset import *
from errorReport.views import save_error

import datetime, time

class Comment(Document):
    """
    A comment document for the hole system
    """
    enviornment_id = StringField()
    enviorment_type = StringField()
    user_id = StringField()
    user_name = StringField()
    user_pic = StringField()
    date = DateTimeField(default=datetime.datetime.now)
    text = StringField()
    response = ListField(EmbeddedDocumentField('Response'))

    @classmethod
    def add_comment(cls,**kwargs):
        try:
            comment = cls(**kwargs)
            comment.save()
        except Exception, e:
            error = "Error adding comment document"+e.__str__().decode('utf-8')
            print error
            save_error(module = 'comments',
                        method = 'add_comment - document',
                        error = error,
                        user_name = '----',
                        user_id = '----')
        
        return comment
    
    @classmethod
    def delete_comment(cls,comment_id):
        
        try:
            comment = cls.object(id=comment_id)
            comment.drop_collection()
            
            return True
        except Exception as e:
            
            return e
        
    @classmethod
    def add_response(cls,comment_id=None, response=None):
    
        if comment_id is not None and response is not None:
            comment = cls.objects(id=comment_id)
            comment.append(response)
            comment.save()
        
            return comment
        
        else:
            return False

    @classmethod
    def get_all_comments(cls,enviornment_id,response=None):
        
        if enviornment_id is not None:
            try:
                response = cls.objects(enviornment_id=enviornment_id.__str__().decode('utf-8')).all()
            except Exception as e:
                error = "Error gettin comments from document: "+e.__str__().decode('utf-8')
                print error

                save_error(module = 'comments',
                        method = 'get_all_comments - document',
                        error = error,
                        user_name = '----',
                        user_id = '----')

        return response

class Response(EmbeddedDocument):
    user_id = StringField()
    user_name = StringField()
    user_pic = StringField()
    date = DateTimeField(default=datetime.datetime.now)
    text = StringField()
    