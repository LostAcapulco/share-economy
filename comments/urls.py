from django.conf.urls.defaults import patterns, url

from views import test_comment, add_comment


urlpatterns = patterns('',
    url(r'^test/$',
        test_comment,
        name='test_comment'),                  #URL TO ALLOW AN USER MAKE AN OFFER
    url(r'^send/$',
        add_comment,
        name='send_comment'),              #URL TO VIEW A CURRENT OFFER CALLED VIA GET
)