from django.conf.urls import patterns, include, url
from user.views import index, tos

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
    #url(r'^actualizar/', 'user_profile.views.user_update_view'),
    ('^$', index),
    ('^home/$', index),
    ('^tos/$',tos),
    (r'^notifications/', include('notifications.urls')),
    (r'^comments/', include('comments.urls')),
    (r'^account/', include('user.urls')),
    (r'^product/', include('product.urls')),
    (r'^stream/', include('feedStream.urls')),
    (r'^group/', include('group.urls')),
    (r'^offer/', include('offer.urls')),
    (r'^auth/', include('facebook.urls')),
    (r'^chat/', include('chat.urls')),
    (r'^updates/', include('errorReport.urls')),

)
