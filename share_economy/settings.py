#!/usr/bin/python
# -*- encoding: utf-8 -*-

# Django settings for share_economy project.
import os.path

CURRENT_PATH = os.path.join(os.path.dirname(__file__))

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

ID_ROOT ='523b638466ad7e77429f11a2'

S3_URL = 'https://s3-us-west-1.amazonaws.com/'
AWS_ACCESS_KEY_ID = 'AKIAI2Y3RZNSBVFS2N7Q'
AWS_SECRET_ACCESS_KEY = 'fxZeTLW0pKAcmbEpfE0tSbqJBZI4osdzwSpTchAo'
BUCKET_PROFILE = 'profilekamvia'
BUCKET_PRODUCT = 'productkamvia'

FILE_UPLOAD_MAX_MEMORY_SIZE = 2000621440

DEFAULT_CHARSET = 'utf-8'

DEBUG =  False
TEMPLATE_DEBUG = DEBUG

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'app.db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = 'var/www/kamvia/static'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(CURRENT_PATH,'static').replace('\\','/'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '=s9hxj#45nrfyn!+b#v*a3)vd*7cu-!&amp;7u5dc%gy+^c01=l#=o'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'share_economy.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'share_economy.wsgi.application'

SESSION_ENGINE = 'mongoengine.django.sessions'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(CURRENT_PATH,'templates').replace('\\','/'),
)

TEMPLATE_CONTEXT_PROCESSORS = ("django.contrib.auth.context_processors.auth",
"django.core.context_processors.debug",
"django.core.context_processors.i18n",
"django.core.context_processors.media",
"django.core.context_processors.static",
"django.core.context_processors.tz",
"django.contrib.messages.context_processors.messages",
"django.core.context_processors.request",)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'user',
    'product',
    'group',
    'notifications',
    'offer',
    'feedStream',
    'chat',
    'comments',
    'errorReport',
)

# Facebook settings are set via environment variables
FACEBOOK_APP_ID = '396639980412048'
FACEBOOK_APP_SECRET = 'c38866b6cc9e6b4fd38e3aab53043eea'
FACEBOOK_SCOPE = 'email,publish_stream,read_friendlists,publish_actions'
FACEBOOK_FORCE_SIGNUP = True
AUTH_PROFILE_MODULE = 'facebook.FacebookProfile'
LOGIN_REDIRECT_URL = '/account/profile/'

#keys pusher 
PUSHER_APP_KEY = '0a155cbf4316a666b475'
PUSHER_APP_ID = '54617'
PUSHER_APP_SECRET = '6a8239047c2b4d44d7c1'

AUTHENTICATION_BACKENDS = (
    'facebook.backend.FacebookBackend',
    'mongoengine.django.auth.MongoEngineBackend',
)

SITE = 'django.contrib.sites.models.Site'

#CAMBIAR EN PRODUCCION
ACCOUNT_ACTIVATION_DAYS = 60

DEFAULT_FROM_EMAIL = 'Kamvia.mx <kamvia@kamvia.mx>'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'kamvia@kamvia.mx'
EMAIL_HOST_PASSWORD = 'kamvia2013'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

try:
    from settings_local import *
except ImportError:
    pass
