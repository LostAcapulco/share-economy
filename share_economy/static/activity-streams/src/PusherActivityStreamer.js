function PusherActivityStreamer(activityChannel, ulSelector, options,id) {
  var self = this;
  
  this._email = null;
  
  options = options || {};
  this.settings = $.extend({
    maxItems: 100
  }, options);

  this._activityChannel = activityChannel;
  this._activityList = $(ulSelector);
  
  this._activityChannel.bind('activity', function(activity) {
      self._handleActivity.call(self, activity, activity.type);
    });
  this._activityChannel.bind('page-load', function(activity) {
      self._handleActivity.call(self, activity, 'page-load');
    });
  this._activityChannel.bind('test-event', function(activity) {
      self._handleActivity.call(self, activity, 'test-event');
    });
  this._activityChannel.bind('scroll', function(activity) {
      self._handleActivity.call(self, activity, 'scroll');
    });
  this._activityChannel.bind('like', function(activity) {
      self._handleActivity.call(self, activity, 'like');
    });

  this._itemCount = 0;
};

PusherActivityStreamer.prototype._handleActivity = function(activity, eventType) {
  var self = this;
  var activityItem = PusherActivityStreamer._buildListItem(activity);
 var urlPath = window.location.pathname;

  urlPath == '/chat/' ? 
    renderChat(selectMessage(activity.body,1),selectMessage(activity.body,2),selectMessage(activity.body,3)):
    activity = activity;

  ++this._itemCount;

  activityItem.addClass(eventType);
  activityItem.hide();
  this._activityList.append(activityItem);
  this._activityList.animate({scrollBottom: ($('.comments-product ul li').length * 50)}, 800);
  activityItem.slideDown(500);

  if(this._itemCount > this.settings.maxItems) {
    this._activityList.find('li:last-child').fadeOut(function(){
      $(this).remove();
      --self._itemCount;
    });
  }
};

PusherActivityStreamer._timeToDescription = function(time) {
  if(time instanceof Date === false) {
    time = Date.parse(time);
  }
  var desc = "dunno";
  var now = new Date();
  var howLongAgo = (now - time);
  var seconds = Math.round(howLongAgo/1000);
  var minutes = seconds/60;
  var hours = minutes/60;
  if(seconds === 0) {
    desc = "justo ahora";
  }
  else if(minutes < 1) {
    desc = "justo ahora";
  }
  else if(minutes < 60) {
    desc = " hace " + minutes;
    if(minutes==1)
    	desc += " m";
    else
    	desc += " m";
  }
  else if(hours < 24) {
    desc = hours + "hace " + hours;
    if(hours==1)
    	desc += " h";
    else
    	desc += " hrs";
  }
  else {
    desc = time.getDay() + " " + ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][time.getMonth()]
  }
  return desc;
};

PusherActivityStreamer.prototype.sendActivity = function(activityType, activityData) {
  var data = {
    activity_type: activityType,
    activity_data: activityData
  };
  if(this._email) {
    data.email = this._email;
  }
  $.ajax({
    url: '/stream/send/',
    data: data
  })
};

PusherActivityStreamer.prototype.setEmail = function(value) {
  this._email = value;
};

PusherActivityStreamer._buildListItem = function(activity) {
  var li = $('<li class="activity"></li>');
  var item =  '<div class="comment bg-gray left">' +
                '<div class="images left" style="height:50px;width:50px">' +
                  '<img src="' + activity.actor.image.url + '" width="50px" height="50px" />' +
                '</div>' +
                '<div class="info-person left">' +
                  '<span class="comment-name bold" title="' + activity.actor.displayName + '">' + activity.actor.displayName + '</span><br />' +
                  '<span class="comment-date">' + PusherActivityStreamer._timeToDescription(activity.published) + '</span>' +
                '</div>' +
                '<div class="comment-text left"><span>' + selectMessage(activity.body,0) + '</span></div>' +
              '</div>';

  li.append(item);
  return li;
};


var selectMessage = function(str,opt){
  var data = '';
  var i = 0;

  $.each(str,function(index,value){
    value != '#' ? i = i : i += 1;
    i == opt ? data += value : data = data;
  });

  data = data.replace('#','');

  return data;
}