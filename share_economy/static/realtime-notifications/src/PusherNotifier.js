function PusherNotifier(channel, options) {
  options = options || {};
  
  this.settings = {
    eventName: 'notification',
    title: 'Kamvia',
    titleEventProperty: null, // if set the 'title' will not be used and the title will be taken from the event
    image: 'https://s3-us-west-2.amazonaws.com/kamviaassets/128x128.jpg',
    eventTextProperty: 'message',
    eventEviormentProperty: 'enviorment',
    gritterOptions: {}
  };
  
  $.extend(this.settings, options);
  
  var self = this;
  channel.bind(this.settings.eventName, function(data){ self._handleNotification(data); });
};
PusherNotifier.prototype._handleNotification = function(data) {
  var gritterOptions = {
   title: (this.settings.titleEventProperty? data[this.settings.titleEventProperty] : this.settings.title),
   text: data[this.settings.eventTextProperty].replace(/\\/g, ''),
   image: this.settings.image
  };

  $.extend(gritterOptions, this.settings.gritterOptions);
  //MANDA UN ALERT CON EL ENVIORMENT
  if (data[this.settings.eventEviormentProperty] == 'offer') {
    $('#offers').removeClass('green').addClass('white bg-red');
  }

  $.gritter.add(gritterOptions);
  page = window.location.href
  if('offer'==page.match(/offer/g))
     location.reload(); 
  getNotifications();
};