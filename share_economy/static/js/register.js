/**
 * @author Joselo 
 */
function RegisterCtrl($scope){
	$scope.startKamvia =  function(){
		kamviaRegSelect($scope.form_id,'s');
		$scope.kamvia_next_id = 'kamvia_id_24';
	}

	$scope.nextKamvia = function(){
		$scope.kamvia_next_id = kamviaRegSelect($scope.form_id,'n');
	}

	function kamviaRegSelect(form_id,kamvia_id_next){
			var id = '';
			switch(form_id){
				case 'form_id_11':
					$scope.kamvia_action = '/account/register/';
					if(kamvia_id_next=='n')
						id = 'kamvia_id_22';
				break;

				case 'form_id_12':
					$scope.kamvia_action = '/account/register_trust/';
					if(kamvia_id_next=='n')
						 id = 'kamvia_id_23';
				break;

				case 'form_id_13':
					$scope.kamvia_action = '/account/register_comun/';
					if(kamvia_id_next=='n')
						id = 'kamvia_id_24';
				break;
				
				case 'form_id_21':
					$scope.kamvia_action = '/product/upload/';
					if(kamvia_id_next=='n')
						 id = 'kamvia_id_22';
				break;

				case 'form_id_22':
					$scope.kamvia_action = '/product/promote/';
					if(kamvia_id_next=='n')
						id = 'kamvia_id_23';
				break;

				case 'form_id_33':
					$scope.kamvia_action = '/account/profile/';
					if(kamvia_id_next=='n')
						id = 'kamvia_id_24';
				break;
			}
			return id; 
	}

}

$(function(){
	$('#uploadify').bind('allUploadsComplete', function(e, data){
		alert("exito!!");
   });
});

