/*
HTML5 File Drag & 
Eyden Barboza (@mantus) 
*/
function hiddenItems(){
	$('#send-button').replaceWith('<button id="send-button" class="left inActive" type="button" onclick="check()">Comenzar ya</button>');
	$("#fileselect").css("display", "none");
	$("#buttonFileUpload").css("display", "none");
}
window.onload=(function() {


	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}

	// output information
	function Output(msg) {
		var m = $id("messages");
		m.innerHTML = m.innerHTML + msg;
	}

	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}

	// file selection
	function FileSelectHandler(e) {
		// cancel event and hover styling
		FileDragHover(e);
		// fetch FileList object
		var files = e.target.files || e.dataTransfer.files;
		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
			UploadFile(f);
		}
	}


	// output file information
	function ParseFile(file) {
		Output(""
			// "<p>File information: <strong>" + file.name +
			// "</strong> type: <strong>" + file.type +
			// "</strong> size: <strong>" + file.size +
			// "</strong> bytes</p>"
		);

		// display an image
		if (file.type.indexOf("image") == 0) {
			var reader = new FileReader();
			reader.onload = function(e) {
				Output(
					// "<p><strong>" + file.name + ":</strong><br />" +
					'<p data-url="" class="photo" style="background-image: url(' + e.target.result +
					')"><img id="del" style="display:none;" src="https://bitbucket.org/LostAcapulco/share-economy/raw/16b38b8525e766677027d018920608c8feaf2d58/share_economy/static/img/error.png"></p>'
					 // + '<img src="' + e.target.result + '" /></p>'
				);
			}
			reader.readAsDataURL(file);
		}

		// display text
		if (file.type.indexOf("text") == 0) {
			var reader = new FileReader();
			reader.onload = function(e) {
				Output(
					"<p><strong>" + file.name + ":</strong></p><pre>" +
					e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;") +
					"</pre>"
				);
			}
			reader.readAsText(file);
		}
	}


	// upload JPEG files
	function UploadFile(file) {
		hiddenItems();
		// following line is not necessary: prevents running on SitePoint servers
		if (location.host.indexOf("sitepointstatic") >= 0) return
		var images_p = $('#messages p').length;
		var xhr = new XMLHttpRequest();
		if (xhr.upload && file.type == "image/jpeg" && images_p <= 4) {
			$('#error-imge-upload').html('');
			ParseFile(file);

			// create progress bar
			var o = $id("progress");
			var progress = o.appendChild(document.createElement("p"));
			// progress.appendChild(document.createTextNode("upload " + file.name));
			// progress bar
			xhr.upload.addEventListener("progress", function(e) {
				var pc = parseInt(100 - (e.loaded / e.total * 100));
				progress.style.backgroundPosition = pc + "% 0";
			}, false);

			// file received/failed
			xhr.onreadystatechange = function(e) {
				if (xhr.readyState == 4) {
					progress.className = (xhr.status == 200 ? "success" : "failure");
					if(xhr.status == 200){
	    				$('#send-button').replaceWith('<input type="submit" id="send-button" value="Comenzar ya" class="left active"/>');
						if(xhr.responseText!='False'||xhr.responseText!=''||xhr.responseText!=' ')
							$('#messages .photo:last-child').attr('data-url',xhr.responseText);
					}else{
						$('#error-imge-upload').html('Error al subir la image');
					}
				}
			};
			// start upload

            var formData = new FormData();
            formData.append("thefile", file);
			xhr.open("POST", $id("upload").action, true);
	    	response=xhr.send(formData);
		} else {
			if (images_p > 5) {
				$('#error-imge-upload').html('Esto esta limitado a 5 imagenes');
			} else if(file.type != "image/jpeg"){
				$('#error-imge-upload').html('El formato de la imagen no es JPEG');
			}else {
				$('#error-imge-upload').html('La imagen es demasiado grande');
			}
		}

	}


	// initialize
	function Init() {

		var fileselect = $id("fileselect"),
			filedrag = $id("filedrag"),
			submitbutton = $id("submitbutton");
		// file select
		fileselect.addEventListener("change", FileSelectHandler, false);
		$("#fileselect").css("display", "none");
		$("#buttonFileUpload").css("display", "none");
		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {

			// file drop
			filedrag.addEventListener("dragover", FileDragHover, false);
			filedrag.addEventListener("dragleave", FileDragHover, false);
			filedrag.addEventListener("drop", FileSelectHandler, false);
			filedrag.style.display = "block";

			// remove submit button
			submitbutton.style.display = "none";

		}

	}

	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		Init();
	}
	hiddenItems();
})();
