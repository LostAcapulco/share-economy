/**
Mod
Description:
Return the  if the lvalue is the remainder of rvalue
Example:
{{#mod 6 3}} YES {{else}} NO {{/mod}}
*/

Handlebars.registerHelper('mod', function(lvalue, rvalue, options) {
	return (lvalue % rvalue === 0) ? options.fn(this) : options.inverse(this);
});


/**
Gt
Description:
Return true if lvalue is greater than rvalue, can handle lengths of arrays
Example:
{{#gt 3 1}} YES {{else}} NO {{/gt}}
*/

Handlebars.registerHelper('gt', function(lvalue, rvalue, options) {
	var rvalue = (rvalue instanceof Array) ? rvalue.length : rvalue;
	var lvalue = (lvalue instanceof Array) ? lvalue.length : lvalue;
	return (lvalue > rvalue) ? options.fn(this) : options.inverse(this);
});

/**
EQ
Description:
Return true if lvalue is equals to rvalue, can handle lengths of arrays
Example:
{{#eq 3 3}} YES {{else}} NO {{/eq}}
*/

Handlebars.registerHelper("eq", function(lvalue, rvalue, options) {
	var rvalue = (rvalue instanceof Array) ? rvalue.length : rvalue;
	var lvalue = (lvalue instanceof Array) ? lvalue.length : lvalue;
	return (lvalue === rvalue) ? options.fn(this) : options.inverse(this);
});

/*
Index
Description:
return the value into lvalue with the rvalue as index
Example:
{{index array 5}}
*/

Handlebars.registerHelper("index", function(lvalue, rvalue) {
	return lvalue[rvalue];
});

/*
quantity
Description:
give the number the cuantity format
Example:
{{quantity 2000}}
*/

Handlebars.registerHelper("quantity", function(number) {
	return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
});

/**
length
Description:
Return the length of string, array or literal object
Example:
{{length something}}
*/

Handlebars.registerHelper("length", function(something) {
	var length;
	if(typeof something === "string"){
		length = something.length;
	}else if(Object.prototype.toString.call(something) === '[object Array]'){
		length = something.length - 1;
	}else{
		length = Object.keys(something).length;
	}
	return length;
});