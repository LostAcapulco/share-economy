/**
 * @author Joselo
 */

// To prevent the conflict of `{{` and `}}` symbols
// between django templating and angular templating we need
// to use different symbols for angular.
'use strict';

var kamviaApp = angular.module('kamviaApp', []) 
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    });
