$(document).ready(function() {

  // Get notifications
  if( window.location.pathname != '/account/register/')getNotifications();
  if( window.location.pathname == '/account/home/')showPopUp();
  var flagclickCat = false;
  var categories = ['Artículos de Colección y Antigüedades','Artículos para mascotas','Bicis, Motos y Transporte','Computación, Celulares, Gadgets y Electrónica','Deportes y Actividades al aire libre','Fotografía, Arte y Diseño','Herramientas, Muebles, Electrodomésticos y Hogar','Juguetes y Artículos para Niños y Bebes','Libros, Revistas y Comics','Música e Instrumentos musicales','Otros','Ropa, Zapatos y Accesorios','Salud y Belleza','Servicios','Videojuegos y Películas'];
  var expanded = false;

  // Remove search content
  $('#content').click(function() {
    hideNotifications();
  });

  // General header search
  $('#general-search').focusin(function(event) {
    if ($(this).val().length > 0) {
      $(this).addClass('text-left');
    } else {
      $(this).removeClass('text-left');  
    }

    return false;
  }).focusout(function(event) {
    if ($(this).val().length > 0) {
      $(this).addClass('text-left');
    } else {
      $(this).removeClass('text-left');  
    }

    return false;
  }).keyup(function(event) {
    if ($(this).val().length > 0) {
      $(this).addClass('text-left');
    } else {
      $(this).removeClass('text-left');  
    }

    return false;
  });

  /*$('#general-search-form').submit(function() {
    var product = $('#general-search').val();
    
    $.post('/product/latest/', { product: product }, function(data) {
        searchDialog(data);
        $('#general-search').val(product);
    });

    return false;
  });*/

  //Evento que maneja la visualizacion de categorias en el header
  $(document).delegate('.bars-icon','click',function() {

    var e = window.event||arguments.callee.caller.arguments[0];
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation) e.stopPropagation();
    if (e.preventDefault) e.preventDefault();
     if (!expanded){
         slideDownMenuCat(categories);
         expanded = true;
        }else{
         slideUpMenuCat(categories);
         expanded = false;
        }

    /*if(flagclickCat==false){
      slideDownMenuCat(categories);
      flagclickCat = true;
    }else if(flagclickCat==true){
      slideUpMenuCat(categories);
      flagclickCat=false;
    }*/
  });

  //Trae articulos por categoria
  $(document).delegate('.categories-list .cat-list','click',function(){
    var categoria = $(this).text();
    getProductsByCat(categoria);
    slideUpMenuCat(categories);
    return false;
  })

  //Cambia estilo al hover
  $(document).delegate('.categories-list li', 'mouseenter',function(){
    $(this).css("background-color","#498897");
    $(this).css("color","#FFFFFF");
  }).delegate('ul.categories-list li', 'mouseleave',function(){
    $(this).css("background-color","#FFFFFF");
    $(this).css("color","#000000");
  });

  $('#get-notifications').click(function() {
    var chanel = $(this).attr('href');

    $('#get-notifications img').attr('src', '/static/img/notificaciones0.svg');
    $('.notifications-number').remove();

    $.post('/notifications/get/', { channel_id: chanel }, function(data) {
      notificationsPreview(data);
    });

    return false;
  });

  // Hover menu group
  $('#menu-group').mouseover(function() {
    $(this).children('.menu-tool-content').show();

    return false;
  }).mouseout(function() {
    $(this).children('.menu-tool-content').hide();

    return false;
  });

  // Hover menu settings
  $('#menu-tool').mouseover(function() {
          $(this).children('.menu-tool-content').show();

          return false;
  }).mouseout(function() {
          $(this).children('.menu-tool-content').hide();

          return false;
  });

  //Abre el fancybox para los wishlist
  body = $(".bg-gray");
  
  (function (elems) {
    $(elems).click(function (e) {
      $.fancybox({
      'height'        : '500px',
      'width'         : '350px',
      'href' : $(this).attr("href"),
      'type'          : 'iframe'
      });
      return false;
    });
  })(body.find("a[id^='wishlists']"));

  $('#issues').click(function() {
    var content = '<div id="send-message-container" class="bg-white container-400">' +
                    '<h2 class="text-12 normal">Reporta un Problema</h2>' +
                    '<div class="send-message-info">Ayudanos a mejorar</div>' +
                    '<div class="send-message-content">' +
                      
                        '<textarea type="text" placeholder="Describe tu problema con el sitio a detalle" class="bg-gray" name="message" id="issue-message"></textarea>' +
                        '<br />' +
                        '<button id="submit-create-issue" class="btn btn-blue right">Enviar</button>' +
                        '<button class="btn btn-red right send-message-container-close">Cancelar</button>' +
                        '<div  class="clear"></div>' +
                     
                    '</div>' +
                  '</div>';

    $.fancybox.open(content, { 'closeBtn' : false, 'padding'   : 0 }, { 'closeBtn' : false });

    return false;
  });

  // hover in friends
  $(document).delegate(".friend-info", "mouseover", function(){
    $(this).children('.over-friend-info').css('display','block');

    return false;
  }).delegate(".friend-info", 'mouseleave', function() {
    $(this).children('.over-friend-info').css('display','none');

    return false;
  });

  // Hover in product
  $(document).delegate(".product", "mouseover", function(e){
    $(this).children('a').children('.products-container-hover').css('display','block');
    $(this).children('.product-ctrls').css('display','block');

    return false;
  }).delegate(".product", 'mouseleave', function(e) {
    $(this).children('a').children('.products-container-hover').css('display','none');
    $(this).children('.product-ctrls').css('display','none');

    return false;
  });

  // User register section
  if ($('#register-form').length > 0) {
    $('#register-form input.required').focusout(function() {
      validForm(this, '#register-form');

      return false;
    });
  }

  $('#id_tags').focusout(function() {
    var tags = $(this).val().split('#').length;
    
    if (tags < 1 || tags > 4) {
      $(this).addClass('error-form').removeClass('success-form');
      showFancyBoxMessage('Error','Debes ingresar de 1 a 3 tags',true);
      
    } else {
      $(this).removeClass('error-form').addClass('success-form');
    }

    return false;
  });

  // Open the file content when click in image
  $('#filedrag').click(function(event) {
    $('.image-preview input:file').trigger('click');

    return false;
  });

  $('.image-preview input:file').change(function() {
    if(window.location.pathname=='/account/register/')
      $.post('/account/picture/', { name: $(this).val(), filename: $(this).val() }, function(data) {
      
    });

    return false;
  });

  // Product gallery
  if ($('.img-galery').length > 0) {
    var first_gallery_image = $('.regiter-galery-option li:first-child').addClass('active').data('image');
    $('.img-galery').attr('style', first_gallery_image);
    var ulr_image = $('.regiter-galery-option li:first-child').data('url-image');
    $('.open-img-gallery').data('url-image', ulr_image);

    $('.regiter-galery-option li').mouseover(function() {
      // delete class active in tabs
      $('.regiter-galery-option li').removeClass('active');
      var gallery_image = $(this).addClass('active').data('image');
      $('.img-galery').attr('style', gallery_image);
      $('.open-img-gallery').data('url-image', $(this).data('url-image'));
      
      return false;
    });

    $('.open-img-gallery').on('click', function() {
      $.fancybox.open('<img src="' + $(this).data('url-image') + '" width="600px">', { 'closeBtn' : false  });

      return false;
    });
  }

        $('.open-img-profile').on('click', function() {
            $.fancybox.open('<img src="' + $(this).data('url') + '">', { 'closeBtn' : false }, { 'closeBtn' : false });

            return false;
        });

        $('#menu-down').mouseover(function() {
                $('#menu-down-container').show();

                return false;
        }).mouseout(function() {
                $('#menu-down-container').hide();

                return false;
        });

        //ACTION TO OPEN A CHAT CONVERSATION FROM CHAT PAGE
        $(document).delegate("a.open-chat", "", function() {
                var id = $(this).attr('href');
                var second_user = $(this).data('user-send');
                renderChat(id,second_user);
                return false;
        });

        // Search Groups
        $('#search-group div.tab').click(function() {
                $('#search-group div.tab').removeClass('active');
                $(this).addClass('active');
                var type = $(this).data('type');

                $.post('/group/search/', { type_group: type }, function(data) {
                        getGroups(data);
                });

                return false;
        });

        // Search in group
        $('#search-in-group').submit(function() {
                var search = $('#search-value').val();
                $('#search-value').val('');
                
                $.post('/group/search/', { type_group: 'search', search: search }, function(data) {
                        getGroups(data);
                });

                return false;
        });

        // Groups hover
        $(document).delegate('.product-info','mouseover', function() {
                $(this).children('.over-product-info').slideDown();

                return false;
        }).delegate('.product-info', 'mouseleave', function() {
                $(this).children('.over-product-info').slideUp();

                return false;
        });

        //Resonse invite group
        $(document).delegate('#invite-group','click', function(event) {
                var invite_type = $(this).attr('invite_type');
                var group = $(this).data('group');
                $(this).replaceWith('<div class="ctrls-message-confirm">'+
                                                        '<button id="response-invite" class="text-12 button-form bg-red white" data-resp="cancel" '+
                                                        'data-group="'+group+'" data-type="' + invite_type + '">Cancelar</button>'+
                                                        '<button id="response-invite" class="text-12 button-form bg-green white" data-resp="acept" '+
                                                        'data-group="'+group+'" data-type="' + invite_type + '">Aceptar</button>' +
                                                        '</div>').fadeIn(500);

                return false;
        });

        $(document).delegate('.photo','mouseover',function(){
            $(this).children('#del').css('display','block');
        });

        $(document).delegate('.photo','mouseleave',function(){
            $(this).children('#del').css('display','none');
        });

        $(document).delegate('.photo','click',function(){
            var url = $(this).data('url');
            var obj = $(this);
            $.post('/product/delete_picture/',{url:url},function(data){
                if(data=='True'){
                    obj.css('display', 'none');
                    $('#progress p:last-child').css('display','none');
                }else{
                    showFancyBoxMessage('Error','No pudimos borrar la imagen',true);
                }
            });
        });

        //Acept invite
        $(document).delegate('#response-invite', 'click', function(event){
                var resp = $(this).data('resp');
                var group = $(this).data('group');
                var type = $(this).data('type');
                response_invite(resp,type,group);
        });
        
        // Add article in group 
        $('#add-article, .add-product').click(function() {
                var id = $(this).attr('href');
                var group = $(this).data('group');

                $.post('/product/products_all/', { user: id }, function(data) {
                        data = JSON.parse(data);
                        var li = '';

                        $.each(data.products, function(key, value) {
                                li = li + '<li class="bold"><input type="checkbox" name="product" class="product-group left" value="' + value[0].product_id + '"> <img src="' + value[0].product_img + '" height="50px" width="50px" class="left"> <span class="left bolck text-12">' + value[0].product_name + '</span><div class="clear"></div></li>';
                        });

                        $.fancybox.open(
                                                                                        '<div class="add-in-group-content">' +
                                                                                                '<div id="list-content" class="left">' +
                                                                                                        '<ul>' + li + '</ul>' +
                                                                                                '</div>' +
                                                                                                '<div class="add-product-ctrl">' +
                                                                                                        '<button id="add-products" class="button-big right bg-yellow white" data-group="' + group + '" data-user="' + id + '">Agregar</button>' +
                                                                                                '</div>' +
                                                                                        '</div>',
                                                                                        { 'closeBtn' : false }
                        );

                });

                return false;
        });

        // Invite friends
        $('#invite-friends').click(function() {
                var id = $(this).attr('href');
                var gpo_id = $(this).attr('data-group');
                var group = $(this).data('group');

                $.post('/group/invite/', { profile_id:id,group_id:gpo_id }, function(data) {
                        data = JSON.parse(data);
                        var li = '';

                        $.each(data.friends, function(key, value) {
                                li = li + '<li class="bold"><input type="checkbox" name="friend" class="friend-group left" value="' + value[0].friend_id + '"> <img src="' + value[0].friend_pic + '" height="50px" width="50px" class="left"> <span class="left bolck text-12">' + value[0].friend_name + '</span><div class="clear"></div></li>';
                        });

                        $.fancybox.open(
                                                                                        '<div class="add-in-group-content">' +
                                                                                                '<div id="list-content" class="left">' +
                                                                                                        '<ul>' + li + '</ul>' +
                                                                                                '</div>' +
                                                                                                '<div class="add-friends-ctrl">' +
                                                                                                        '<button id="add-friends" class="button-big right bg-yellow white" data-group="' + group + '">Agregar</button>' +
                                                                                                '</div>' +
                                                                                        '</div>',
                                                                                        { 'closeBtn' : false }
                        );

                });

                return false;
        });
        
        // Send invitation
        $(document).delegate('#add-friends', 'click', function() {
                var group = $(this).data('group');

                $('#list-content ul li input[type="checkbox"]:checked').each(function(index) {
                        var friend_id = $(this).val();
                        $.post('/group/send_invite/', { user: friend_id, group: group, invite_type: 1 }, function(data) {
                                $('.add-in-group-content').html('<div class="text-center text-14 bold green">' + data + '</div>');
                        }).error(function() {
                                $('.add-in-group-content').html('<div class="text-center text-14 bold green">' + data + '</div>');
                        });
                });

                return false;
        });

        //Acept friend request
        $(document).delegate('.acept-request', 'click', function(){
            var user_id = $(this).data('id');
            var not_id = $(this).data('notify');

            $.post('/account/resp_friend_request/',
                    {friend_id:user_id, notification_id:not_id, response:'1'},
                    function(data){
                      if(data=='True'){
                        showFancyBoxMessage('Solicitud de Amistad','Ahora son amigos',true);
                        
                      }else{
                        showFancyBoxMessage('Solicitud de Amistad','Ocurrio un error al responder la solicitud.',true);
                      }
                    });
        });

        //Cancel friend request
        $(document).delegate('.cancel-request', 'click', function(){
            var user_id = $(this).data('id');
            var not_id = $(this).data('notify');

            $.post('/account/resp_friend_request/',
                    {friend_id:user_id, notification_id:not_id, response:'0'},
                    function(data){
                      if(data=='True'){
                        showFancyBoxMessage('Solicitud de Amistad','Solicitud cancelada',true);
                      }else{
                        showFancyBoxMessage('Error','Ocurrio un error al responder la solicitud.',true); 
                      }
                    });
        });

        //FUNCION PARA VER MÁS NOTIFICACIONES
        $(document).delegate('#ver-mas','click',function(){
                window.location.href ="../../../account/home/#news";
        });

        $('#search-error-form').submit(function(event) {
                var search = $('#search').val();
                if (search != '') {
                        location = "/account/home/#" + search;
                }

                return false;
        });

        //Show the user chat who made an offer over one product
        $(document).delegate('.chat-select', 'click', function(event) {
          var offer_id = $(this).data('offerid');
          window.location.href = '/offer/current/'+offer_id;

          return false;
        });

        // Select tab in profile articule
        $(document).delegate('.tab-info', 'click', function(event) {
          var content = $(this).data('content');

          $('.tab-info').removeClass('active-tab');
          $(this).addClass('active-tab');

          //Hide the comments or the chat
          $('.tab-info-container').hide();

          switch(content){
            case '#info-comments-product':
              $('.mini-profile-user').hide();
              $('#send-comment').css('display','none');
              $('#send-chat').css('display','block');
            case '#info-inbox':
              $('.mini-profile-user').show();
              $('#send-comment').css('display','block');
              $('#send-chat').css('display','none');
            default:
              $('.mini-profile-user').show();
              $('#send-comment').css('display','block');
              $('#send-chat').css('display','none');
          }

          $(this).show();
          $(content).show();

          return false;
        });

        //Send a product comment
      $(document).delegate('#send-comment','submit',function() {
          var form_data = $(this).serializeArray();
          var comment = form_data[1];
          var product_id = form_data[2];
          addComment(comment.value,product_id.value);
          $("#comment-post").val('');

          return false;
        });

      //Send chat text
      $(document).delegate('#chat','submit',function(){
                var $this = $(this);
                $.ajax({
                    url: $this.attr('action'),
                    type: $this.attr('method'),
                    data: $this.serialize(),
                }).done(function(data) {
                  getChats('{{ request.user.id }}');
                });
                this.reset();
                return false;
        });


        // Open overlay to the offer
        $(document).delegate("#offer", 'click', function() {
          var source = $('#send-message-template').html();
          var template = Handlebars.compile(source);
          //if($(this).data('usrid')!='')$("")replaceWith($(this).data('username'));

          $('#name-owner-product').html('fobar');
          $('#message-chat').html('fobar');
          $.fancybox.open(template(), { 'closeBtn' : false, 'padding'   : 0 });

          if($(this).data('usrid')){
            showBoxMessage($(this));
          }

          return false;
        });

        // Close overlay offer
        $(document).delegate('.send-message-container-close', 'click', function(event) {
          $.fancybox.close();

          return false;
        });

        //ACTION TO HANDEL ACTION OVER PEOPLE WHO HAS OFFER ON A PRODUCT
        $(document).delegate('.product-more-info', 'mouseover', function(event) {
          var check = $('#chat-title-user').is(':visible');
          var data = $('#chat-title-user').data('check');
          if(check&&data==true){
            $('#chats-container').css('display','none');
          }else{
            $(this).children('.product-more-info-content').show();
          }

          return false;
        });

        $(document).delegate('.product-more-info', 'mouseleave', function(event) {

          $(this).children('.product-more-info-content').hide();

          return false;
        });

        //redirecciona a tus amigos
        $(document).delegate('.friends','click', function(){
            window.location.href ="../../../account/profile/#profile-friends";
        });
        

        $('.show-notif').on('click', function(){
          reloadNews();
          hideNotifications();

          return false;
        });

        $(document).delegate('#submit-create-issue','click',function() {
                var issue_message = $('#issue-message').val();
                $('#issue-message').val('');
                
                $.post('/account/error/', { message: issue_message }, function(data) {
                        if(data=='ok')
                          showFancyBoxMessage('Mensaje','El mensaje ha sido enviado',true);
                        else
                          showFancyBoxMessage('Error','Intenta nuevamente',true);
                });

                return false;
        });

        //ACTION TO CLICK ON NOTIFICATION MESSAGE
        $(document).delegate('#noti-link', 'click', function(){
                data_link = $(this).data('link');
                if(data_link!=null)
                  '/account/profile/'==location.pathname ? changeTab('profile-offers'): window.location.href = '/offer/current/'+data_link;
                else
                  window.location.href == '/account/profile/#profile-offers'
        });

        //MANEJO DE ACCION DE CLICK BOTON OFERTAS
        $(document).delegate('#offers','click', function(){
            url = window.location.href;
            if('/account/profile/'==url.match('/account/profile/'))
              changeTab('profile-offers');
            else 
              window.location.href = '/account/profile/#profile-offers'
        });

        //MANEJO CALIFICACION DE USUARIO
        $(document).delegate('.rate','click',function(){
          var rate = 0;
          var value = 0;
          content = $("#rating_user");
          rate = $(this).data('rate');

          $('.rate_user').attr('rank',rate);

          (function ($, elems) {
            $(elems).each(function (i, el) {
              status = $(el).data('status');
              if(status=='full'){
                $(el).html('☆');
              }
            });
          })(jQuery, content.find("[id^='star_rate_']"));

          (function ($, elems) {
            $(elems).each(function (i, el) {
              status = $(el).data('status');
              console.log("STATUS: "+ status)
              value = $(el).attr("id").replace('star_rate_','');
              console.log("VALUE: "+value)
              console.log("RATE: "+rate)
              if(value<=rate){
                $(el).html('★');
                $(el).data('status','full');
              }
            });
          })(jQuery, content.find("[id^='star_rate_']"));
        });

        //MANEJO DE ENVIO DE CALIFICACION DE USUARIO
        $(document).delegate('.rate_user','click',function(){
          var rating = $(this).attr('rank');
          var user_to_rate = $(this).data('userid');
          var user_type = $(this).data('usertorate');
          var offer_id = $(this).data('offerid');
          $(this).hide()
          $.post('/offer/add_rating/', 
                { user_rated_id: user_to_rate, 
                 rating: rating, 
                 user_type:user_type,
                 offer_id: offer_id}, 
                 function(data) {
                        if(data=='True')
                          showFancyBoxMessage('CALIFICACION','Se ha calificado al usuario',true);
                        else
                          showFancyBoxMessage('CALIFICACION','Intenta nuevamente',true);
                });

                return false;
        });
});

//FUNCION PARA TRAER UN CHAT CON UN USUARIO
function getUserChat(id,type_id){
        $.post('/chat/retrieve_message/', { search_id: id, type_id:type_id }, function(data) {
                                                                        var chat_message = JSON.parse(data).chats;
                                                                        var content = '';
                                                                        var username = '';
                                                                                $.each(chat_message, function(key, value) {
                                                                                        tamMen = value[0].message.length;
                                                                                        username = value[0].user_receptor;
                                                                                        if(tamMen>66){
                                                                                                tam = Math.round((tamMen*303)/370);
                                                                                        }else{
                                                                                                tam = 50
                                                                                        }

                                                                                        content += '<li>' +
                                                                                                      '<div class="comment bg-gray left">' +
                                                                                                        '<div class="images left" style="height:50px;width:50px">' +
                                                                                                          '<img src="' + value[0].picture + '" width="50px" height="50px" />' +
                                                                                                        '</div>' +
                                                                                                        '<div class="info-person left">' +
                                                                                                          '<span class="comment-name bold" title="' + value[0].username + '">' + value[0].username + '</span><br />' +
                                                                                                          '<span class="comment-date">'+value[0].date.substring(0,10)+'</span>' +
                                                                                                        '</div>' +
                                                                                                        '<div class="comment-text left"><span>' + value[0].message+ '</span></div>' +
                                                                                                      '</div>' +
                                                                                                    '</li>';
                                                                                });
                                                                        $('#chat-title').html('Conversación con '+username);
                                                                        $('.add-comment-info').html('Presiona Enter para enviar');
                                                                        $('.button-send-comment').val('Agregar un comentario');
                                                                        $('.mensaje-input').attr('placeholder','Agrega un nuevo comentario');
                                                                        $('.comments-product ul#chat-comments-list').html(content);

        }); 
  return true;
}

//Reload News
function reloadNews(){
        data_request = JSON.parse(localStorage.request);
        getNews(data_request.id, data_request.username);
}

//ajax to send comment
function addComment(comment,product_id) {
        $.ajax({
          type: 'POST',
          url: '/comments/send/',
          data: {
          'enviorment_id':product_id,
          'enviorment_type':'product_profile',
          'text':comment
          }
        }).done(function(data){
          if(data!='True')
            showFancyBoxMessage('Comentarios','No pudimos enviar tu comentario.',true);
        });
        }

//Get today date
function getTodayDate(){
  var d = new Date();
  var today = d.toUTCString();

  today = today.substring(0,16);

  return today;
}

//match date with today
function matchDateToday(date){

  var today = getTodayDate();
  var dateAnt = date.substring(0,16)

  dateAnt.match(today) ? when = 'Hoy' : when = transDate(dateAnt);

  return when;
}

//Translate date
function transDate(date){
  var ingDay = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat','','','','','');
  var espDay = new Array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
  var ingMonth = new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
  var espMonth = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

  for(var i=0; i<12; i++){
    if(date.match(ingMonth[i])!='')date = date.replace(ingMonth[i],espMonth[i]);
    if(date.match(ingDay[i])!='')date = date.replace(ingDay[i],espDay[i]);
  }

  return date;
}

//LOAD CHATS FOR A GIVEN chatId,secondUser
      function loadChatsOffer(chat_env){
        var id = chat_env;

        $.post('/chat/retrieve_message/', { search_id: id }, function(data) {
          // Se parsean los datos
          var chat_message = JSON.parse(data).chats;
          // Se inicializa la lista de comentarios
          var content = '<ul>';
          // Se llena la lista de comentarios
          $.each(chat_message, function(key, value) {
            content = content + '<li>' +
                                  '<div class="comment bg-gray left">' +
                                    '<div class="images left">' +
                                      '<img src="' + value[0].picture + '" title="' + value[0].username + '" width="50px" height="50px" />' +
                                    '</div>' +
                                    '<div class="info-person left">' +
                                      '<span class="comment-name bold" title="' + value[0].username + '">' + value[0].username + '</span><br />' +
                                      '<span class="comment-date">May 7 at 12:32pm</span>' +
                                    '</div>' +
                                    '<div class="comment-text left text-left"><span>' + value[0].message + '</span></div>' +
                                  '</div>' +
                                '</li>';
          });
          // cerramos la lista de comentarios
          content = content + '</ul>';
          // Se inserta en la seccion de inbox de la oferta
          $('#user-inbox-content').html(content);     
        }); 

        return false;
      }

//Slide up para menu categorias
function slideUpMenuCat(categories){
  $('#menu-categories-content').hide();
  $('.menu-categorias').slideUp();
    var thisCat = categories;
    thisCat.forEach(function(data){
    $('.cat-list').remove();
    });
    return false;
}
//Slide down menu categorias
function slideDownMenuCat(categories){
  var content = '';
  var thisCat = categories;
      thisCat.forEach(function(data){
         content += '<li class="cat-list" style="display:block; width:270px; height:30px; font-style: italic;'+
                    ' cursor:pointer; color:black; padding-left:10px;">'+data+'</li>';
      });
      $('.categories-list').html(content);
      $('#menu-categories-content').show();
    $('.menu-categorias').slideDown();
    return false;
}

//Create a rating stars
function ratingStars(rating,container){
  var stars = '';
  switch(rating){
    case 0:
      stars = '☆ ☆ ☆ ☆ ☆';
    case 1:
      stars = '★ ☆ ☆ ☆ ☆ ';
    case 2:
      stars = '★ ★ ☆ ☆ ☆ ';
    case 3:
      stars = '★ ★ ★ ☆ ☆ ';
    case 4:
      stars = '★ ★ ★ ★ ☆ ';
    case 5:
      stars = '★ ★ ★ ★ ★ ';
    default:
      stars = '☆ ☆ ☆ ☆ ☆ ';
  }
  $(container).html(stars);
                  
}

//Show the box message for lo quiero click button
function showBoxMessage(container){

          var url = $(container).data('pic');
          var message = 'Hola Quiero tu '+$(container).data('productname');
          var id_user = $(container).data('usrid');
          var img_url = $(container).data('imgproduct')
          var product_name = $(container).data('productname');
          var user_name = $(container).data('username');
          var product = $(container).data('id');

          console.log(url+'-'+ message+'-'+ user_name+'-'+ product+'-'+ id_user+'-'+ product_name+'-'+ img_url);

          $(".send-message-info").html('<img src="'+url+'"><span class="username"></span><span class="send-message-rank blue text-18"></span>');
          $(".username").html(user_name);
          $("#name-owner-product").html(product_name);
          $("#message-chat").html(message);

          ratingStars($(container).data('userrating'),'.send-message-rank');

          // Send offer
          $('#send-message').live('submit', function() {
            var url = $(this).attr('action');

            if($('#message-chat').val()!=''){
              sendMessage(url, message, user_name, product, id_user, product_name, img_url);

              $('#offer').addClass('inactive-icon');
            }
          
          return false;
        });
}

//RENDER CHAT FOR A GIVEN CHAT ID AND USER ID
function renderChat(id, second_user,session_id){
  if(session_id!=second_user){
    $('#second-user').val(second_user);
    $('#chat-id').val(id);
    getUserChat(id,"chat_id");
  }
}

//FUNCTION TO APPEND MESSAGE JUST send-message-template
function justSendMessage(message, urlPicture,userName){
  var li = $('<li class="activity"></li>');
  var item =  '<div class="comment bg-gray left">' +
                '<div class="images left" style="height:50px;width:50px">' +
                  '<img src="' + urlPicture + '" width="50px" height="50px" />' +
                '</div>' +
                '<div class="info-person left">' +
                  '<span class="comment-name bold" title="' + userName + '">' + userName + '</span><br />' +
                  '<span class="comment-date">Justo ahora</span>' +
                '</div>' +
                '<div class="comment-text left"><span>' + message + '</span></div>' +
              '</div>';

  li.append(item);
  return li;
}

//AGREAGA EL METODO in_array AL OBJETO ARRAY DE JAVASCRIPT PARA SU POSTERIOR USO EN user_has_offered
Array.prototype.in_array = function(value) {
  response = false;
  for(var i = 0; i < this.length; i++){
    if(this[i] == value) {
      response = true;
    }
  }
  return response;
}

//FUNCTION TO RENDER A SEARCH RESULT
function searchUsers(criteria){

  $.post('/account/search_user/', { item: criteria }, function(data) {
    var data = JSON.parse(data);
    var friends = data.users;

    appendFriends(friends,'.profile-products');
  });

}

function searchDialog(data) {
        var data = JSON.parse(data);
        var products = data.products;
        var users = data.users;
        var groups = data.groups;
        
        var content = '<div class="notifications-content">';
        // Valid products
        if (products.length > 0) {
                content = content + '<span class="left text-12">Productos</span><hr class="right"><div class="clear"></div>';

                $.each(products, function(key, val) {
                        $.each(val, function(key, value) {
                                content = content + '<a href="/product/profile/' + value.product_id + '" class="black">' +
                                                                                                                        '<div class="notification-content clear">' +
                                                                                                        '<div class="notification-image left" style="background-image: url(' + value.product_img + ')">' +
                                                                                                          '<div class="clear"></div>' +
                                                                                                        '</div>' +
                                                                                                        '<div class="left notification-description">' +
                                                                                                          '<div class="text-14">' + value.product_name + '</div>' +
                                                                                                          '<i class="right text-12 white">ver mas</i>' +
                                                                                                        '</div>' +
                                                                                                        '<div class="clear"></div>' +
                                                                                                      '</div>' +
                                                                                        '</a>';
                        });
                });
        }

        if (products.length < 1 && users.length < 1 && groups.length < 1) {
                content = content + '<div class="text-center text-12 black">Sin resultados...</div>'
        }

        content = content + '</div>';

        if ($('#general-search-content').is(':visible')) {
                $('#general-search-content').html(content);
        } else {
                $('#general-search-content').html(content).slideDown('normal');
        }
        if ($('.notifications-content').height() > 400) {
          $('.notification-description').addClass('notification-description-responsibe');
        } else {
          $('.notification-description').removeClass('notification-description-responsibe');
        }
}

function notificationsPreview(data) {
  data = JSON.parse(data);
  data = data.notifications;

  var content = '';

  $.each(data, function(index, value) {
    if (index < 4) {
      if (value[0].enviorment == "offer") {
        content = content +     '<li class="text-left tab-content">' +
                                  '<a id="noti-link" data-link="'+value[0].env_id+'" class="black">' +
                                    '<img src="' + value[0].user_pic + '" class="left" height="50px" width="50px" /><div class="left bold"> ' + value[0].user_name + ' ' + value[0].action_trigered + '</div>' +
                                  '</a>' +
                                '</li>';
      } else if (value[0].enviorment == "chat") {
        content = content +     '<li class="text-left tab-content">' +
                                  '<a href="/chat/#' + value[0].user_id + '" class="black">' +
                                    '<img src="' + value[0].user_pic + '" class="left" height="50px" width="50px" /><div class="left bold">' + value[0].user_name + ' ' + value[0].action_trigered + '</div>' +
                                  '</a>' +
                                '</li>';
      }
    }
  });

  if (data.length > 4) {
    $('#view-more-notifications').show();
  } else {
    if (data.length == 0) {
      var content = '<li class="text-center black text-center">No tienes notificaciones.</li>';
    }
  }

  $('#menu-notifications-content').slideDown('400', function() {
          $('.notificatios-list').html(content);
  });

  return false;
}

function getGroups(data) {
        var groups = JSON.parse(data).groups;
        var content = '';
        if (groups.length) {
                $.each(groups, function(key, value) {
                        content = content + '<div class="product-info column left bg-white">' +
                                                                                  '<div class="white bg-green title-content">' + value[0].group_name + '</div>' +
                                                                                  '<div class="bg-white text-center black">Este grupo no cuenta con articulos aun.</div>' +
                                                                            '<div class="over-product-info hide">' +
                                                                                                '<div class="title-content bg-blue"><a class="go-group white block" href="/group/profile/' + value[0].group_id + '">' + value[0].group_name + '</a></div>' +
                                                                                                '<div class="black tab-content">' +
                                                                                                        '<span class="bold">Miembros</span><br>' +
                                                                                                        '<span>' + value[0].group_members + '</span>' +
                                                                                                '</div>' +
                                                                                                '<div class="black tab-content">' +
                                                                                                        '<span class="bold">Productos</span><br>' +
                                                                                                        '<span>' + value[0].group_products + '</span>' +
                                                                                                '</div>' +
                                                                                                '<div class="title-content bg-blue"><a class="leave-group white block" href="' + value[0].group_id + '">Abandonar grupo</a></div>' +
                                                                                                '<div class="title-content bg-blue"><a class="resign-group white block" href="' + value[0].group_id + '">Renunciar moderador</a></div>' +
                                                                                                '<div class="title-content bg-blue"><a class="close-group white block" href="' + value[0].group_id + '">Cerrar grupo</a></div>' +
                                                                                        '</div>' +
                                                                                        '<div class="clear"></div>' +
                                                                                      '</div>';
                });
        } else {
                content = '<div class="text-center bold">No se encontraron resultados.</div>';
        }

        
        $('.profile-products').html(content);


        return false;
}

// friends
function getFriends(id) {
  $.post('/account/list_user_friends/', { profile_id: id }, function(data) {
    printFriends('.profile-products', data);
  });
  
  return false;
}

function searchFriends(id, item) {
  $.post('/account/search_friend/', { user_id: id, item: item }, function(data) {
    printFriends('.profile-products', data);
  });

  return false;
}

function printFriends(container, data) {
        var data = JSON.parse(data);
        var friends = data.friends;

        appendFriends(friends,container);

        return false;
}

// getNews
function getNews(user_id, user_name) {
        $.post('/notifications/get/', { channel_id:user_id }, function(data) {
          var feeds = JSON.parse(data);
          var content = '<div id="messages-container">';
          var tempDate = '';
          if (feeds.notifications.length > 0) {

            var enviroment = '';
            $.each(feeds.notifications, function(key, value){
              var date = value[0].date;
              var title = matchDateToday(date);
              if(date.substring(0,16)!=tempDate.substring(0,16)){
                content += '<div class="container-600 center">'+
                            '<div class="bg-white top-title-content black">'+title+'</div>'+
                            '<div>';
                tempDate = date;
                }

              var message = value[0].action_trigered;
              var userPic = value[0].user_pic;
              var username = value[0].user_name;
              var targetname = value[0].target_name;
              var targetId = value[0].target_id;
              var enviorment = value[0].enviorment;
              var moderator = 'Tienes una invitacion para ser moderador del grupo';
              var joinGroup = "Tienes una invitacion al grupo Autos clasicos";
              var invite_type = message.match(joinGroup) ? '1' : message.match(moderator) ? '2' : '0';
              var link_to = '';
              var friend_id = value[0].user_id;
              var response_request = '';
              var notification_id = value[0].notification_id;

              if (enviorment == 'chat') {
                enviroment = '<div class="blue icon-menu bg-gray block left"><img src="/static/img/mensajes.svg" /></div>';
                link_to = '<a href="/chat/' + value[0].target_id+'" class="black">';
              } else if (enviorment == 'offer') {
                enviroment = '<div class="blue icon-menu bg-gray block left">d</div>';
                link_to = '<a href="/offer/current/' + value[0].target_id+'" class="black">';
                
              }else if (enviorment == 'user profile'){
                link_to = '<a class="black request">';
                response_request = '<button class="btn btn-yellow right acept-request" data-id="'+friend_id+'" data-notify="'+notification_id+'">Aceptar</button><button class="btn btn-red right cancel-request" data-id="'+friend_id+'" data-notify="'+notification_id+'">Cancelar</button>';
              }

              content +=  link_to +
                            '<div class="notifications-content top-title bg-white">' +
                              '<div class="buzon-message left">'+ enviroment +
                                '<div class="left text-12">' +
                                  // '<span><a href="/account/profile/' + user_id + '" class="blue"><i>'+username+'</i></a> ' + message + ' <i class="blue">' + targetname + '</i></span>' +
                                  '<span><i class="blue">'+username+'</i> ' + message + ' <i class="blue">' + targetname + '</i>'+response_request+'</span>'+
                                '</div>'+
                                '<div class="clear"></div>' +
                              '</div>' +
                              '<div class="clear"></div>' +
                            '</div>' +
                          '</a>';
            });

            content += '</div></div></div>';

            $('.profile-products').html(content);
            $('#messages-container').html(content);
            $('#product-content').html(content);
          } else {
            $('.profile-products').html('<div class="text-center bold">No se encontraron Noticias.</div>');
          }
        }).error(function(error) {
                
        });
}

// getWishes
function getWishes(id) {
  $.post('/product/matchwishes/', { user_id: id }, function(data) {
    getItems('.profile-products', data, 0);
  });

  return false;
}

//Search with page result
function searchResults(item){
    $.post('/product/latest/', { product: item }, function(data) {
        console.log(data);
        getItems('.profile-products', data, 0);
    });
}

// recientes
function getProducts() {
  $.post('/product/latest/', function(data) {
    getItems('.profile-products', data, 0,0);
  });

  return false;
}

//Recientes por categoria
function getProductsByCat(cat){
  $.post('/product/get_by_cat/',
    {category:cat},
    function(data){
      console.log(data);
      getItems('.profile-products',data,0)
    });
}

// GetOffers
function GetOffers(user_id) {
        $.post('/stream/getfeed/', { user_name: user_name, user_id: user_id }, function(data) {
                var feeds = JSON.parse(data);

                

                getItems(container, data, owner)
        }).error(function(error) {
                
                $('.profile-products').html('<div class="text-center bold">No se encontraron Noticias.</div>');

        });
}

// Products
function findItem(user, owner) {
        $.ajax({
                url:'/product/search/',
                type:'POST',
                data:{
                        item: $('#search-value').val(),
                        user_id: user
                }
        }).done(function(data) {
                getItems('.profile-products', data, owner);
        });

        return false;
}

function sendMessage(url, message, user_name, product, id_user, product_name, img_url) {
  $.post(url, { title_chat: product, message: message, second_user: id_user }, function(data) {
    $('#message-chat').val('');
    $.fancybox.close();

    $.post('/offer/make/', { name: user_name, product: product, id_user: id_user, product_name: product_name, img_url: img_url, chat_id: data }, function(data) {
      if(data != 'False') {
        showFancyBoxMessage('Oferta','Tu oferta ha sido realizada, haz clic en ofertas para darle seguimiento.',true)
        window.location.href = '/offer/current/'+data;
      }
    });
  });

  return false;
}

function allProducts(id, type, optEdit) {

        $.post('/product/products_all/', { user: id, offer_id:type}, function(data) {
          getItems('.profile-products', data, type, optEdit);
        });

        return false;
}

function allOffers(){
  $.post('/offer/ajax_active_offers/',{foo:'bar'}, function(data) {
          getOffer('.profile-products', data, 0, 0);
        });
}

function productsInPropose(id){
  $.post('/offer/bagbidder/',{offer_id:id}, function(data){
    getItems('.profile-products', data, 1, 3);
  });

  return false;
}

function filterCategories(type, user_id, category, owner) {
        $.ajax({
                url:'/product/products/',
                type:'POST',
                data:{
                        type: type, user_id: user_id, category: category
                }
        }).done(function(data) {
                getItems('.profile-products', data, owner);
        });

        return false;
}

// Get friend products
function friendProducts(id, owner, optEdit) {
  $.post('/product/friendProduct/', function(data) {
    getItems('.profile-products', data, owner, optEdit);
  });

  return false;
}

// Get friends products of the friend
function friendsProducts(id, owner, optEdit) {
  $.post('/product/friendProduct/', { user_id: id, offer_id:owner }, function(data) {
    getItems('.profile-products', data, owner, optEdit);
  });

  return false;
}

function getItems(container, data, owner, opt_temp) {
        var checkData = data;
        var data = JSON.parse(data);
        console.log(data);
        var optEdit = opt_temp;

        if(checkData =='{"products": [[{}]]}' || checkData == '{"products": []}'){
          $(container).html('<div class="text-center bold">No se encontraron resultados.</div>');
        }else if (checkData!='{"products": [[{}]]}' || checkData!= '{"products": []}') {
          var i = 1;
          $.each(data.products, function(key, value) {
            var product_name = value[0].product_name;
            var product_name_alt = value[0].product_name;
            var is_owner = value[0].session_is_owner;
            var user_has_offered = value[0].user_has_offered;
            var owner_username = value[0].user_name_product;
            var owner_picture = value[0].user_pic_product;
            var owner_id = value[0].user_id_product;
            var owner_rating = value[0].user_rating_product;
            var user_is_following = value[0].user_is_following;
            var edit_opt = value[0].edit_opt;

            if(edit_opt!='0'&& optEdit!='3'){
              optEdit = edit_opt;
            }
            if(optEdit==undefined)optEdit= 0;
            console.log("opcion edit: "+optEdit)
            console.log("opcion editBKDN: "+edit_opt)
            if(user_has_offered){
              active_offer = 'inactive-icon';
              offer_id = '';
            }else{
              active_offer = '';
              offer_id = 'offer';
            }
            if(user_is_following){
              active_follow = 'inactive-icon';
              follow_id = '';
            }else{
              active_follow = '';
              follow_id = 'follow';
            }

            if (product_name.length > 18) {
              product_name = product_name.substring(0,18) + '...';
            }

            var edit = '';
            //Eliminar editar producto
            if (optEdit == '1') {
              edit =  '<button onclick="location.href=' + '/product/edit/' + value[0].product_id + '" class="btn-over-offer btn btn-yellow">Editar producto</button>' +
                      '<button class="delete-product btn-over-product btn btn-red close" data-id="' + value[0].product_id + '">Elimina producto</button>';
            } else {
              //Agregar y quitar de la oferta
              if (optEdit == '2') {
                edit = '<button data-product="' + value[0].product_id + '" class="btn-over-offer btn btn-blue right add-prod-offer">Agregar a Propuesta</button>';
              } else if (optEdit == '3') {
                edit = '<button data-id="' + value[0].product_id + '" class="btn-over-offer btn btn-yellow right delete-offer-product">Quitar de Propuesta</button>';
              } else if(optEdit == 0 && is_owner == false){
                edit =  '<button id="'+follow_id+'" data-id="' + value[0].product_id + '" class="'+active_follow+
                       ' btn-over-offer btn btn-blue">Seguir</button>' +
                      '<button id="'+offer_id+'" class="btn-over-product btn btn-yellow '+active_offer+
                      '" data-id="' + value[0].product_id + '" data-username="'+owner_username+
                      '" data-pic="'+owner_picture+'" data-usrid="'+owner_id+
                      '" data-productname="'+product_name+'" data-userrating="'+owner_rating+
                      '" data-imgproduct="'+value[0].product_img+'">Lo quiero</button>';
              }
            }

            var content = '<div class="product product-tab column  left " data="' + value[0].product_id + '">' +
                            '<div class="text-center" style="border-width:0px;">' +
                              '<div class="title-content">' + product_name + '</div>' +
                              '<a style="text-decoration:none;" href="/product/profile/' + value[0].product_id + '" title="' + product_name_alt + '" class="black">' +
                                '<img style="text-decoration:none;" src="' + value[0].product_img + '" width="100%" style="border-width:-1px;">' +
                              '</a>' +
                            '</div>' +
                            '<div class="product-ctrls hide">' + edit +'</div>' +
                          '</div>';
            
            if (key == 0) {
                    $(container).html('<div class="left item-tab tab-1 parche">' + content + '</div><div class="left item-tab tab-2 parche"></div><div class="left item-tab tab-3 parche"></div><div class="left item-tab tab-4 parche"></div><div class="clear">');
            } else {
                    $('.tab-' + i).append(content);
            }
            if (i > 3) {
                i = 1;
            } else {
                i = i + 1;
            }
            optEdit = opt_temp;
          });
        }else {
                $(container).html('<div class="text-center bold">No se encontraron ofertas.</div>');
        }

        return false;
}

//Function to built offer mini profile
function getOffer(container, data, owner, optEdit) {
        var checkData = data;
        var data = JSON.parse(data);
        console.log(data);
  
        if(checkData =='{"products": [[{}]]}' || checkData == '{"products": []}'){
          $(container).html('<div class="text-center bold">No se encontraron resultados.</div>');
        }else if (checkData!='{"products": [[{}]]}' || checkData!= '{"products": []}') {
          var i = 1;
          $.each(data.products, function(key, value) {
            var product_name = value[0].product;
            var product_name_alt = value[0].product;
            if (product_name.length > 18) {
              product_name = product_name.substring(0,18) + '...';
            }

            var edit='';

            var content = '<div class="product product-tab column left ui-widget-content" data="' + value[0].product_id + '">' +
                            '<a href="' + value[0].url + '" title="' + product_name_alt + '" class="black">' +
                              '<div class="text-center">' +
                                '<div class="title-content">' + product_name + '</div>' +
                                '<img src="' + value[0].photo + '" width="100%">' +
                              '</div>' +
                            '</a>' +
                            '<div class="product-ctrls hide">' + edit +'</div>' +
                          '</div>';
            console.log("Contenido: "+content);
            if (key == 0) {
                    $(container).html('<div class="left item-tab tab-1 parche">' + content + '</div><div class="left item-tab tab-2 parche"></div><div class="left item-tab tab-3 parche"></div><div class="left item-tab tab-4 parche"></div><div class="clear">');
            } else {
                    $('.tab-' + i).append(content);
            }
            if (i > 3) {
                i = 1;
            } else {
                i = i + 1;
            }
          });
        }

        return false;
}

//built user mini profile
function appendFriends(friends,container){
        if (friends.length > 0){
          var i = 1;
                $.each(friends, function(key, value) {
                        
                var content =  '<div class="product product-tab column left ui-widget-content" >' +
                                    '<div class="text-center title-content">' + value[0].friend_name + '</div>' +
                                    '<a href="/account/profile/' + value[0].friend_id + '">' +
                                      '<img src="' + value[0].friend_pic + '" style="width: 100%;">' +
                                    '</a>' +
                                '</div>';
                        if (key == 0) {
                                $(container).html('<div class="left item-tab tab-1 parche">' + content + '</div><div class="left item-tab tab-2 parche"></div><div class="left item-tab tab-3 parche"></div><div class="left item-tab tab-4 parche"></div><div class="clear">');
                        } else {
                                $('.tab-' + i).append(content);
                        }
                        if (i > 3) {
                            i = 1;
                        } else {
                            i = i + 1;
                        }
                });
        } else {
                $(container).html('<div class="text-center bold">Sin resultados.</div>');
        }
}

function deleteProduct(id_product, id_user) {
  var response = false;
  $.post('/product/delete/', { product_id: id_product, user_id: id_user }, function(data) {
    if(data=="True")
      window.location = "../../../account/profile/";
    else
      showFancyBoxMessage('Error','No pudimos eliminar el producto intentalo más tarde.',true);
  });
  
  return response;
}

function deleteOfferProduct(id_product, id_offer) {
  $.post('/offer/delete_prod/', { product_id: id_product, offer_id: id_offer }, function(data) {
    productsInPropose(id_offer);

    return true;
  }).error(function(data) {
    

    return false;
  });

  return false;
}

function addFriend(user) {
  $.ajax({
    url:'/account/friend_request/',
    type:'POST',
    data:{ user_request_id: user }
  }).done(function(data){
      showFancyBoxMessage('Solicitud de Amistad',data,true);
  });

        return false;
}

// Chat
function getChats(id) {
  $.post('/chat/retrieve_chat/', { user: id }, function(data) {
    var chats = JSON.parse(data).chats;
    var content = '';
    var date = '';
    if(chats.length > 0) {
      $.each(chats, function(key, value) {
        date = value[0].date.replace('-',' ');
        date = date.replace('-',' ');
        date = date.substring(0,16);
        content = content + '<div id=="'+value[0].id+'">' +
                                                '<div class="left text-12">'+date+'</div>' +
                                                '<hr>' +
                                                '<div class="clear"></div>' +
                                                '<div class="buzon-message-content">' +
                                                  '<div class="left" style="background-image:url('+value[0].second_user_picture+');background-size:cover; background-repeat: no-repeat; width:50px; height:50px;">' +      
                                                  '</div>' +
                                                  '<div class="buzon-message left">' +
                                                    '<div class="left text-12"><i class="bold">' + value[0].second_username + '</i>: ' + value[0].txt.substr(0, 20) + '...</div>' +
                                                    '<div class="right">' +
                                                      '<a href="../offer/current/' + value[0].env_id + '/" data-user-send="' + value[0].second_user + '" class="open-chat text-12 black"><i>ir a conversación</i></a>' +
                                                      '<div id="delete" class="bg-red white icon right" data="'+value[0].id+'" style="cursor:pointer;">o</div>' +
                                                    '</div>' +
                                                    '<div class="clear"></div>' +
                                                  '</div>' +
                                                  '<div class="clear"></div>' +
                                                '</div>' +
                                              '</div>';
      });
    } else {
      content = '<div class="text-center">No hay mensajes</div>';
    }

    $('#buzon-messages').html(content);
  });

  return false;
}

function onlyNumbersDano(evt) {
  var keyppressed = (evt.which) ? evt.which : evt.keyCode

  return !(keyppressed > 31 && (keyppressed < 48 || keyppressed > 57));
}

function validForm(element, form) {
        var element_id = '#' + element.id;
        var element_type = $(element).data('type');
        var resp = false;

        switch (element_type) {
                case ('text'):
                        // Valid the caracters
                        if ($(element_id).val().length > 0) {
                                resp = true;
                                $(element_id).addClass('success-form').removeClass('error-form');
                        } else {
                                resp = false;
                                $(element_id).addClass('error-form').removeClass('success-form');
                        }
                break;
                case ('email'):
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        
                        if ($(element_id).val().length > 0 && emailReg.test($(element_id).val())) {
                                resp = true;
                                $(element_id).addClass('success-form').removeClass('error-form');
                        } else {
                                resp = false;
                                $(element_id).addClass('error-form').removeClass('success-form').attr('placeholder', 'Debes ingresar un correo valido (ej: user@kamvia.mx)').val('');
                        }
                break;
                case ('password'):
                        if ($(element_id).val().length >= 8) {
                                resp = true;
                                $(element_id).addClass('success-form').removeClass('error-form');
                        } else {
                                resp = false;
                                $(element_id).addClass('error-form').removeClass('success-form').attr('placeholder', 'La contraseña debe ser de 8 caracteres (ej: A&defe.01)').val('');
                        }
                break;
                case ('repeat_password'):
                        if ($(element_id).val().length >= 8 && $(element_id).val() == $(element_id).prev('input[type="password"]').val()) {
                                resp = true;
                                $(element_id).addClass('success-form').removeClass('error-form');
                        } else {
                                resp = false;
                                $(element_id).addClass('error-form').removeClass('success-form').attr('placeholder', 'Las contraseñas no coinciden').val('');
                        }
                break;
                default:
                        
        }
        return resp;
}

function getNotifications() {
  $.get('/notifications/count_notification/', function(data) {
    if (data > 0) {
      $('#get-notifications img').attr('src', '/static/img/notificaciones.svg');
      $('#not-number').replaceWith('<div id="not-number" class="notifications-number text-center white">' + data + '</div>');
    }
  });

  return false;
}

function hideNotifications() {
  if ($('#general-search-content').is(':visible') && $('#menu-notifications-content').is(':visible')) {
    $('#general-search-content').hide();
    $('#general-search').val('');
    $('#menu-notifications-content').hide();
  } else if ($('#general-search-content').is(':visible')) {
    $('#general-search-content').hide();
    $('#general-search').val('');
  } else if ($('#menu-notifications-content').is(':visible')) {
    $('#menu-notifications-content').hide();
  }

  return false;
}

//IMPLEMENTS A STANDAR FANCYBOX MESSAGE
function showFancyBoxMessage(title,data,timer){
  var header = '<div id="send-message-container" class="bg-white container-400"><div class="text-center">';
  var body = '</div><div class="send-message-info"></div><div class="send-message-content text-center"><h4>';
  var footer = '</h4><div  class="clear"></div></div></div>';
  var message = header+title+body+data+footer;
  
  $.fancybox.open(message, { 'closeBtn' : false });
  //check if timer is require
  if(timer)setTimeout(function() { location = location; }, 3000);
}

//CHECK IF THE USER DATA IS FULLY COMPLETE
function showPopUp(userId){
  gritterOptions = { eventName: 'notification',
                      title: 'Kamvia',
                      image: 'https://s3-us-west-2.amazonaws.com/kamviaassets/128x128.jpg',
                      text: 'Para poder comenzar a intercambiar debes subir una '+
                      '<b>foto de algún artículo</b>. No olvides completar tu perfil'+
                      ' subiendo alguna foto tuya.',
                    }
  
  $.post('/account/checkdata/', function(data) {
    console.log(data)
    if(data=="False")
      $.gritter.add(gritterOptions);
  });
}