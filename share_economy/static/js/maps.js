/**
 * @author Joselo
 */
function MapsCtrl($scope){

	if(navigator.geolocation){
		navigator.geolocation.getCurrentPosition(getCoords, getError);
	}else{
		
	}
	
	function getCoords(position){
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		
		initialize(lat,lng);
	}
	
	function getError(err){
		initialize(37.0625,-95.677068);
	}
	
	function initialize (lat,lng) {
	  var latlng =  new google.maps.LatLng(lat,lng);
	  var mapSettings = {
	  		center: latlng,
	  		zoom: 15,
	  		mapTypeId: google.maps.MapTypeId.ROADMAP
	  }
	  
	  map = new google.maps.Map(document.getElementById('maps'), mapSettings);
	}
}
