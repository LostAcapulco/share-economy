from mongoengine import StringField, DateTimeField, Document

import datetime, time

class Feed(Document):
    """
    A feed document for the hole system
    """
    user_id = StringField()
    user_name = StringField()
    user_pic = StringField()
    action_trigered = StringField()
    target_id = StringField()
    target_name = StringField()
    date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def add_feed(cls,**kwargs):

        feed = cls()
        feed.user_id = kwargs['user_id']
        feed.user_name = kwargs['user_name']
        feed.user_pic =StringField['user_pic']
        feed.action_trigered = kwargs['action_trigered']
        feed.target_id = kwargs['target_id']
        feed.target_name = kwargs['target_name']

        feed.save()

        return feed

    @classmethod
    def delete_feed(cls,feed_id):

        try:
            feed = cls.object(id=feed_id)
            feed.drop_collection()
            
            return True
        except Exception as e:
            
            return e
    