from array import array

from user.documents import *
from email import utils

import uuid, datetime, time

class Activity:
    
    @classmethod
    def __init__(cls,activity_type, action_text, user_id, email=None):
        user = User.objects(id=user_id).first()
        username = user.username
        user_pic = user.picture
        
        cls.id = uuid.uuid1().__str__().decode('utf-8')
        cls.type = activity_type
        cls.action_text = action_text
        cls.user_pic = user_pic
        cls.username = username
        dt = datetime.datetime.now()
        dt = dt.timetuple()
        dt = time.mktime(dt)
        cls.date = utils.formatdate(dt)

    @classmethod
    def get_message(cls):
        activity ={}
        activity = {'id':cls.id,
                    'body':cls.action_text,
                    'published':cls.date,
                    'actor':{'displayName':cls.username,
                             'objectType':'person',
                             'image':{'url':cls.user_pic,
                                      'width':48,
                                      'heigth':48}
                             }
                    }

        return activity


        