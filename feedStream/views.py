#!/usr/bin/python
# -*- encoding: utf-8 -*-

from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from activity import Activity
from documents import Feed
from email import utils

import pusher , json, datetime, time

pusher.app_id = settings.PUSHER_APP_ID
pusher.key = settings.PUSHER_APP_KEY
pusher.secret = settings.PUSHER_APP_SECRET

p = pusher.Pusher()

@csrf_exempt
@login_required(login_url='/account/login/')
def add_feed(request):
    
    if request.POST:
        try:
            feed = Feed()
            response = feed.add_feed(user_id=request.POST.get('user_id'),
                                     user_name=request.POST.get('user_name'),
                                     user_pic=request.POST.get('user_pic'),
                                     action_trigered=request.POST.get('action_trigered'),
                                     target_id=request.POST.get('target_id'),
                                     target_name=request.POST.get('target_name'))
            send_stream(request,'comento',request.POST.get('text'))
            response = True

        except Exception as e:
            
            response = e
            
    return HttpResponse(response)

@csrf_exempt
@login_required(login_url='/account/login/')
def get_feeds(request):
    if request.POST:
        user_id = request.POST.get('user_id')
        user_name = request.POST.get('user_name')

        feeds = Feed.objects(user_id=user_id).all()

        if feeds is not None:
            array_feeds = {}
            chunck_feeds = []
            for feed in feeds:
                user_feed = {}
                user_feed.update({'user_id':feed.user_id})
                user_feed.update({'user_name':feed.user_name })
                user_feed.update({'action_trigered':feed.action_trigered})
                user_feed.update({'target_id':feed.target_id})
                user_feed.update({'target_name':feed.target_name})
                dt = feed.date.timetuple()
                dt = time.mktime(dt)
                dt= utils.formatdate(dt)
                user_feed.update({'date':dt})
                chunck_feeds.append([user_feed])
            array_feeds.update({'products':chunck_feeds})

    return HttpResponse(json.dumps(array_feeds))

def test_feed_stream(request, template='test_stream.html'):
    user = request.session['user']
    user_id = user.id.__str__().decode('utf-8')
    request.session['user_id'] = user_id
    return render_to_response(template, 
                              {'PUSHER_KEY': settings.PUSHER_APP_KEY,}, 
                              RequestContext(request))
@csrf_exempt
#@login_required(login_url='/login/')
def send_stream(request):
    if request.POST.get('user_id'):
        user_id = request.POST.get('user_id')
    activity_type = "activity"
    action_text = "Algo paso en la plataforma"

    activity = Activity(activity_type, action_text, user_id).get_message()

    p['feedStream'].trigger(activity_type,activity)

    return HttpResponse('')