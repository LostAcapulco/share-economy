from django.conf.urls.defaults import patterns, url
from views import send_stream, test_feed_stream, get_feeds


urlpatterns = patterns('',
    url(r'^test_feed/$',
        test_feed_stream,
        name='test_feed_stream'
        ),
    url(r'^send/$',
        send_stream,
        name='test_stream'),                  #URL FOR A RENDER FORM THAT SEND A NOTIFICATION
    url(r'^getfeed/$',
        get_feeds,
        name='get_feeds'),                  #URL FOR A RENDER FORM THAT SEND A NOTIFICATION

)