#!/usr/bin/python
# vim: set fileencoding= utf-8 :
"""
Forms and validation code for user registration.

"""


from django import forms
from django.contrib.auth.forms import PasswordResetForm as AuthPRF
from django.contrib.auth.hashers import UNUSABLE_PASSWORD
from django.utils.translation import ugettext_lazy as _
from mongoengine.django.auth import User
from models import *
from documents import RegistrationProfile, Address

import re


# I put this on all required fields, because it's easier to pick up
# on them with CSS or JavaScript if they have a class of "required"
# in the HTML. Your mileage may vary. If/when Django ticket #3515
# lands in trunk, this will no longer be necessary.
attrs_dict = {'class': 'required'}


class RegistrationForm(forms.Form):
    """
    Form for registering a new user account.

    """
    name = forms.CharField( max_length=30,
                            widget=forms.TextInput(attrs={'ng-model':'user.first_name','placeholder':'Escribe tu nombre', 'class':'left required bg-gray', 'data-type':'text'}),
                            label=_(u'El nombre que usaremos para correos electronicos'),
                            error_messages={'required': 'Campo requerdo'}
                            )
    username = forms.CharField(max_length=30,
                                widget=forms.TextInput(attrs={'ng-model':'user.username','placeholder':'Nombre de usuario', 'class':'left required bg-gray', 'data-type':'text'}),
                                label=_(u'Nombre de usuario'),
                                required=False
                                )
    user = forms.CharField(widget=forms.HiddenInput(attrs={'value':''}),required=False)
    email = forms.EmailField(widget=forms.TextInput(attrs=dict({'ng-model':'user.email','placeholder':'tu correo', 'class':'left required bg-gray', 'data-type':'email'},
                                                               maxlength=75)),
                             label=_(u'Correo'),
                             required=False
                             )
    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'ng-model':'user.password1','placeholder':'una contraseña', 'class':'left required bg-gray', 'data-type':'password'}, render_value=False),
        label=_(u'Contraseña'),
        required=False
        )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'ng-model':'user.password2','placeholder':'confirma tu contraseña', 'class':'left required bg-gray', 'data-type':'repeat_password'}, render_value=False),
        label=_(u'Contraseña (repetir)'),
        required=False
        )
    stp1_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'form_id','value':'{[form_id="form_id_11"]}'}),required=False)
    nxt_stp_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'kamvia_next_id', 'value':'{[kamvia_next_id]}'}),required=False)

    tos = forms.BooleanField()

    def clean(self):
        user_id = self.cleaned_data.get('user')
        if user_id != '':
            user = RegistrationProfile.objects(id=user_id).first()
            if user.username != self.cleaned_data['username']:
                if RegistrationProfile.objects(
                    username__iexact=self.cleaned_data['username']):
                    print "Este nombre de usuario ya ha sido tomado por un usuario."
                    raise forms.ValidationError(
                        _(u'Este nombre de usuario ya ha sido tomado por un usuario.'))
        else:
            if RegistrationProfile.objects(
                username__iexact=self.cleaned_data['username']):
                print "Este nombre de usuario ya ha sido tomado por un usuario."
                raise forms.ValidationError(
                    _(u'Este nombre de usuario ya ha sido tomado por un usuario.'))

        """
        Verifiy that the values entered into the two password fields
        match. Note that an error here will end up in
        ``non_field_errors()`` because it doesn't apply to a single
        field.

        """

        if user_id == '':
            print "tooooos"
            tos = self.cleaned_data.get('tos')
            name = self.cleaned_data.get('name')
            username = self.cleaned_data.get('username')
            email = self.cleaned_data.get('email')
            password1 = self.cleaned_data.get('password1')
            password2 = self.cleaned_data.get('password2')
            if tos is not True:
                print "Debes aceptar los terminos y condiciones."
                raise forms.ValidationError(_(u'Debes aceptar los terminos y condiciones.'))
            if password1 != password2:
                print "Las contraseñas no son iguales."
                raise forms.ValidationError(
                    _(u'Las contraseñas no son iguales.'))

            if len(password1)<8:
                print "La contraseña debe contener almenos 8 caracteres."
                raise forms.ValidationError(
                    _(u'La contraseña debe contener almenos 8 caracteres.'))

            if password1==None or password2 ==None or email==None or username==None or name==None :
                print "Todos los campos son necesarios"
                raise forms.ValidationError(
                    _(u'Todos los campos son necesarios'))
        """
        Verifiy that the email entered is not duplicated
        for register user and for a new user
        user_id = '' new user
        """

        user_id = self.cleaned_data.get('user')
        if user_id == '':
            if RegistrationProfile.objects(
                email__iexact=self.cleaned_data['email']):
                print "Ya existe una cuenta con ese email."
                raise forms.ValidationError(
                    _(u'Ya existe una cuenta con ese email.'))
        else:
            user = RegistrationProfile.objects(id=user_id).first()
            user_email = user.email
            if self.cleaned_data.get('email')!=user_email:
                if RegistrationProfile.objects(
                    email__iexact=self.cleaned_data['email']):
                    print "Ya existe una cuenta con ese email."
                    raise forms.ValidationError(
                        _(u'Ya existe una cuenta con ese email.'))

        return self.cleaned_data

    def save(self ,user_edit,current_user):
        """
        Create the new ``User`` and ``RegistrationProfile``, and
        returns the ``User``.

        """
        new_user = RegistrationProfile.create_inactive_user(username=self.cleaned_data['username'],
                                                            password=self.cleaned_data['password1'],
                                                            email=self.cleaned_data['email'],
                                                            user_edit=user_edit,
                                                            name=self.cleaned_data['name'],
                                                            current_user=current_user)
        
                                                            
        return new_user

class RegistrationFormStp2(forms.Form):

    street_num = forms.CharField(max_length=90, required=False, widget=forms.TextInput(attrs={'placeholder':'Calle y numero', 'class':'bg-gray required', 'data-type':'text'}), label=(u'Calle y numero'), initial='Dirección')
    colony = forms.CharField(max_length=90, required=False, widget=forms.TextInput(attrs={'placeholder':'Colonia', 'class':'bg-gray required', 'data-type':'text'}), label=(u'Colonia'))
    deleg_mun = forms.CharField(max_length=90, required=False, widget=forms.TextInput(attrs={'placeholder':'Delegacion/munucipio', 'class':'bg-gray required', 'data-type':'text'}), label=(u'Delegacion/munucipio'))
    zc = forms.CharField(max_length=5, required=False, widget=forms.TextInput(attrs={'placeholder':'Código postal', 'class':'bg-gray required', 'data-type':'text', 'onkeypress':'return onlyNumbersDano(event)', 'maxlength':'5'}), label=(u'Codigo postal'))
    city = forms.CharField(max_length=90, required=False, widget=forms.TextInput(attrs={'placeholder':'Ciudad', 'class':'bg-gray required', 'data-type':'text'}), label=(u'Ciudad'))
    state = forms.CharField(max_length=90, required=False, widget=forms.TextInput(attrs={'placeholder':'Estado', 'class':'bg-gray required', 'data-type':'text'}), label=(u'Estado'))
    country = forms.CharField(max_length=90, required=False, widget=forms.TextInput(attrs={'placeholder':'País', 'class':'bg-gray required', 'data-type':'text'}), label=(u'País'))
    
    about_me = forms.CharField(max_length=70, widget=forms.Textarea(attrs={'placeholder':'Escribe algo sobre ti máximo 70 caracteres.', 'class':'bg-gray required', 'data-type':'text'}),required=False)
    
    stp1_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'form_id','value':'{[form_id="form_id_12"]}'}),required=False)
    nxt_stp_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'kamvia_next_id', 'value':'{[kamvia_next_id]}'}),required=False)

    def save(self,profile_id,user_edit):

        address = Address(street_num=self.cleaned_data['street_num'],
                                 colony=self.cleaned_data['colony'],
                                 deleg_mun=self.cleaned_data['deleg_mun'],
                                 zc=self.cleaned_data['zc'],
                                 city=self.cleaned_data['city'],
                                 state=self.cleaned_data['state'],
                                 country=self.cleaned_data['country'],
                                 about_me=self.cleaned_data['about_me'])

        profile_nivel_2 = RegistrationProfile.register_profile_step_2(profile_id,address,user_edit);

        return profile_nivel_2

class FormInviteFriends(forms.Form):
    
    friend_email= forms.CharField(max_length=90, widget=forms.TextInput(attrs={'placeholder':'Escribe los correos de tus amigos separados por una coma ","', 'class':'bg-gray'}), required=True, label=(u'Emails'))
    message = forms.CharField(max_length=90, widget=forms.Textarea(attrs={'placeholder':'Cuentales algo de kamvia', 'class':'bg-gray'}), required=True, label=(u'Cuerpo del Mensaje'))
    stp1_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'form_id','value':'{[form_id="form_id_33"]}'}),required=False)
    nxt_stp_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'kamvia_next_id', 'value':'{[kamvia_next_id]}'}),required=False)

      
class RegistrationFormTermsOfService(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds a required checkbox
    for agreeing to a site's Terms of Service.

    """
    tos = forms.BooleanField(
        widget=forms.CheckboxInput(attrs=attrs_dict),
        label=_(u'I have read and agree to the Terms of Service'),
        error_messages={
            'required': _(u"You must agree to the terms to register")})


class RegistrationFormUniqueEmail(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which enforces uniqueness of
    email addresses.

    """
    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.

        """
        if RegistrationProfile.objects(
            email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(
                _(u'El e-mail que propocionaste ya esta en uso. '))
        return self.cleaned_data['email']

class loginForm(forms.Form):
    """ 
    TO DO:
        validation of fields
    """
    user_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Usuario', 'class':'bg-gray'}))
    user_password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Contraseña', 'class': 'bg-gray'},
                                                               render_value=False),
                                    label=_(u'Contraseña'))

class wishList(forms.Form):
    wish_name = forms.CharField(max_length=90, required=True, widget=forms.TextInput(attrs={'placeholder':'Inserta tu Wish', 'class':'bg-gray'}), label=(u'Wish Name'))
    
class PasswordResetForm(forms.Form):
    """
    Form for change recuperate password

    """

    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'ng-model':'user.password1','placeholder':'una contraseña', 'class':'left required bg-gray', 'data-type':'password'}, render_value=False),
        label=_(u'Contraseña'),
        required=False
        )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'ng-model':'user.password2','placeholder':'confirma tu contraseña', 'class':'left required bg-gray', 'data-type':'repeat_password'}, render_value=False),
        label=_(u'Contraseña (repetir)'),
        required=False
        )

    key_salt = forms.CharField(widget=forms.HiddenInput(attrs={'value':''}),required=False)

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        
        if password1 != password2:
            raise forms.ValidationError(_(u'Las contraseñas no son iguales.'))

        return self.cleaned_data

    def save(self,salt):
        print "clean data password:", self.cleaned_data['password1']
        profile = RegistrationProfile.new_password(password=self.cleaned_data['password1'],
                                                    salt=salt);

        return profile