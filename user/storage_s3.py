import boto.s3
from boto.s3.key import Key
import sys
import logging
from django.conf import settings
import os
#import Image
from PIL import Image

boto_logger = logging.getLogger('boto')

def upload_s3(name_file=""):
    try:
        bucket_name = 'profilekamvia'
        conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,
                settings.AWS_SECRET_ACCESS_KEY)
        bucket = conn.get_bucket(bucket_name)
        file_profile="data_profile/"+str(name_file)
        k = Key(bucket)
        k.key = name_file
        k.set_contents_from_filename(file_profile)
        k.set_acl('public-read')
        os.remove(file_profile)
    except Exception as e:
        return  str(e)

def handle_uploaded_file(f,name,type='product'):
    if type=='user':
        new_width = 200
    elif type=='product':
        new_width= 600
    try:
        f = Image.open(f)
    except Exception as e:
        print e

    if f.size[0]>new_width:
        new_heigth = (new_width*f.size[1])/f.size[0]
    else:
        new_width = f.size[0]
        new_heigth = f.size[1]
    try:
        img = f.resize((new_width,new_heigth), Image.ANTIALIAS)
        img.save('data_profile/'+str(name),"PNG")
        upload_s3(str(name))
    except Exception as e:
        print e

    
def percent_cb(complete, total):
    sys.stdout.write('.')
    sys.stdout.flush()
