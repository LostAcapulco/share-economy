import datetime
import random
import re
import sha
import json, urllib
from random import random

from django.conf import settings
from django.utils.hashcompat import md5_constructor, sha_constructor
from django.utils.encoding import smart_str
from mongoengine import *
from mongoengine import EmbeddedDocumentField, EmbeddedDocument, GeoPointField, DateTimeField
from mongoengine.django.auth import User
from errorReport.views import save_error
from signals import activate

from math import ceil

SHA1_RE = re.compile('^[a-f0-9]{40}$')

def get_hexdigest(algorithm, salt, raw_password):
    raw_password, salt = smart_str(raw_password), smart_str(salt)
    if algorithm == 'md5':
        return md5_constructor(salt + raw_password).hexdigest()
    elif algorithm == 'sha1':
        return sha_constructor(salt + raw_password).hexdigest()
    raise ValueError('Got unknown password algorithm type in password')

class RegistrationProfile(User,Document):
    """
    A profile which stores an activation key for use during
    user account registration.

   """
    first_name = StringField(max_length=40)
    ACTIVATED = u"ALREADY_ACTIVATED"
    facebook_id = StringField()
    access_token = StringField()
    activation_key = StringField(max_length=40)
    address = ListField(EmbeddedDocumentField("Address"))
    picture = StringField(max_length=400)
    friends = ListField(EmbeddedDocumentField("Friend"))
    wishes = ListField(EmbeddedDocumentField("Wishlist"))
    products = ListField(EmbeddedDocumentField("Product"))
    date = DateTimeField(default=datetime.datetime.now)
    allow = StringField(max_length=300)
    swaps = IntField()
    rating = IntField()
    password_reset_key = StringField()
    data_complete = StringField()

    def activation_key_expired(self):

        expiration_date = datetime.timedelta(
            days=settings.ACCOUNT_ACTIVATION_DAYS)
        return self.activation_key == self.ACTIVATED or \
               (self.date_joined + expiration_date <= datetime.datetime.now())
    activation_key_expired.boolean = True

    @classmethod
    def activate_user(cls, activation_key):

        if SHA1_RE.search(activation_key):
            profile = cls.objects(class_check=False,
                activation_key=activation_key).first()
            if profile and not profile.activation_key_expired():
                profile.is_active = True
                profile.activation_key = cls.ACTIVATED
                profile.save()
                activate.send(cls, document=profile)
                return profile
        return False

    @classmethod
    def create_inactive_user(cls, username, password, email ,user_edit ,name , current_user):#, send_email=True):
        if user_edit:
            registration_profile = cls.update_user(name, current_user, username, email, password)

        else:
            registration_profile = cls.create_user(username=username, password=password, email=email)
            id = registration_profile.id
            cls.add_name_user(id, name)

        return registration_profile

    @classmethod
    def create_user(cls, *args, **kwargs):
        """
        Create a ``RegistrationProfile`` for a given
        ``User``, and return the ``RegistrationProfile``.

        """
       
        profile = super(cls, cls).create_user(*args, **kwargs)
        salt = sha.new(str(random())).hexdigest()[:5]
        profile.activation_key = sha.new(salt + profile.username).hexdigest()
        profile.is_active = False
        profile.swaps = 0
        profile.rating = 0 
        profile.save()
        

        return profile

    @classmethod
    def add_name_user(cls, user_id, name):
        
        user = cls.objects(id=user_id).first()
        user.first_name = name
        user.picture = "https://s3-us-west-2.amazonaws.com/kamviaassets/default-bg-product.jpg"
        user.save()

    @classmethod
    def update_user(cls,name, current_user, username, email, password=None):

        update_user = cls.objects(id=current_user).first()
        update_user.update(set__username=username)
        update_user.update(set__email=email)
        update_user.update(set__first_name=name)

        pass_len = len(password)
        if pass_len > 0:
            salt = get_hexdigest('sha1', str(random()), str(random()))[:5]
            hash = get_hexdigest('sha1', salt, password)
            password = '%s$%s$%s' % ('sha1', salt, hash)
            update_user.update(set__password=password)

        return update_user

    @classmethod
    def register_profile_step_2(cls, profile_id, new_address , edit_user):
        """
        Create a ``Profile nivel 2`` form a given ``user_id``

        """
        try:
            profile_level_2 = cls.objects(id=profile_id).update_one(set__address__0=new_address)
        except:
            profile_level_2 = cls.objects(id=profile_id).first()
            profile_level_2.address.append(address)
            profile_level_2.data_complete = '1'
            profile_level_2.save()
            
        return profile_level_2

    @classmethod
    def add_friends(cls,user_id,friend_id):
        try:
            
            check_frindship = cls.objects(Q(id=user_id)&Q(friends__uid=friend_id)).first()

            if check_frindship == None:
                user = cls.objects(id=friend_id).first()
                friend=Friend()
                friend.uid=friend_id
                friend.friend_name = user.username
                friend.friend_pic = user.picture
                profile = cls.objects(id=user_id).first()
                profile.friends.append(friend)
                profile.save()

            response = True
        except Exception as e:
            error = "Error adding document friend: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'user',
                        method = 'add_friends - document',
                        error = error,
                        user_name = '----',
                        user_id = user_id)
            response = False

        return response

    @classmethod
    def are_friends(cls,user_id,friend_id,response=None):
        """Check if a user -friend_id- is on the friend list of other user -user_id-"""
        try:
            response = cls.objects(Q(id=user_id)&Q(friends__uid=friend_id)).first()
        except Exception, e:
            print "Error getting friend from document: ", e
        return response

    @classmethod
    def get_friends(cls,user_id=None):
        """Return the user friends, use user_id to make the search"""
        if user_id is not None:
            user = cls.objects(id=user_id).first()
            friends = user.friends
        return friends

    @classmethod
    def register_profile_pic(cls, profile_id, image_url):
        """
        Upload a profile picture for a given ``user_id``

        """
        
        profile = cls.objects(id=profile_id).first()
        profile.picture = image_url
        profile.save()

        if profile.picture is not None:
            return True
        else:
            return False

    @classmethod
    def update_one(cls, user_id=None, element=None, obj=None):
        
        if user_id is not None:
            user = cls.objects(id=user_id).update_one(element=obj)
            return user
    
    @classmethod
    def add_rating(cls, user_id, rating):
        user = cls.objects(id=user_id).first()
        count =  int(user.rating)
        swaps = int(user.swaps)
        print "rating given ", rating
        print "previos rating", count
        print "current swaps", swaps
        rate = int(ceil(((count+0.0)+(int(rating)+0.0))/(swaps+0.0)))
        print "new raging ", rate
        user.rating = rate
        print "updated rating ", user.rating    
        user.save()
        print "raging saved ", user.rating

    @classmethod
    def add_user_product(cls,**kwargs):
        try:
            user = cls.objects(id=kwargs['user_id']).first()
            
            product = Product()
            
            product.produc_id = kwargs['produc_id']
            product.product_name = kwargs['product_name']
            product.product_about = kwargs['product_about']
            product.product_picture = kwargs['product_picture']

            user.products.append(product)

            return user
        except Exception as e:
            print e

    @classmethod
    def search_product_user(cls, product_name, user_id, product=None):
        list_products = []
        try:
            users = cls.objects(Q(products__product_name__icontains=product_name)&Q(id=user_id)).all()
            if users is not None:
                for user in users:
                    list_products.append(user.products)
        except Exception as e:
            print e
        return list_products

    @classmethod
    def reset_password(cls,user_mail=None,response=None):
        try:
            profile = cls.objects(email=user_mail).first()
            salt = sha.new(str(random())).hexdigest()[:5]
            profile.password_reset_key = sha.new(salt + profile.username).hexdigest()
            profile.save()
            response = profile
        except Exception as e:
            print "Error creating salt password document: ", e

        return response

    @classmethod
    def new_password(cls,password,salt,response=None):
        try:
            print salt
            profile = cls.objects(password_reset_key=salt).first()
            pass_len = len(password)
            print "Usuario para cambio de password: ", profile
            if profile is not None and pass_len > 0:
                salt = get_hexdigest('sha1', str(random()), str(random()))[:5]
                hash = get_hexdigest('sha1', salt, password)
                password = '%s$%s$%s' % ('sha1', salt, hash)
                print "Password anterior: ", profile.password
                print "Password cifrado: ", password
                profile.update(set__password=password)
                profile.update(set__password_reset_key='')
                response = profile
        except Exception as e:
            error = "Error saving password: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'user',
                        method = 'new_password - document',
                        error = error,
                        user_name = '----',
                        user_id = '-----')

        return response

    @classmethod
    def get_user(cls, user_id, response=None):
        """Get a user by user_id"""
        if user_id is not None:
            try:
                response = cls.objects(id=user_id).first()
            except Exception as e:
                error = "Error getting user info: "+e.__str__().decode('utf-8')
                print error
                save_error(module = 'user',
                        method = 'get_user - document',
                        error = error,
                        user_name = '----',
                        user_id = '-----')
            return response

    @classmethod
    def search_user(cls, criteria, response=None):
        """Make a search for a user in Mongo"""
        if criteria is not None:
            try:
                response = cls.objects(Q(username__icontains=criteria)|Q(first_name__icontains=criteria)).all()

            except Exception as e:
                error = "Error getting user info: "+e.__str__().decode('utf-8')
                print error
                save_error(module = 'user',
                        method = 'search_user - document',
                        error = error,
                        user_name = '----',
                        user_id = '-----')
        return response

    @classmethod
    def add_swap(cls, user_id, response=None):
        try:
            user = cls.objects(id=user_id).first()
            user_swaps = user.swaps
            user_swaps = user_swaps + 1
            user.swaps = user_swaps

            user.save()

            response = True
        except Exception as e:
                error = "Error adding swap to a user: "+e.__str__().decode('utf-8')
                print error
                save_error(module = 'user',
                        method = 'add_swap - document',
                        error = error,
                        user_name = '----',
                        user_id = '-----')
                response = False
        return response

    @classmethod
    def system_user_list(cls, response=None):
        """ Return all the registrated users on the plataforma"""
        try:
                response = cls.objects().all()
        except Exception as e:
            error = "Error getting system users: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'user',
                    method = 'system_user_list - document',
                    error = error,
                    user_name = '----',
                    user_id = '-----')
        return response

    @classmethod
    def check_data_fully_complete(cls, user_id, response=False):
        """ Check if the data for a given user is fully complete"""
        try:
            obj_user = cls.objects(id=user_id).first()
            if obj_user.address[0] is not None:
                if obj_user.address[0].street_num is not None:
                        if obj_user.address[0].colony is not None:
                            if obj_user.address[0].zc is not None:
                                if obj_user.address[0].state is not None:
                                    if obj_user.address[0].country is not None:
                                        if obj_user.address[0].about_me is not None:
                                            response = True
        except Exception as e:
            error = "Error checking user data: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'user',
                    method = 'check_data_fully_complete - document',
                    error = error,
                    user_name = '----',
                    user_id = '-----')

        return response

class Address(EmbeddedDocument):

        street_num=StringField(max_length=90)
        colony=StringField(max_length=60)
        deleg_mun=StringField(max_length=90)
        zc=StringField(max_length=5)
        city=StringField(max_length=60)
        state=StringField(max_length=60)
        country=StringField(max_length=60)
        about_me=StringField(max_length=400)

class Product(EmbeddedDocument):
    produc_id = StringField()
    product_name = StringField()
    about_product = StringField()
    product_picture = StringField()

class Location(EmbeddedDocument):

    location = GeoPointField()

class Friend(EmbeddedDocument):

    uid=StringField(max_length=300)
    friend_name = StringField(max_length=90)
    friend_pic = StringField(max_length=400)
    date = DateTimeField(default=datetime.datetime.now)
    
class User_Wishlist(EmbeddedDocument):
    user_id = StringField()


class Wishlist(Document):
    owner_id = StringField(max_length=90)
    wishes = ListField(EmbeddedDocumentField("Wish"))

    @classmethod
    def add_wish(cls, *args, **kwargs):
        try:
            wishlist = cls.objects(owner_id=args[0]).first()
            wish = Wish()
            wish.wish_name = kwargs['wish_name']
            if wishlist is None:
                wishlist = cls()
                wishlist.owner_id = args[0]
                wishlist.wishes.append(wish)
                wishlist.save()
            else:
                wishlist.wishes.append(wish)
                wishlist.save()
            return wishlist
        except Exception, e:
            return e

    @classmethod
    def delete_wishlist(cls,user_id):
        try:
            wishlist = cls.objects(owner_id=user_id).first()
            if wishlist is not None:
                wishlist.delete()
            response = True
        except Exception as e:
            response = False
        
        return response

    @classmethod
    def get_wishlist(cls,user_id):
        try:
            print "user",user_id;
            wishlist = cls.objects(owner_id=user_id).first()

            return wishlist.wishes
        except Exception, e:
            return e
        

class Wish(EmbeddedDocument):
    wish_name = StringField(max_length=60)
    date = DateTimeField(default=datetime.datetime.now)
    produc_id = StringField()   

class Chat (Document):
    first_user=StringField()
    second_user=StringField()

class Tokens(Document):
    
    token = StringField()
    used = BooleanField()