from django.conf.urls.defaults import patterns, url
from django.views.generic import RedirectView
from django.contrib.auth import views as auth_views
from views import *
from forms import PasswordResetForm

urlpatterns = patterns('',
    url(r'^register/(?P<user_id>\w+)/$',
        register,
        name='registration_register'),
    url(r'^register/$',
        register,
        name='registration_register'),                    #URL TO STEP 1 OF USER REGISTER
    url(r'^register_trust/(?P<edit>\w+)/$',
        register_step_2,
        name='registration_register_step_2'), 
    url(r'^register_trust/$',
        register_step_2,
        name='registration_register_step_2'),            #URL TO STEP 2 OF USER REGISTER
    url(r'^register_comun/(?P<user_id>\w+)/$',
        registration_register_step_3,
        name='registration_register_step_3'),
    url(r'^register_comun/',
        registration_register_step_3,
        name='registration_register_step_3'),           #URL TO STEP 3 OF USER REGISTER
    url(r'^register/complete/$',
        RedirectView,
        {'template': 'registration_complete.html'},     #URL TO REGISTRATION COMPLETE
        name='registration_complete'),
    url(r'^activate/(?P<activation_key>\w+)/$',
        activate,
        name='registration_activate'),                  #URL TO ACTIVATE AN ACCOUNT
    url(r'^login/',
        login_view,
        name='kamvia_login'),                           #URL TO LOGIN VIA Kamvia
    url(r'^profile/$',
        user_profile,
        name='admin_profile'),                           #URL TO USER PROFILE 
    url(r'^profile/(?P<user_id>\w+)/$',
        user_profile,
        name='user_profile'),                           #URL TO USER PROFILE 
    url(r'^$',index,name='index'),
    url(r'^picture/', regist_prof_pic, name='picture_upload'), #Upload picture to s3
    url(r'^home/', home, name='home_inicio'),
    url(r'^logout/',user_logout,name='logout'),
    url(r'^friend_request/',send_friend_request,name='send_friend_request'),
    url(r'^resp_friend_request/', response_friend_request,name='response_friend_request'),
    url(r'^wishlist/$',wish_form,name='wish_form'),
    url(r'^add_wish/',add_wish,name='add_wish'),
    url(r'^get_wishlist/$',get_wishlist,name="get_wishlist"),
    url(r'^get_wishlist/(?P<user_id>\w+)/$',get_wishlist,name="get_wishlist_user"), 
    url(r'^user_friends/(?P<user>\w+)/$',user_friends,name='user_friends'),
    url(r'^list_user_friends/$',user_friend_json,name='user_friend_json'),
    url(r'^search_friend/$', search_friend, name='search_friend'),
    url(r'^csv/',reader_csv,name='token'),
    url(r'^token_activate/(?P<token>\w+)/$',token_activate,name='token_activate'),
    url(r'^wishes/$',wish_follow_all,name='home_whises'),
    url(r'^edit/$',register,name='edit_register'),
    url(r'^mail/$',webmail,name='edit_register'),
    url(r'^error/$',error_report,name='edit_register'),
    url(r'^retrive_password/$',email_reset_password,name='reset_password'),
    url(r'^recupera_contra/(?P<salt>\w+)/$',new_password,name='new_password'),
    url(r'^change_pass',new_password,name='new_password_sn_salt'),
    url(r'^search_result/$',search,name='search'),           #URL TO RENDER TE RESULTS PAGE 
    url(r'^search_user/$',find_users,name='find_users'),
    url(r'^checkdata/$',check_popup,name='check_popup'), 
)
