#!/usr/bin/python
# -*- encoding: utf-8 -*-
"""
Views which allow users to create and activate accounts.

"""

from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from documents import *
from forms import *
from offer.models import *
from storage_s3 import handle_uploaded_file
from django.core.mail import send_mail
from product.views import products_user
from product.documents import Product
from product.forms import cat_producto_array
from group.documents import Group
from django.template.loader import render_to_string
import sha
import csv

def webmail(request):
    return render_to_response('welcome_mail.html')

def index(request,template='index.html',to_redirect="/account/profile/"):
    """
    Authenticate and login a given -User-
    set the expire session via request.session.set_expiry
    """
    try:
        user = request.session['user']
        user.is_authenticated()
        return redirect(to_redirect)
    except:
        try:
            user = request.session['user']
            user.is_authenticated()
            return redirect("/account/profile/")
        except:

            form = loginForm()
            if request.method == "POST":
                form = loginForm(request.POST)
                if form.is_valid():
                    cd = form.cleaned_data
                    try:
                        user = authenticate(username=request.POST.get('user_name'),password=request.POST.get('user_password'))
                        if user is not None:
                            if user.is_authenticated:
                                request.session['user'] = user
                                request.name = user.username
                                login(request, user)
                                return redirect('/account/home/')
                        else:
                            context = RequestContext(request, {"error":"Nombre y/o contraseña incorrectos"})
                            return render_to_response(template,{"form":form}, context)
                    except DoesNotExist:
                        return HttpResponse('UPS!! Algo salió mal...')
                else:
                    form = loginForm
                    context = RequestContext(request, {"error":"Nombre y/o contraseña incorrectos"})
                    return render_to_response(template,{"form":form}, context)
            else:
                context = RequestContext(request)
        return render_to_response(template,{"form":form}, context)

def tos(request,template='tos_kamvia.html'):
    context = RequestContext(request,{})
    return render_to_response(template,context)

@login_required(login_url='/account/login/')
def home(request):
    if request.method == 'POST':
        user_session = request.session['user']
        user_id = user_session.id
        facebook_profile = RegistrationProfile.get_facebook_profile(user_id)

    return render_to_response('home.html',
                              context_instance=RequestContext(request, { 'list_users': user_friends, 
                                                                         'categories': cat_producto_array,
                                                                         'PUSHER_KEY':settings.PUSHER_APP_KEY, }))

@login_required(login_url='/account/login')
def search(request,item=None,template='home.html'):
    """
    Render a result search page
    """
    if request.POST:
        item = request.POST['item']
        context = RequestContext(request,{'item':item})
        return render_to_response(template,context)
    return redirect('/account/home/')

def activate(request, activation_key,
             template_name='activate.html',
             extra_context=None):
    activation_key = activation_key.lower() # Normalize before trying anything with it.
    account = RegistrationProfile.activate_user(activation_key)
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    return render_to_response(template_name,
                              account,
                              context_instance=context)


def register(request,tmpl_nm='registration_basic.html',form_class=RegistrationForm,user=None,user_id=None):
    """
    Allow a new user to register an account and loggedin a given user
    """
    if not request.user.is_authenticated():
        user_edit=False
        extra_context = {'edit':False}
        print "USUARIO NO LOGEADO"
    else:
        print "USUARIO LOGEADO"
        user_edit=True
        user_session = request.session['user']
        user_id = user_session.id.__str__().decode('utf-8')
        user_username = user_session.username
        user_name = user_session.first_name
        user_mail = user_session.email
        extra_context = {'user_id':user_id,'user_username':user_username,'user_name':user_name,'user_mail':user_mail,'edit':True}

    form = form_class(data=request.POST, files=request.FILES)
    print form.is_valid()
    if request.method == 'POST' and form.is_valid():
        print "valid"
        User = form.save(user_edit,user_id)
        if user_edit == False:
            user_auth = authenticate(username=User.username,password=form.cleaned_data['password1'])
            if user_auth is not None:
                request.session['user'] = user_auth
                if user_auth.is_authenticated:
                    request.name = user_auth.username
                    login(request, user_auth)
                    login(request, user_auth)
                    send_mail_confirm(User)
                    #to_redirect = manage_next(form.cleaned_data['nxt_stp_id'],user_edit)
                    return redirect('/account/home/')
                else:
                    return HttpResponse('404-noauth')
            else:
                return HttpResponse('404-noneUs')#TO DO: SI EXISTE UN ERROR REDIRIGIR A LA PAGINA DE REGISTRO CON MENSAJE DE ERROR Y BORRAR EL USER DE LA DB
        else:

            user = RegistrationProfile.objects(id=user_id).first()
            request.session['user'] = user
            to_redirect = manage_next(form.cleaned_data['nxt_stp_id'],user_edit)
            return redirect(to_redirect)
    else:
        to_response_extra = response_none_valid(request,tmpl_nm,extra_context)
        return render_to_response(to_response_extra['template_name'],
                                  {'form':form},
                                  context_instance=to_response_extra['context_instance'])

@login_required(login_url='/account/login/')
def register_step_2(request,stp2_class=RegistrationFormStp2,tmpl_nm='registration_trust.html',extra_context=None,edit=None):
    if not request.user.is_authenticated():
        edit=False
        extra_context = {}
    else:
        edit=True
        user_session = request.session['user']
        user_id = user_session.id.__str__().decode('utf-8')
        try:
            user_session_address = user_session.address[0]
            street_num = user_session_address.street_num
            colony = user_session_address.colony
            deleg_mun = user_session_address.deleg_mun
            zc = user_session_address.zc
            city = user_session_address.city
            state = user_session_address.state
            country = user_session_address.country
            about_me = user_session_address.about_me

            extra_context = {'user_id':user_id,
                             'street_num':street_num,
                             'colony':colony,
                             'deleg_mun':deleg_mun,
                             'zc':zc,
                             'city':city,
                             'state':state,
                             'country':country,
                             'about_me':about_me}
        except Exception as e:
            print e
            extra_context = {'user_id':user_id,
                             'street_num':'',
                             'colony':'',
                             'deleg_mun':'',
                             'zc':'',
                             'city':'',
                             'state':'',
                             'country':'',
                             'about_me':''}

    reg_trust = stp2_class(data=request.POST, files=request.FILES)

    if request.method == 'POST' and reg_trust.is_valid():
        user_session = request.session['user']
        user_id = user_session.id
        reg_trust.save(user_id,edit)
        to_redirect = manage_next(reg_trust.cleaned_data['nxt_stp_id'],edit)

        return redirect(to_redirect)
    else:
        to_response_extra = response_none_valid(request,tmpl_nm,extra_context)
        return render_to_response(to_response_extra['template_name'],
                                  {'form':reg_trust},
                                  context_instance=to_response_extra['context_instance'])

@csrf_exempt
@login_required(login_url='/account/login/')
def regist_prof_pic(request):
    user_session = request.session['user']
    user_id = user_session.id
    handle_uploaded_file(request.FILES['thefile'],user_id,'user')
    picture_url=settings.S3_URL+settings.BUCKET_PROFILE+"/"+str(user_id)
    profile_picture=RegistrationProfile.register_profile_pic(user_id,picture_url)

    return HttpResponse('valid')

@csrf_exempt
@login_required(login_url='/account/login/')
def  registration_register_step_3(request,stp3_class=FormInviteFriends,tmpl_nm='invite.html',extra_context=None):

    form_friends = stp3_class(data=request.POST)
    email_list=[]
    if request.POST:
        if form_friends.is_valid():
            friends = request.POST['friend_email']
            message = request.POST['message']
            friend_list= friends.split(',')
            for item in friend_list:
                email_list.append(item)
            send_email_kamvia(message,email_list)

    to_response_extra = response_none_valid(request,tmpl_nm)

    return render_to_response(to_response_extra['template_name'],
                                  {'form':form_friends},
                                  context_instance=to_response_extra['context_instance'])


def manage_next(next_stp,user_edit):
    if user_edit:
        next_to_redirect = {'kamvia_id_21':'/account/profile/',
                            'kamvia_id_22':'/account/register_trust',
                            'kamvia_id_23':'/account/register_comun',
                            'kamvia_id_24':'/account/home/'}
    else:
        next_to_redirect = {'kamvia_id_21':'/account/home/',
                            'kamvia_id_22':'/account/register_trust/',
                            'kamvia_id_23':'/account/register_comun',
                            'kamvia_id_24':'/account/home/'}
    return next_to_redirect[next_stp]


def response_none_valid(request, tmpl_nm, extra_context=None):
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    to_response_extra = {'template_name':tmpl_nm,
                         'context_instance':context}
    return to_response_extra


def send_mail_confirm(user, send_email=True):
    if send_email:
        from django.core.mail import EmailMultiAlternatives
        subject, from_email, to = 'Activacion de tu cuenta en Kamvia', settings.DEFAULT_FROM_EMAIL, user.email

        html_content = render_to_response('activa_cuenta.html',{'activation_key': user.activation_key,'user':user.username})
        text_content = 'Bienvenido a Kamvia'
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

def send_email_kamvia(message,to_email):
    subject="Invitacion a Kamvia"
    from_email=settings.DEFAULT_FROM_EMAIL

    send_mail(subject,message,from_email,to_email,fail_silently=False)

@csrf_exempt
def error_report(request, mail_error="jl.mrtz@gmail.com", send_email=True):
    if request.POST:
        try:
            if send_email:
                from django.core.mail import EmailMultiAlternatives
                subject, from_email, to = 'REPORTE DE ERRORES', settings.DEFAULT_FROM_EMAIL, mail_error

                message = request.POST.get('message')
                text_content = 'REPORTE DE ERRORES'
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(message, "text/html")
                msg.send()
                print "enviado"
                resp = "ok"
        except Exception as e:
            print "ERROR: ", e
            resp = "error"

    return HttpResponse(resp)

@login_required(login_url='/account/login/')
def user_profile(request,user_id=None, template="profile.html"):
    mutual_friends=False
    session = request.session['user']
    session_id = session.id.__str__().decode('utf-8')
    num_common_friends=""

    follow_second = False
    follow_first = False
    owner = False
    if user_id is None:
        request.session['profile_id'] = session_id
        try:
            user = User.objects(id = session_id).first()
            owner = True
            user_id = session_id 
        except:
            template="not_found.html"
            context = RequestContext(request)
            return render_to_response(template, context)

    else:

        try:
            num_common_friends = commun_friends(request,user_id)
            request.session['profile_id'] = user_id
            user = User.objects(id = user_id).first()
        except:
            template="not_found.html"
            context = RequestContext(request)
            return render_to_response(template, context)

        if session_id != user_id:
            owner = False
            user_first = User.objects(id = user_id).first()
            friends = user_first.friends

            for item in friends:
                if user_id == item.uid and follow_first==False:
                    follow_first = True
            user_second = User.objects(id = user_id).first() 
            friends_second = user_second.friends

            for item_second in friends_second:
                if str(session_id) == str(item_second.uid) and follow_second==False:
                    follow_second=True
            if follow_first == True and follow_second == True:
                mutual_friends = True
                          
        else:
            owner = True 

    email = user.email
    picture_profile = user.picture
    name = user.first_name
    username = user.username
    try:
        about = user.address[0]
        about = about.about_me
    except:
        about = ''
    
    rating = user.rating
    id_user=user.id

    try:
        address = user.address[0]
        address = address.deleg_mun
        if address == '':
            address = "Sin dirección"
    except:
        address = "Sin dirección"
    
    user_offers = Offer.objects(Q(owner_id=user_id)&Q(status="1")).all()
    total_offers = user_offers.count()

    products = Product.objects(user_id=user_id).all()
    products_tot = products.count()

    groups = Group.objects(user_id=user_id).all()
    group_tot = groups.count()

    tot_user_friend = user_friends(request,user_id)
    tot_wishes = wish_list(request,user_id)

    user_session= request.session['user']
    user_id = user_session.id.__str__().decode('utf-8')
    request.session['user_id'] = user_id
    request.session['category'] = None
    
    friend_status = friend_request_or_allredy_friends(request,user_id)
       

    extra_context = {   'username':username,
                        'email':email,
                        'address':address,
                        'image': picture_profile,
                        'rating': rating,
                        'user_id':request.session['profile_id'],
                        'profile_id':id_user,
                        'about':about,
                        'total_offers':total_offers,
                        'categories': cat_producto_array,
                        'owner_profile':owner,
                        'mutual_friends':mutual_friends,
                        'friend_status':friend_status,
                        'products_tot':products_tot,
                        'tot_user_friend':tot_user_friend,
                        'group_tot':group_tot,
                        'tot_wishes':tot_wishes,
                        'swaps':user.swaps,
                        'rating':user.rating,
                        'common_friends':num_common_friends,
                        'PUSHER_KEY': settings.PUSHER_APP_KEY,
                    }

    context = RequestContext(request, extra_context, [products_user])
    return render_to_response(template,context)
    

def login_view(request,template="login.html"):
    """
    Authenticate and login a given -User-
    set the expire session via request.session.set_expiry
    """
    try:
        user = request.session['user']
        user.is_authenticated()
        return redirect("/account/profile/")
    except:

        form = loginForm()
        if request.method == "POST":
            form = loginForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                try:
                    user = authenticate(username=request.POST.get('user_name'),password=request.POST.get('user_password'))
                    if user is not None:
                        if user.is_authenticated:
                            request.session['user'] = user
                            request.name = user.username
                            login(request, user)
                            return redirect('/account/home/')
                    else:
                        context = RequestContext(request, {"error":"Nombre y/o contraseña incorrectos"})
                        return render_to_response(template,{"form":form}, context)
                except DoesNotExist:
                    return HttpResponse('UPS!! Algo salió mal...')
            else:
                form = loginForm
                context = RequestContext(request, {"error":"Nombre y/o contraseña incorrectos"})
                return render_to_response(template,{"form":form}, context)
        else:
            context = RequestContext(request)
            return render_to_response(template,{"form":form}, context)


def user_logout(request):
    logout(request)
    
    return redirect('/account/login/')

@csrf_exempt
@login_required(login_url='/account/login/')
def send_friend_request(request):
    if request.POST:
        try:
            from notifications.views import general_notification
            user_id = request.POST.get('user_request_id')
            user_session = request.session['user']

            user = User.objects(id=user_id).first()

            message = unicode("Tienes una solicitud de amistad ",'utf-8')

            general_notification(request,
                            channel_id=user_id, #person who will recive de message
                            user_id=user_session.id.__str__().decode('utf-8'), #person who is sending de message
                            user_name=user_session.username, #name of the person who is sending the message
                            user_pic=user_session.picture, #picture of the person who is sending the message
                            action_trigered="envio una solicitud amistad",
                            target_id=user_id,
                            target_name=user.username,
                            enviorment='user profile',
                            is_read='0',
                            message=message,)

            response = "Solicitud enviada"
        except Exception as e:
            print "ERROR: ", e
            response = "Hubo un error al enviar la invitación"

    return HttpResponse(response)

@csrf_exempt
def response_friend_request(request, response=None):
    if request.POST:
        try:
            from notifications.documents import Notification
            user_two = request.POST.get('friend_id')
            notification_id = request.POST.get('notification_id')
            response_request = request.POST.get('response')
            user_session = request.session['user']
            user_one = user_session.id.__str__().decode('utf-8')

            if response_request == '1':
                response_one = add_friend(user_one, user_two)
                response_two = add_friend(user_two, user_one)

                if response_one == True and response_two == True:
                    response = Notification.delete_notification(notification_id)
                else:
                    response = False

            elif response_request == '0':
                response = Notification.delete_notification(notification_id)
                print response
                
        except Exception as e:
            print "Error executing adding friend: ", e
            response = False

    return HttpResponse(response)

def add_friend(user_one=None, user_two=None, response=None):
    
    if user_one is not None and user_two is not None:
        try:
            response=RegistrationProfile.add_friends(user_one,user_two)
        except Exception as e:
            print "Error adding friend: ", e
            response = False
    return response

@login_required(login_url='/account/login/')
def wish_follow_all(request, template='wishlist_follow.html'):
    user_session = request.session['user']
    user_session_id = user_session.id.__str__().decode('utf-8')
    chunk_wishes = []
    chunk_products = []

    wishlist = Wishlist.objects(owner_id=user_session_id).first()
    if wishlist is not None:
        wishes = wishlist.wishes

        for obj in wishes:
            wish = {}
            wish.update({'date':obj.date})
            wish.update({'wish_name':obj.wish_name})
            if obj.produc_id:
              wish.update({'prod_link':'/product/profile/'+obj.produc_id.__str__().decode('utf-8')})  
            chunk_wishes.append([wish])

    products = Product.objects(follows__user_id=user_session_id).all()
    if products is not None:
        for obj in products:
            product = {}
            product.update({'date':obj.date})
            product.update({'product_name':obj.product_name})
            product.update({'product_pic':obj.pictures[0]})
            product.update({'prod_link':'/product/profile/'+obj.id.__str__().decode('utf-8')})
            user = User.objects(id=user_session_id).first()
            product.update({'user_pic':user.picture})
            product.update({'username':user.username})
            chunk_products.append([product])



    context = RequestContext(request,{'chunk':chunk_wishes,
                                      'chunk_prod':chunk_products,
                                      'PUSHER_KEY': settings.PUSHER_APP_KEY})
    return render_to_response(template,context)

@login_required(login_url='/account/login/')
def wish_form(request):

    context = RequestContext(request)
    return render_to_response("wishlist.html",context)

@csrf_exempt
def add_wish(request,response=None):
    if request.POST:
        user_session = request.session['user']
        names_wishes = request.POST['wishes']
        wish_list = names_wishes.split()
        user_id = user_session.id.__str__().decode('utf-8')
        try:
            Wishlist.delete_wishlist(user_id)
            for wish in wish_list:
                Wishlist.add_wish(user_id,wish_name=wish)
            reponse = True
        except Exception as e:
            response = e

    return HttpResponse(response)

@csrf_exempt
def get_wishlist(request,user_id=None):
    if request.POST:
        try:
            user_id = request.POST.get('user_id')
            wishlist = Wishlist.get_wishlist(user_id)
            wishes = []
            for wish in wishlist:
                wishes.append(wish.wish_name)
                wishes.append(" ")
            response = wishes
        except Exception as e:
            print e
            response = False

    return HttpResponse(response)

def wish_list(request,user=None,template='wishlist.html'):
    wishlist =[]
    if user is not None:
        wish = Wishlist.objects(owner_id=user).first()
        if wish is not None:
            total = len(wish.wishes)
        else:
            total = 0
    return total

def user_friends(request,user=None):
    list_friends=[]
    user= User.objects(id=user).first()
    friends = user.friends
    for item in friends:
        list_friends.append(item.uid)

    return len(list_friends)

@csrf_exempt
def user_friend_json(request):
    arrFriends = {}
    if request.POST:
        profile_id = request.POST.get('profile_id')
        user = User.objects(id=profile_id).first()
        
        friends = user.friends
        if friends is not None:
            chunck_friends = []
            for friend in friends:
                f = User.objects(id=friend.uid).first()

                try:
                    address = f.address[0].country
                except:
                    address = "Sin dirección"

                list_user_friend = {}
                list_user_friend.update({'friend_id':friend.uid})
                list_user_friend.update({'friend_name':f.username})
                list_user_friend.update({'friend_pic':f.picture})
                list_user_friend.update({'friend_rating':f.rating})
                list_user_friend.update({'friend_country':address})
                list_user_friend.update({'friend_friends':f.friends.__len__()})

                chunck_friends.append([list_user_friend])

            arrFriends.update({'friends':chunck_friends})
    
    return HttpResponse(json.dumps(arrFriends))

@csrf_exempt
def search_friend(request):
    arrFriends = {}
    if request.POST:
        user_id = request.POST.get('user_id')
        item = request.POST.get('item')
        user = User.objects(id=user_id).first()

        friends = user.friends
        if friends is not None:
            chunck_friends = []
            for friend in friends:
                f = User.objects(Q(id=friend.uid) & Q(username__icontains=item)).first()
            
                if f is not None:
                    list_user_friend = {}
                    list_user_friend.update({'friend_id':friend.uid})
                    list_user_friend.update({'friend_name':f.username})
                    list_user_friend.update({'friend_pic':f.picture})

                chunck_friends.append([list_user_friend])

            arrFriends.update({'friends':chunck_friends})
    
    return HttpResponse(json.dumps(arrFriends))

def get_friends_ids(user_id=None):
    """
    Return a list of friends ids
    """
    chunck_friends_ids = []
    try:
        if user_id is not None:
            friends = RegistrationProfile.get_friends(user_id)
            if friends is not None:
                for friend in friends:
                    chunck_friends_ids.append(friend.uid)
    except Exception as e:
        print e
    return chunck_friends_ids

def commun_friends(request,user_id=None, num_commun_friends=None):
    if user_id is not None:
        user = User.objects(id=user_id).first()
        user_friends = user.friends

        user_session = request.session['user']
        user_session_id = user_session.id.__str__().decode('utf-8')
        user_session = User.objects(id=user_session_id).first()
        user_session_friends = user_session.friends

        commun_friends_list=[]
        for user_friend in user_friends:
            user_friend_id = user_friend.uid
            for user_session_friend in user_session_friends:
                user_session_friend_id = user_session_friend.uid
                if user_friend_id == user_session_friend_id:
                    commun_friends_list.append(user_friend_id)

        num_commun_friends=len(commun_friends_list)
    return num_commun_friends

def already_friends(request,user_id=None,are_friends=False):
    try:
        user_session = request.session['user']
        user_session_id = user_session.id.__str__().decode('utf-8')
        user_is_follower = RegistrationProfile.objects(Q(id=user_session_id) & Q(friends__uid=user_id)).first()
        session_is_follower = RegistrationProfile.objects(Q(id=user_id) & Q(friends__uid=user_session_id)).first()
        if user_is_follower is not None or session_is_follower is not None:
            are_friends = True
    except Exception as e:
        print e
    return are_friends

@csrf_exempt
def find_users(request):
    """Make a search of a user """

    if request.POST:
        user_name = request.POST.get('item')
        users = RegistrationProfile.search_user(user_name);

        if users is not None:
            arr_bus = {}
            arr_users = []

            for u in users:
                list_users = {}
                list_users.update({ 'friend_name': u.username })
                list_users.update({ 'friend_pic': u.picture })
                list_users.update({ 'friend_id': u.id.__str__().decode('utf-8') })

                arr_users.append([list_users])
            
            arr_bus.update({'users':arr_users})

    return HttpResponse(json.dumps(arr_bus))

def reader_csv(request):
    from django.core.mail import EmailMultiAlternatives
    with open('/home/joselo/Downloads/data.csv', 'rb') as f:
        try:
            reader = csv.reader(f)
        except Exception as e:
            print e
        for row in reader:
            email=row[0]
            if email != 'email':

                subject, from_email, to = 'Bienvenido a Kamvia', settings.DEFAULT_FROM_EMAIL, email

                html_content = render_to_response('welcome_mail.html')
                text_content = '¡Kamvia listo!'
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
    
    return HttpResponse("csv")

def token_activate(request,token):
    token_search=Tokens.objects(token=token).first()
    if token_search is None:
        return redirect("http://www.kamvia.mx")
    else:
        return redirect("/account/register/")

    return HttpResponse("activate")

@csrf_exempt
def search_in_wishlist(request):
    arrUsers = {}
    if request.POST:
        criteria = request.POST.get('criteria')
        if criteria!='':
            wishes = Wishlist.objects(wishes__wish_name__icontains=criteria).all()
            if wishes is not None:
                for wish in wishes:
                    user = RegistrationProfile.objects(id=wish.owner_id).first()
                    if user is not None:
                        chunck_friends = []
                        try:
                            address = user.address[0].country
                        except:
                            address = "Sin dirección"

                        list_user = {}
                        list_user.update({'friend_id':user.id.__str__().decode('utf-8')})
                        list_user.update({'friend_name':user.username})
                        list_user.update({'friend_pic':user.picture})
                        list_user.update({'friend_rating':user.rating})
                        list_user.update({'friend_country':address})
                        list_user.update({'friend_friends':user.friends.__len__()})

                        chunck_friends.append([list_user])

                        arrUsers.update({'friends':chunck_friends})
    return HttpResponse(json.dumps(arrUsers))

def friend_request_or_allredy_friends(request,user_id=None,friend_status=None):
    """
    Check if session user and user_id are friend friend_status = 2,
    the user already have a friend request friend_status = 1
    or there is no friend request and are not friend friend_status = 0
    """
    if user_id is not None:
        try:
            from notifications.documents import Notification
            
            friend_request = Notification.get_friend_request_not(user_id,request.session['profile_id'])
            
            if friend_request == None:
                is_friend = RegistrationProfile.are_friends(user_id,request.session['profile_id'])
                is_friend_to = RegistrationProfile.are_friends(request.session['profile_id'],user_id)
                if is_friend is not None and is_friend_to is not None:
                    friend_status = 2
                else:
                    friend_status = 0
            else:
                friend_status = 1
        except Exception, e:
            print "Error getting friend status: ", e

    return friend_status

@csrf_exempt
def email_reset_password(request,response=None):
    """
    Function to send salt email to reset user password
    """
    
    if request.POST:
        email = request.POST.get('email')
        try:
            profile = RegistrationProfile.reset_password(email)
            if profile is not None:
                from django.core.mail import EmailMultiAlternatives
                subject, from_email, to = 'Recuperacion contraseña en Kamvia', settings.DEFAULT_FROM_EMAIL, email

                html_content = render_to_response('mail_reset_pass.html',{'reset_password_key': profile.password_reset_key,'user':profile.username})
                text_content = 'Recuperacion contraseña'
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                response = "Enviamos un correo a tu dirección con instrucciones para recuperara tu contraseña"
        except Exception as e:
            print "Error sending mail to reset password: ", e
            response = "Error al recuperar la contraseña"

    return HttpResponse(response)

def new_password(request,salt=None,template="new_password.html",form_class = PasswordResetForm,to_redirect='/account/home/'):
    try:
        from documents import RegistrationProfile as user_profile

        profile = user_profile.objects(password_reset_key=salt).first()
        if profile is not None:
            form = form_class(data=request.POST, files=request.FILES)
            print "Cambiando password ", form.is_valid()
            if request.method == 'POST' and form.is_valid():
                
                clean_data_form = form.cleaned_data
                print "clean data salt: ", clean_data_form['key_salt']
                profile = form.save(clean_data_form['key_salt'])
                print "Autenticando al usuario: ", profile.username
                user_auth = authenticate(username=profile.username,password=form.cleaned_data['password1'])
                print "Usuario autenticado: ", user_auth.is_authenticated
                
                if user_auth is not None:
                    print "user_auth: ", user_auth
                    request.session['user'] = profile
                    if user_auth.is_authenticated:
                        request.name = profile.username
                        login(request, user_auth)
                        return redirect(to_redirect)
            else:
                print "Cargando forma para cambio de password"
                context = RequestContext(request, {"salt":salt})
                return render_to_response(template,{"form":form}, context)
        else:
            return redirect('/')

    except Exception as e:
        print "Error executing saving new password: ", e

    context = RequestContext(request, {"salt":salt})
    return render_to_response(template,{"form":form}, context)

@csrf_exempt
def check_popup(request,response=False):
    user_session = request.session['user']
    user_session_id = user_session.id.__str__().decode('utf-8')
    response = RegistrationProfile.check_data_fully_complete(user_session_id)

    return HttpResponse(response)