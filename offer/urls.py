from django.conf.urls.defaults import patterns, url

from views import make_offer, current_offer, chek_status_offer, response_response, rating_user
from views import products_in_bidder, response_propose, delete_offer_prod, get_product_offer, send_propouse
from views import active_offers_ajax

urlpatterns = patterns('',
    url(r'^make/$',
        make_offer,
        name='offer_made'),                  #URL TO ALLOW AN USER MAKE AN OFFER
    url(r'^current/(?P<offer_id>\w+)/$',
        current_offer,
        name='current_offer'),              #URL TO VIEW A CURRENT OFFER CALLED VIA GET
    url(r'^current/$',
        current_offer,
        name='current_offer'),              #URL TO ADD PRODUCTS TO AN OFFER
    url(r'^status/$',
        chek_status_offer,
        name='offer_status'),                 #URL TO CHECK THE STATUS OF A GIVEN OFFER
    url(r'^response/$',
        response_response,
        name='response_response'),                 #URL TO RESPONSE A SENT OFFER
    url(r'^add_rating/$',
        rating_user,
        name='rating_user'),
    url(r'^bagbidder/$',
        products_in_bidder,
        name='bag'),                            #URL TO GET ALL PRODUCTS IN THE BAG BIDDER FOR A GIVEN OFFER
    url(r'^finish/$',
        response_propose,
        name="response_propose"),               #URL TO RESPONSE AND FINISH AN OFFER
    url(r'^delete_prod/$',
        delete_offer_prod,
        name='delete_offer_prod'),           #URL TO DELETE A PRODUCT FROM A PROPOSE IN A OFFER
    url(r'^product_offers/$',
        get_product_offer,
        name='get_product_offer'),           #URL TO GET ALL OFFERS FOR A GIVEN ID PRODUCT
    url(r'^envia_prop/$',
        send_propouse,
        name='send_propouse'),           #URL TO SEND A NOTIFICATION FOR A PROPOUSE MADE
    url(r'^ajax_active_offers/$',
        active_offers_ajax,
        name='active_offers_ajax'),           #URL TO SEND A NOTIFICATION FOR A PROPOUSE MADE

)