#!/usr/bin/python
# -*- encoding: utf-8 -*-

from mongoengine import StringField, ListField, DateTimeField, Document
from mongoengine import EmbeddedDocumentField, EmbeddedDocument
from mongoengine.queryset import *

from errorReport.views import save_error

import datetime

class Offer(Document):
    """
    An offer that store info about users and products
    """
    bidder_id = StringField()
    bidder_name = StringField()
    owner_id = StringField()
    owner_name = StringField()
    product_owner_id = StringField()
    product_owner_name = StringField()
    product_owner_img = StringField()
    user_id_last_prop = StringField()
    products_bidder = ListField(EmbeddedDocumentField("ProductBidder"))
    owner_rated = StringField()
    bidder_rated =StringField()
    status = StringField()
    user_type = StringField()
    date = DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return u"Dueño del producto %s" % self.date


    @classmethod
    def create_offer(cls,bidder_id, bidder_name, owner_id, owner_name, product_owner_id, product_name, img, status=None, product_bidder=None, offer_id=None):
        """
        Create a new, -Offer- related to a profile.
        there is three type of status
            wating for A (DEFAULT), B or C
            canceled for A (DEFAULT), B or C
            acepted
        """

        offer = cls()
        offer.bidder_id = bidder_id
        offer.bidder_name = bidder_name
        offer.owner_id = owner_id
        offer.owner_name = owner_name
        offer.product_owner_id = product_owner_id
        offer.product_owner_name = product_name
        offer.product_owner_img = img

        if status is None:
            offer.status = "1"
        else:
            offer.status = status
        offer.save()
        offer_id = offer.id.__str__().decode('utf-8')
        return offer_id

    @classmethod
    def add_product_bidder(cls,*args, **akwargs):

        product_bidder = ProductBidder(**akwargs)
        check = cls.objects(Q(products_bidder__product_bidder_id=akwargs['product_bidder_id']) & Q(id=args[0])).first()
        if check is None:
            offer = cls.objects(id=args[0]).first()
            offer.products_bidder.append(product_bidder)
            offer.save()

            return offer
        else:
            return False

    @classmethod
    def delete_product_bidder_offer(cls,offer_id,product_id):
        try:
            offer = Offer.objects(id=offer_id).update_one(pull__products_bidder__product_bidder_id=product_id)
        except Exception as e:
            print e

        return offer

    @classmethod
    def response(cls,**kwargs):
        try:
            offer = cls.objects(id=kwargs['offer_id']).first()
            offer.status = kwargs['offer_response']
            offer.save()
        except Exception as e:
            response = e
        return

    @classmethod
    def get_all_offers_product(cls, product_id, offers=None):
        """
        Get all active offers for a given product_id
        """
        try:
            offers = cls.objects(Q(product_owner_id=product_id)&Q(status="1")).all()

        except Exception as e:
            error = "Error getting offers for a given product_id: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'Offers',
                    method = 'get_all_offers_product - Document',
                    error = error,
                    user_name = '----',
                    user_id = '----')

        return offers

    @classmethod
    def get_all_user_offers(cls,user_id,response=None):

        if user_id is not None:
            try:
                response = Offer.objects(Q(owner_id=user_id)|Q(bidder_id=user_id)).order_by('-date').all()
            except Exception, e:
                Error = "Error Getting Document offer"+e.__str__().decode('utf-8')
                print Error

                save_error(module = 'Offers',
                    method = 'get_all_user_offers - Document',
                    error = error,
                    user_name = '----',
                    user_id = user_id)

        return response

    @classmethod
    def product_has_user_offer(cls,user_id,product_id,response=None):
        if user_id is not None and product_id is not None:
            try:
                response = cls.objects(Q(bidder_id=user_id)&Q(product_owner_id=product_id)).first()
            except Exception, e:
                raise e
        return response

    @classmethod
    def add_last_proposal(cls,user_id, offer_id,response=None):
        """Save the user who made the las proposal"""
        try:
            print user_id, " of ", offer_id
            offer  = cls.objects(id=offer_id.__str__().decode('utf-8')).first()
            print "OFERTA: ", offer
            offer.user_id_last_prop = user_id
            offer.save()
            response = True
        except Exception as e:
            print "Error saving the user id who made last proposal: ", e
        
        return response

    @classmethod
    def bag_bidder(cls,offer_id,response=None):
        """Gets all products on a bag bidder"""
        try:
            if offer_id is not None:
                offer = cls.objects(id=offer_id).first()
                response=[]
                for item in offer.products_bidder:
                    response.append(item.product_bidder_id)
        except Exception, e:
            print "Error getting bag bidder from offer: ", e
        return response

    @classmethod
    def set_user_rated_true(cls,user_type,offer_id,response=None):

        try:
            offer = cls.objects(id=offer_id).first()
            if user_type == 'bidder':
                offer.bidder_rated = 'True'
            elif user_type == 'owner':
                offer.owner_rated = 'True'
            offer.save()
            response = True
        except Exception, e:
            print "Error setting user rated: ", e
            response = False

        return response


class ProductBidder(EmbeddedDocument):

    user_id_proposal = StringField()
    product_bidder_id = StringField()
