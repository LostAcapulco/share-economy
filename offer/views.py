#!/usr/bin/python
# -*- encoding: utf-8 -*-
"""
Views which allow users to create and interact with an offer.

"""

from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils.encoding import smart_str, smart_unicode

from mongoengine.django.auth import User
from mongoengine.queryset import *

from forms import RegistrationForm
from models import *

from product.views import Product
from chat.documents import Chat
from user.models import *
from notifications.views import general_notification
from errorReport.views import save_error

import unicodedata

@csrf_exempt
@login_required(login_url='/account/login/')
def make_offer(request, tmpl_nm='reg_prod_info.html',form_class=RegistrationForm,):
    """
    Allow a -user- make an offer
    """
    if request.POST.get('product'):

        try:
            from chat.documents import Chat
            offer = Offer()
            bidder = request.session['user']
            bidder_id = bidder.id.__str__().decode('utf-8')
            bidder_name = bidder.username
            
            product_owner_id = request.POST.get('product')
            img = request.POST.get('img_url')
            product_name = request.POST.get('product_name')
            chat_id = request.POST.get('chat_id')
            owner_id = request.POST.get('id_user')
            
            owner = User.objects(id=owner_id).first()
            owner_name = owner.username
            
            bidder = User.objects(id=bidder_id).first()
            bidder_pic = bidder.picture
            
            session_user = request.session['user']
            session_user = session_user.id.__str__().decode('utf-8')

            offer_id = offer.create_offer(bidder_id,
                               bidder_name, 
                               owner_id, 
                               owner_name, 
                               product_owner_id, 
                               product_name, 
                               img)

            Chat.add_env_id(chat_id=chat_id,env_id=offer_id)

            message = bidder_name+" ha ofertado tu "+product_name
            print message
            general_notification(request,channel_id=owner_id,
                                 user_id=bidder_id,
                                 message=message, 
                                 user_name=bidder_name, 
                                 user_pic=bidder_pic,
                                 action_trigered='ha ofertado', 
                                 target_id=str(offer.id), 
                                 target_name=product_name,
                                 enviorment='offer',
                                 env_id = offer_id,
                                 is_read='0')

            response = offer_id
        except Exception as e:
            error = "Error making offer: ",+e.__str__().decode('utf-8')
            print error
            save_error(module = 'offer',
                    method = 'make_offer - view',
                    error = error,
                    user_name = '----',
                    user_id = '----')
            response  = False
    else:
        response = False
    print response
    return HttpResponse(response)

@csrf_exempt
@login_required(login_url='/account/login/')
def current_offer(request,offer_id=None,context=None,wisheslist=0,template='current_offer.html'):
    """
    Allow a product owner to add products for and given offer and send a bidder 
    """
    from user.documents import RegistrationProfile as obj_user
    if request.POST:
            offer_id = request.POST.get('offer_id')
            product = request.POST.dict()
            user_session = request.session['user']
            user_session_id = user_session.id.__str__().decode('utf-8')
            Offer.add_product_bidder(product['offer_id'],user_id_proposal=user_session_id,
                                     product_bidder_id=product['product_id'])
            return HttpResponse('Oferta Guardada')
    else:
        try:
            from product.views import get_product_info, owner_product_info, product_profile, product_comments, matches_wishlist
            from product.documents import Product as obj_product
            from chat.documents import Chat as obj_chat

            user_session = request.session['user']
            user_session_id = user_session.id.__str__().decode('utf-8')

            offer = Offer.objects(id=offer_id).first()
            owner_id = offer.owner_id
            owner_username = offer.owner_name
            product = offer.product_owner_name
            product_id = offer.product_owner_id
            offer_status = offer.status
            owner_rated = offer.owner_rated
            bidder_rated = offer.bidder_rated
    
            user_obj = User.objects(id=owner_id).first()
            product_obj = obj_product.get_product(offer.product_owner_id)
            info_product = get_product_info(request,product_id)

            if product_obj.user_id == user_session_id:
                owner = True
                product_user_id = user_session_id

                tags = product_obj.tags[0].first_tag+' '+product_obj.tags[0].second_tag+' '+product_obj.tags[0].third_tag
                tags = tags.__str__().decode('utf-8')
                tags = {tag.strip("#") for tag in tags.split() if tag.startswith("#")}
                
                searchable_tags =[]
                
                for tag in tags:
                    searchable_tags.append(tag)

                wisheslist = matches_wishlist(request,searchable_tags)

            else:
                owner = False

            offers_top = Offer.objects(Q(owner_id=owner_id.__str__().decode('utf-8'))&Q(product_owner_id=product_id.__str__().decode('utf-8'))&Q(status='1')).all()
            tot_offer = len(offers_top)

            bidder_id = offer.bidder_id
            bidder_username = offer.bidder_name
            session_user = request.session['user']
            session_user_id = session_user.id.__str__().decode('utf-8')

            bidder_obj = User.objects(id=bidder_id).first()

            comments = product_comments(product_id)


            if session_user_id == bidder_id or owner_id == session_user_id:
                if session_user_id == bidder_id and owner_id != session_user_id:
                    user_type = 'B'
                    second_user = owner_id
                elif bidder_id != session_user_id and session_user_id == owner_id:
                    user_type = 'A'
                    second_user = bidder_id
                try:
                    chat = obj_chat.get_chat_from_env_id(offer_id)
                    chat_id = chat.id.__str__().decode('utf-8')

                except Exception as e:
                    error = "ERROR getting chat from offer: "+e.__str__().decode('utf-8')
                    print error

                    user_sesion = request.session['user']
                    user_sesion_name = user_sesion.username
                    user_session_id = user_sesion.id.__str__().decode('utf-8')

                    save_error(module = 'offer',
                    method = 'current_offer - view',
                    error = error,
                    user_name = user_sesion_name,
                    user_id = user_session_id)
                    chat_id = "0"

                offer_is = Offer.objects((Q(bidder_id=user_session_id) & Q(product_owner_id=product_id)) & Q(status='1')).all()

                if len(offer_is) == 0:
                    is_offered = 'N'
                else:
                    is_offered = 'Y'

                follower = obj_product.is_follower(product_id,user_session_id)

                if follower is not None:
                    is_follower = True
                else:
                    is_follower = False

                followers = len(product_obj.follows)

                last_user_prop = offer.user_id_last_prop

                if last_user_prop == None and user_session_id == bidder_id:
                    show_menu_offer = True
                    show_acept_deny = False
                    new_offer = True
                elif last_user_prop is not None and last_user_prop != session_user_id:
                    show_menu_offer = True
                    show_acept_deny = True
                    new_offer =False
                elif last_user_prop == None and user_session_id == owner_id:
                    show_menu_offer = False
                    show_acept_deny = False
                    new_offer = True
                elif last_user_prop is not None and user_session_id == last_user_prop:
                    show_menu_offer = False
                    show_acept_deny = False
                    new_offer = False

                obj_owner  = obj_user.get_user(owner_id)
                owner_img = obj_owner.picture
                obj_bidder = obj_user.get_user(bidder_id)
                bidder_img = obj_bidder.picture

                context = RequestContext(request,{'offer_id':offer_id,
                                              'owner_id':owner_id,
                                              'owner_profile':owner,
                                              'owner_username':owner_username,
                                              'owner_img':owner_img,
                                              'bidder_id':bidder_id,
                                              'bidder_username':bidder_username,
                                              'bidder_rating':bidder_obj.rating,
                                              'bidder_img':bidder_img,
                                              'is_offered':is_offered,
                                              'is_follower':is_follower,
                                              'followers':followers,
                                              'user_type':user_type,
                                              'product':product,
                                              'product_id': product_id,
                                              'info_product': info_product,
                                              'tags': info_product['tags'],
                                              'tot_offer': tot_offer,
                                              'wisheslist':wisheslist,
                                              'chat_id':chat_id,
                                              'comments':comments,
                                              'show_menu_offer':show_menu_offer,
                                              'show_acept_deny':show_acept_deny,
                                              'new_offer':new_offer,
                                              'second_user':second_user,
                                              'offer_status':offer_status,
                                              'owner_rated':owner_rated,
                                              'bidder_rated':bidder_rated,
                                              'PUSHER_KEY': settings.PUSHER_APP_KEY},[owner_product_info])
            
                return render_to_response(template,context)
        except Exception as e:
                error = "Error getting offer info: "+e.__str__().decode('utf-8')
                print error
                user_sesion = request.session['user']
                user_sesion_name = user_sesion.username
                user_session_id = user_sesion.id.__str__().decode('utf-8')
                
                save_error(module = 'offer',
                method = 'current_offer - view',
                error = error,
                user_name = user_sesion_name,
                user_id = user_session_id)
                
                context = RequestContext(request,{})
                
                return render_to_response('not_found.html',context)
        else:
            return redirect('/account/home/')

@csrf_exempt
def response_response(request):
    """
    Response to an offer made for some user add a product to the bag bidder
    """
    if request.POST:
        user = request.session['user']
        user_id = user.id.__str__().decode('utf-8')
        user_name = user.username
        user_type = request.POST.get('user_type')
        product_id = request.POST.get('product_id')
        offer_id = request.POST.get('offer_id')

        offer = Offer.objects(id=offer_id).first()
        offer.status = '1'
        offer.save()
        try:
            if product_id:
                response = offer.add_product_bidder(offer_id,type_user_abc=user_type,product_bidder_id=product_id)
                if response is not False:
                    product_owner = offer.product_owner_name
                    owner_id = offer.owner_id
                    product_id = offer.product_owner_id
                    product_img = offer.product_owner_img
                    message = user_name+" ha agregado un articulo a la propuesta de "+product_owner
                    
                    user = request.session['user']
                    user_id = user.id.__str__().decode('utf-8')
                    try:
                        offer = Offer.objects(owner_id=user_id).first()
                        channel_id = offer.bidder_id
                    except:
                        offer = Offer.objects(bidder_id=user_id).first()
                        channel_id = offer.owner_id

                    general_notification(request,
                                         channel_id=channel_id,
                                         user_id=user_id,
                                         message=message, 
                                         user_name=user_name, 
                                         user_pic=product_img,
                                         action_trigered=" ha agregado un articulo la propuesta ", 
                                         target_id=product_id,
                                         target_name=product_owner,
                                         enviorment='offer',
                                         env_id=offer_id,
                                         is_read='0')
                    response = True
                else:
                    response = "El producto ya se encuentra en la propuesta"
            else:
                response = "Intenta agregar el producto nueva mente"
        except Exception as e:
            response = e

        return HttpResponse(response)

@csrf_exempt
def response_propose(request):
    """ Acept or cancel an offer 
        2 cancel 
        3 acept
    """
    from user.views import RegistrationProfile as obj_user
    if request.POST:
        offer_id = request.POST.get('offer_id')
        offer_response = request.POST.get('offer_res')
        user_id = request.POST.get('user_id')
        product_id = request.POST.get('product_offer_id')
        user_session = request.session['user']
        user_session_id = user_session.id.__str__().decode('utf-8')

        try:
            cancel_other_offers(request,product_id,offer_id)
            offer = Offer.response(offer_id=offer_id,offer_response=offer_response)
            response = True
            if response:
                if offer_response == '2':
                    ress = "cancelado"
                elif offer_response == '3':
                    ress = "aceptado"
                    obj_user.add_swap(user_id);
                    obj_user.add_swap(user_session_id);

                message = user_session.username+" ha "+ress+" la oferta"

                try:
                    offer = Offer.objects(owner_id=user_session_id).first()
                    channel_id = offer.bidder_id
                    name = offer.bidder_name

                except:
                    offer = Offer.objects(bidder_id=user_session_id).first()
                    channel_id = offer.owner_id
                    name = offer.owner_name
                general_notification(request,
                                     channel_id=channel_id,
                                     user_id=user_session_id,
                                     message=message, 
                                     user_name=user_session.username, 
                                     user_pic=offer.product_owner_img,
                                     action_trigered=" ha "+ress+" la oferta ", 
                                     target_id=channel_id,
                                     target_name=name,
                                     enviorment='offer',
                                     is_read='0')

        except Exception, e:
            print "Error responding de offer: ", e
            response = False

    else:
        response = False

    return HttpResponse(response)

def cancel_other_offers(request,product_id,offer_id,response=None):
    """
        Check if theres more than one product on user stock
        for a given product_id if there is just one and more 
        offers for the product cancel the other current offers
        2 cancelado
        3 aceptado
    """
    from product.documents import Product as obj_product
    try:
        user_session = request.session['user']
        user_session_id = user_session.id.__str__().decode('utf-8')
        prod = obj_product.get_product(product_id)
        prod_stock = prod.cant_prod
        print "Product in stock: ", prod_stock
        if prod_stock == '1':
          offers = Offer.get_all_offers_product(product_id)
          message = user_session.username+" ha cancelado la oferta"
          for offer in offers:
            if offer_id != offer.id.__str__().decode('utf-8'):
                Offer.response(offer_id=offer.id,offer_response='2')
                channel_id = offer.bidder_id
                general_notification(request,
                                         channel_id=channel_id,
                                         user_id=user_session_id,
                                         message=message, 
                                         user_name=user_session.username, 
                                         user_pic=offer.product_owner_img,
                                         action_trigered=" ha cancelado la oferta ", 
                                         target_id=channel_id,
                                         target_name=offer.bidder_name,
                                         enviorment='offer',
                                         is_read='0')
                print offer.bidder_name, message
    except Exception, e:
        print "Error while canceling other offers: ", e
    return response

@csrf_exempt
def products_in_bidder(request,prdcts=None):
    from product.documents import Product as obj_product
    prdct = []
    if request.POST:
        try:
            offer_id = request.POST.get('offer_id')
            offer = Offer.objects(id=offer_id).first()

            products = offer.products_bidder
            if products is not None:
                for product_bddr in products:
                    product = obj_product.objects(id=product_bddr.product_bidder_id).first()
                    prdct.append(product)
        except Exception as e:
            print e
        prdcts = get_products(prdct)

    return HttpResponse(prdcts)

def get_products(products=None):
    arrProducts = {}
    if products is not None: 
        chunck_products = []
        tags = {}
        category = {}
        for product in products:
            user_product = {}
            user_product.update({'product_name':product.product_name})
            user_product.update({'product_id':product.id.__str__().decode('utf-8')})
            user_product.update({'product_img':product.pictures[0].url })
            for c in product.categories:
                category.update({'category_1': c.first_cat})
                category.update({'category_2': c.second_cat})
            user_product.update({'categories':category})
            for t in product.tags:    
                tags.update({'tags_1':t.first_tag})
                tags.update({'tags_2':t.second_tag})
                tags.update({'tags_3':t.third_tag})
            user_product.update({'product_tags':tags})
            if product.shares is not None:
                user_product.update({'product_shares':product.shares})
            else:
                user_product.update({'product_shares':0})
                user_product.update({'product_follows': product.follows.__len__() })
            #user_product.update({'geo_location':product.geo_location})
            chunck_products.append([user_product])

    return json.dumps({'products':chunck_products})

@login_required(login_url='/account/login/')
def chek_status_offer(request):
    """
    Check the status of a given "Offer"
    """
    if request.POST:
        offer_id = request.POST.get('offer_id')
        offer = Offer.objects(id=offer_id).first()
        offer_status = offer.status
    else:
        offer_status = None 
    context = RequestContext(request,{'offer_status':offer_status})
     
    return HttpResponse(context)

@csrf_exempt
def active_offers_ajax(request,chunk_offers=None):
    """
    Get all active offers for a given user
    """
    if request.POST:
        try:
            user_session = request.session['user']

            offers = Offer.get_all_user_offers(user_session.id.__str__().decode('utf-8'))
            chunk_offers = serialize_offers(request,offers)

        except Exception as e:
            user_session = request.session['user']
            error = "Error getting offers via ajax: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'offer',
                    method = 'serialize_offers - view',
                    error = error,
                    user_name = user_session.username,
                    user_id = user_session.id.__str__().decode('utf-8'))

    return HttpResponse(json.dumps({'products':chunk_offers}))

def serialize_offers(request,offers=None):
    """Get all active offers for a given user"""
    chunk_offers=[]
    if offers is not None:
        try:
            user_session = request.session['user']
            tmp_list = []
            for obj in offers:
                offer = {}
                

                if unicode(obj.product_owner_name) not in tmp_list:

                    tmp_list.append(obj.product_owner_name)

                    if obj.owner_id == user_session.id.__str__().decode('utf-8'):

                        offer.update({'date':obj.date.__str__().decode('utf-8')})
                        user = User.objects(id=obj.bidder_id).first()
                        offer.update({'bidder_photo':user.picture})
                        offer.update({'bidder':obj.bidder_name})
                        offer.update({'title':' quiere tu '})
                        offer.update({'product':''+obj.product_owner_name})
                        offer.update({'owner':obj.owner_name})
                        offer.update({'photo':obj.product_owner_img})
                        offer.update({'url':'/offer/current/'+obj.id.__str__().decode('utf-8')})
                        offer.update({'status':obj.status})

                        chunk_offers.append([offer])
                    elif obj.bidder_id == user_session.id.__str__().decode('utf-8'):

                        offer.update({'date':obj.date.__str__().decode('utf-8')})
                        user = User.objects(id=obj.bidder_id).first()
                        offer.update({'bidder_photo':user.picture})
                        offer.update({'bidder':obj.bidder_name})
                        offer.update({'product':obj.product_owner_name+'  de'})
                        offer.update({'title':'ofertaste  en'})
                        offer.update({'owner':obj.owner_name})
                        offer.update({'photo':obj.product_owner_img})
                        offer.update({'url':'/offer/current/'+obj.id.__str__().decode('utf-8')})
                        offer.update({'status':obj.status})
                        
                        chunk_offers.append([offer])

        except Exception as e:
            error = "Error serializing data in offer: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'offer',
                    method = 'serialize_offers - view',
                    error = error,
                    user_name = user_session.username,
                    user_id = user_session.id.__str__().decode('utf-8'))
    return  chunk_offers

@csrf_exempt
def get_product_offer(request):
    """
    Get all offers for a given product
    """
    chunk_offers = []
    if request.POST:
        user_session = request.session['user']
        try:
            from chat.documents import Chat as obj_chat
            product_id = request.POST.get('product_id')

            offers = Offer.get_all_offers_product(product_id)
 
            if offers is not None:
                for obj in offers:
                    offer = {}
                    user = User.objects(id=obj.bidder_id).first()

                    offer.update({'offer_id':obj.id.__str__().decode('utf-8')})
                    offer.update({'bidder_photo':user.picture})
                    offer.update({'bidder_name':obj.bidder_name})
                    offer.update({'bidder_id':obj.bidder_id})
                    offer.update({'owner_id':obj.owner_id})
                    offer.update({'photo':obj.product_owner_img})
                    offer.update({'rating':user.rating})

                    try:
                        chat = obj_chat.get_chat_from_env_id(obj.id.__str__().decode('utf-8'))
                        chat_id =chat.id.__str__().decode("utf-8")

                    except Exception as e:
                        chat_id = ''
                        error = "ERROR getting chat from offer: "+e.__str__().decode('utf-8')
                        print error
                        save_error(module = 'offer',
                                    method = 'get_product_offer - view',
                                    error = error,
                                    user_name = user_session.username,
                                    user_id = user_session.id.__str__().decode('utf-8'))

                    offer.update({'chat_id':chat_id})
                    chunk_offers.append([offer])

        except Exception as e:
            error = "Error getting offers for a given product: "+e.__str__().decode('utf-8')
            print error
            save_error(module = 'offer',
                    method = 'get_product_offer - view',
                    error = error,
                    user_name = user_session.username,
                    user_id = user_session.id.__str__().decode('utf-8'))


    return HttpResponse(json.dumps({'offers':chunk_offers}))

@csrf_exempt
def rating_user(request, response=None):
    """Add rating for a given user and offer"""
    from user.documents import RegistrationProfile as obj_user
    if request.POST:
        try:
            user_rated_id = request.POST.get('user_rated_id')
            user_type = request.POST.get('user_type')
            rating = request.POST.get('rating')
            offer_id = request.POST.get('offer_id')
            user_session = request.session['user']
            grader_user_name = user_session.username
            print user_rated_id
            print user_session.id
            obj_user.add_rating(user_rated_id,rating)
            Offer.set_user_rated_true(user_type,offer_id)

            user = User.objects(id=user_rated_id).first()
            user_pic = user.picture
            user_rated_name = user.username
            action_trigered = "te ha calificado"
            message = action_trigered
            general_notification(request,
                             channel_id=user_rated_id,
                             user_id=user_rated_id,
                             message=message, 
                             user_name=user_session.username, 
                             user_pic=user_pic,
                             action_trigered=action_trigered, 
                             target_id=user_rated_id, 
                             target_name=user_rated_name,
                             enviorment='offer',
                             is_read='0')
            response = True
        except Exception as e:
            print "Error triying to rate user: ", e
            response =  False
    return HttpResponse(response)

@csrf_exempt
def delete_offer_prod(request, response=False):
    if request.POST:
        product_id = request.POST.get('product_id')
        offer_id = request.POST.get('offer_id')
        try:
            Offer.delete_product_bidder_offer(offer_id,product_id)
            offer = Offer.objects(id=offer_id).first()
            user_session = request.session['user']
            user_session_id = user_session.id.__str__().decode('utf-8')
            user_session_name = user_session.username
            message = user_session_name+" ha borrado un articulo de la oferta al articulo "+offer.product_owner_name
            if offer.bidder_id == user_session_id:
                channel_id = offer.owner_id
                target_name = offer.owner_name
            elif offer.owner_id==user_session_id:
                channel_id = offer.bidder_id
                target_name = offer.bidder_name

            general_notification(request,
                             channel_id=channel_id,
                             user_id=user_session_id,
                             message=message,
                             user_name=user_session_name,
                             user_pic=user_session.picture,
                             action_trigered=message,
                             target_id=offer_id,
                             target_name=target_name,
                             enviorment='offer',
                             is_read='0')
            response = True
        except Exception as e:
            print e
            response = False
    return HttpResponse(response)


@csrf_exempt
def send_propouse(request):
    """
    Sends a notifications for a user in a proposal
    """
    if request.POST:
        offer_id = request.POST.get('offer_id')

        try:
            user_session = request.session['user']
            user_session_id = user_session.id.__str__().decode('utf-8')
            response = True

            message = user_session.username+" te ha hecho una propuesta"

            try:
                offer = Offer.objects(Q(owner_id=user_session_id) & Q(id=offer_id)).first()
                channel_id = offer.bidder_id
                name = offer.bidder_name

            except:
                offer = Offer.objects(Q(bidder_id=user_session_id) & Q(id=offer_id)).first()
                channel_id = offer.owner_id
                name = offer.owner_name

            print "Guardando datos de ultimo usuario"
            respuesta = Offer.add_last_proposal(user_session_id,offer.id)
            print "Se guardo el usuario: ", respuesta
            print "Enviando notification"
            general_notification(request,
                                 channel_id=channel_id,
                                 user_id=user_session_id,
                                 message=message, 
                                 user_name=user_session.username, 
                                 user_pic=offer.product_owner_img,
                                 action_trigered=" ha hecho un propuesta ", 
                                 target_id=channel_id,
                                 target_name=name,
                                 enviorment='offer',
                                 env_id=offer_id,
                                 is_read='0')

        except Exception, e:
            print "ERROR sending notification of proposal: ", e
            response = False

    else:
        response = False

    return HttpResponse(response)