#!/usr/bin/python
# vim: set fileencoding= utf-8 :

from django import forms
from django.utils.translation import ugettext_lazy as _

from models import *

attrs_dict = {'class': 'required'}
cat_producto = {'Categorias','VIDEOJUEGOS / CONSOLAS / ACCESORIOS',
                'TECNOLOGÍA / COMPUTACIÓN / GADGETS'
                'CELULARES Y CÁMARAS',
                'ELECTRODOMÉSTICOS Y LÍNEA BLANCA',
                'TV / AUDIO / VIDEO',
                'PELÍCULAS / SERIES / CD-S',
                'MÚSICA / INSTRUMENTOS / DJ',
                'FOTO / ARTE / DISEÑO',
                'LIBROS / REVISTAS / COMICS',
                'MUEBLES / DECORACIÓN /HOGAR',
                'ROPA / CALZADO / ACCESORIOS',
                'PARA MASCOTAS Y ANIMALES',
                'DEPORTES / FITNESS / APARATOS',
                'BELLEZA Y SALUD',
                'JUEGOS Y JUGUETES',
                'PRODUCCIÓN ARTESANAL',
                'COLECCIÓN / VINTAGE / ANTIGÜEDAD',
                'BICIS / MOTOS / TRANSPORTE',
                'OTRO ARTICULO',
                'SERVICIOS',
                'HABILIDADES Y CONOCIMIENTOS',
                'OFERTAS Y PROMOCIONES',
                'MI KAMVIO ',
                'RENTA'}

class RegistrationForm(forms.Form):
    """
    Form for upload a prodcut to your inventory

    """
    productname = forms.CharField( max_length=30,
                                widget=forms.TextInput(attrs={'placeholder':'Nombre del producto'}),
                                label=_(u'Dale el nombre que gustes a tu producto(Ejemplos: iPad Clasico, Xbox 360)'),required=True)
    tags = forms.CharField( max_length=30,
                            widget=forms.TextInput(attrs=dict({'placeholder':'Tags'},
                            maxlength=75)),
                            label=_(u'Escoje tags para que la gente encuentre tu producto (Ejemplo: #iPad #apple #tablet)'),required=True)
    category = forms.ChoiceField(choices=[(x, x) for x in cat_producto],
                                 label=_(u'¿En cuál de las categorías de Kamvia crees que encaja tu producto? Puedes elejir dos categorís distintas'))
    category_2 = forms.ChoiceField(choices=[(x, x) for x in cat_producto],
                                   label=_(u' '))
    about_product = forms.CharField(widget=forms.Textarea(attrs={'palceholder':'Decribe tu producto (max 200 caracteres)'}),
                                    label=_(u'Agrega una descripcion de tu producto, razones por la que quieres intercambiar, etc'),
                                    required=True)
    stp1_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'form_id','value':'{[form_id="form_id_21"]}'}),required=False)
    nxt_stp_id = forms.CharField(widget=forms.HiddenInput(attrs={'ng-model':'kamvia_next_id', 'value':'{[kamvia_next_id]}'}),required=False)
    
    
    def save(self, user_id=None):

        tags_list = self.cleaned_data['tags'].split()
        
        cat_list = ['','']

        cat_list[0] = self.cleaned_data['category']
        cat_list[1] = self.cleaned_data['category_2']
        
        if cat_list[0] == 'Categorias':
            cat_list[0] = ''

        new_product = Product.create_product(user_id,
                                             self.cleaned_data['productname'],
                                             tags_list,
                                             cat_list,
                                             self.cleaned_data['about_product'])

        return new_product
